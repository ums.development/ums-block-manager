# UMS Block Manager

University of Maine System IT, Custom Enterprise Solutions, Web Development Team

https://www.maine.edu/

WordPress Plugin to manage an approved list of Gutenberg blocks for use on UMS sites.

This plugin was developed to fill a need to create an allow list of approved block types (rather than a deny list of dis-approved block types), thus ensuring that newly installed types would not be accidentally enabled without first being reviewed.

Large portions of this project, particularly the front end, are based on code from the [Gutenberg Block Manager Plugin By Darren Cooney](https://wordpress.org/plugins/block-manager/). Both projects are available under the GPL v2 or later license.

Due to resource limitations, this code base is being released as is and without support. Merge requests are welcome but will be considered only as time allows.

### Initial Setup
In order for the Gutenberg system to function, the core/paragraph block needs to be enabled. Once the plugin has been network activated, simply toggle the switch to globally activate the Paragraph block. 

## Developer Setup
Regardless of which of the below development paths you take (both have their pros and cons), you will need a WordPress MultiSite up and running.

### Plugin Development
This describes a setup using the features within IntelliJ IDEA and a locally run docker container with WordPress Multisite. If you use a different IDE, feel free to adapt.  Under `File > Settings > Deployment` create a deployment scheme with a locally mounted folder. This will point to the location of the `wp-content` folder of the WordPress Docker mount. For example: `/home/user/git/docker/WordPress-Multisite/wp-mnt/wp-content`.

Set `/client/` as an excluded path.

Under Mappings:
* `./` points to `plugins/ums-block-manager`.
* `./client/network-admin/build/static/css` points to `plugins/ums-block-manager/dist/`.
* `./client/network-admin/build/static/js` points to `plugins/ums-block-manager/dist/`.

### Server Development
Install the dependencies by running `composer install`.

### Local Client Development with Hot Reload
Any commands below will assume that we are in the `client/network-admin` directory.

#### Enable CORS Bypass
Place the following code in your functions.php theme file.
```php
add_action( 'rest_api_init', function()
{
	header( "Access-Control-Allow-Origin: *" );
} );
```

#### Install/Enable Basic Authentication
Install the plugin, [Basic Auth](https://github.com/WP-API/Basic-Auth).
Once installed, network activate the plugin.

#### Install Dependencies
Run `yarn`.
This will install all the dependencies needed for the client code, including the development dependencies.

#### Set the Environment Variables
Set valid values for the environment variables listed in `client/network-admin/.env`. If you are using the WP Docker image, then the host should be correct. You will need a valid NONCE though. You can grab one by watching for a rest api call from within the WordPress plugin. Look for the header, X=WP-Nonce. Alternatively, you can use the php function, [wp_create_nonce](https://developer.wordpress.org/rest-api/using-the-rest-api/authentication/). 

#### Start the development server
Run `yarn start`.
A new browser tab should have opened with the app running under `http://localhost:3000/`.
There will be some default data that has been populated for convenience as the data is only present when running the app within the actual WP environment. This data is static and can be changed if you like in the files:  `src/store/wpDefaultBlockData.js`, and `src/store/wpDefaultCategoryData.js`.
In order for the actions within the app to work, the custom rest api routes need to be added by activating the plugin itself.

### Local Client Development from within WordPress
Any commands below will assume that we are in the `client/network-admin` directory.

#### Install Dependencies
Run `yarn`.
This will install all the dependencies needed for the client code, including the development dependencies.

#### Generating the client code
There are 2 ways you can generate the client code.

The first is with `yarn build` from the client/network-admin directory. Once the command finishes successfully, you can deploy the folder, `client/network-admin/build`, assuming you set up the project deployment settings like they are in the Plugin Development section above.

The other way, is to run `composer client-generate` from the project root. This will place the distributable files in the `/dist` folder. The `/dist` folder can now be deployed to `plugins/ums-block-manager/dist/`. You can add the mapping to the project deployment settings by following the example given in the Plugin Development section above. This option is what is used to create the actual distributable used in production, however, it will remove your client development dependencies. 
