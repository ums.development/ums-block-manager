<?php
/**
 * Code for filtering the gutenburg blocks in the editor
 *
 * @package UMS\WordPress\BlockManager
 */

namespace UMS\WordPress\BlockManager;

/**
 * EditorBlockFiltering
 */
class EditorBlockFiltering {
	/**
	 * Enqueue scripts for the user editor
	 *
	 * @return void
	 */
	public static function enqueue() {
		wp_enqueue_script(
			Constants::SLUG . '-block_filter',
			plugins_url( 'scripts/block_filter.js', __FILE__ ),
			array( 'wp-edit-post' ),
			Constants::VERSION,
			false
		);
		wp_add_inline_script(
			Constants::SLUG . '-block_filter',
			'const UMS_BLOCK_MANAGER_ENABLED_BLOCKS_ALL = ' . Helpers::get_enabled_blocks_as_json() . ';',
			'before'
		);
	}



}
