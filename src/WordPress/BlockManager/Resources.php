<?php
/**
 * Singleton class designed to hold any resources that the plugin requires
 *
 * @package UMS\WordPress\BlockManager
 */

namespace UMS\WordPress\BlockManager;

/**
 * Singleton class designed to hold any resources that the plugin requires
 */
class Resources {

	/**
	 * Singleton instance
	 *
	 * @var Resources
	 */
	private static $instance = null;

	/**
	 * Template engine
	 *
	 * @var \Twig\Environment
	 */
	private $template_engine = null;

	/**
	 * Create resources needed by the plugin
	 */
	public function __construct() {
		$loader                = new \Twig\Loader\FilesystemLoader( __DIR__ . '/../../../templates/' );
		$this->template_engine = new \Twig\Environment( $loader );
	}

	/**
	 * Get the singleton instance
	 *
	 * @return Resources|null
	 */
	public static function get_instance(): ?Resources {
		if ( ! self::$instance ) {
			self::$instance = new Resources();
		}

		return self::$instance;
	}

	/**
	 * Get the template engine
	 *
	 * @return \Twig\Environment
	 */
	public function get_template_engine():\Twig\Environment {
		return $this->template_engine;
	}
}
