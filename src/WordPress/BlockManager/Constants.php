<?php
/**
 * Constants used by the plugin
 *
 * @package UMS\WordPress\BlockManager
 */

namespace UMS\WordPress\BlockManager;

/**
 * Constants
 */
class Constants {

	public const SLUG                      = 'ums-block-manager';
	public const BLOCK_STATUS_OPTION       = self::SLUG . '_BLOCK_STATUSES';
	public const EXTRA_BLOCK_STATUS_OPTION = self::SLUG . '_EXTRA_BLOCK_STATUSES';
	public const EXTRA_BLOCK_SITES_OPTION  = self::SLUG . '_EXTRA_BLOCK_SITES';
	public const VERSION                   = '1.2.2';
	public const MIN_WP_VERSION            = '5.9.3';
}
