<?php
/**
 * Toggle Rest API Route
 *
 * @package UMS\WordPress\BlockManager\API
 */

namespace UMS\WordPress\BlockManager\API;

use UMS\WordPress\BlockManager\Constants;
use UMS\WordPress\BlockManager\Helpers;

/**
 * Toggle
 */
class Toggle {


	/**
	 * Construct
	 *
	 * Registers the route for use
	 */
	public function __construct() {
		add_action(
			'rest_api_init',
			function () {
				register_rest_route(
					Constants::SLUG,
					'/toggle',
					array(
						'methods'             => 'POST',
						'callback'            => array( $this, 'toggle' ),
						'permission_callback' => function () {
							return Helpers::has_access();
						},
					)
				);
			}
		);
	}


	/**
	 * Enable/Disable individual gutenberg blocks.
	 *
	 * @param \WP_REST_Request $request Request.
	 * @return void
	 */
	public static function toggle( \WP_REST_Request $request ) {
		$block = Helpers::get_block_data_from_request( $request );
		if ( null === $block ) {
			wp_send_json_error(
				array(
					'error' => 'Unable to get the block name/parent',
				),
				400
			);

			return;
		}

		$block = Helpers::toggle_block( Constants::BLOCK_STATUS_OPTION, $block );

		wp_send_json(
			$block
		);
	}
}

