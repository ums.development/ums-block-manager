<?php
/**
 * Site Rest API Route
 *
 * @package UMS\WordPress\BlockManager\API
 */

namespace UMS\WordPress\BlockManager\API;

use UMS\WordPress\BlockManager\Constants;
use UMS\WordPress\BlockManager\Helpers;

/**
 * Site
 */
class Site {


	/**
	 * Construct
	 *
	 * Registers the route for use
	 */
	public function __construct() {
		add_action(
			'rest_api_init',
			function () {
				register_rest_route(
					Constants::SLUG,
					'/sites',
					array(
						'methods'             => 'GET',
						'callback'            => array( $this, 'get_all' ),
						'permission_callback' => function () {
							return Helpers::has_access();
						},
					)
				);
			}
		);
		add_action(
			'rest_api_init',
			function () {
				register_rest_route(
					Constants::SLUG,
					'/site',
					array(
						'methods'             => 'POST',
						'callback'            => array( $this, 'add_site' ),
						'permission_callback' => function () {
							return Helpers::has_access();
						},
					)
				);
			}
		);
		add_action(
			'rest_api_init',
			function () {
				register_rest_route(
					Constants::SLUG,
					'/site',
					array(
						'methods'             => 'DELETE',
						'callback'            => array( $this, 'delete_site' ),
						'permission_callback' => function () {
							return Helpers::has_access();
						},
					)
				);
			}
		);
	}


	/**
	 * Get All Sites.
	 *
	 * @return void
	 */
	public static function get_all() {
		$sites = Helpers::get_sites_abbreviated();

		wp_send_json(
			$sites
		);
	}


	/**
	 * Add a Site.
	 *
	 * @param \WP_REST_Request $request Request.
	 * @return void
	 */
	public static function add_site( \WP_REST_Request $request ) {
		$id = self::get_site_id( $request );
		if ( null === $id ) {
			wp_send_json_error(
				array(
					'error' => 'Unable to get the site id',
				),
				400
			);

			return;
		}

		if ( ! Helpers::does_site_exist( $id ) ) {
			wp_send_json_error(
				array(
					'error' => 'Unknown site id',
				),
				400
			);

			return;
		}

		$sites = (array) get_network_option( null, Constants::EXTRA_BLOCK_SITES_OPTION, array() );

		$sites[] = $id;
		$sites   = array_unique( $sites );

		update_network_option( null, Constants::EXTRA_BLOCK_SITES_OPTION, $sites );

		wp_send_json(
			$sites
		);
	}


	/**
	 * Delete a Site.
	 *
	 * @param \WP_REST_Request $request Request.
	 * @return void
	 */
	public static function delete_site( \WP_REST_Request $request ) {
		$id = self::get_site_id( $request );
		if ( null === $id ) {
			wp_send_json_error(
				array(
					'error' => 'Unable to get the site id',
				),
				400
			);

			return;
		}

		$sites = (array) get_network_option( null, Constants::EXTRA_BLOCK_SITES_OPTION, array() );

		$sites = array_diff( $sites, array( $id ) );

		update_network_option( null, Constants::EXTRA_BLOCK_SITES_OPTION, $sites );

		wp_send_json(
			$sites
		);
	}


	/**
	 * Gets the Site ID from the request body
	 *
	 * @param \WP_REST_Request $request Rest API Request.
	 * @return int|null
	 */
	public static function get_site_id( \WP_REST_Request $request ): ?int {
		$body = json_decode( $request->get_body(), true );

		if ( ! $body || strlen( $body['id'] ) <= 0 ) {
			return null;
		}

		$id = $body['id'];

		if ( ! is_numeric( $id ) ) {
			return null;
		}

		return (int) $id;
	}
}

