<?php
/**
 * Enabled Site Rest API Route
 *
 * @package UMS\WordPress\BlockManager\API
 */

namespace UMS\WordPress\BlockManager\API;

use UMS\WordPress\BlockManager\Constants;
use UMS\WordPress\BlockManager\Helpers;

/**
 * EnabledSites
 */
class EnabledSites {


	/**
	 * Construct
	 *
	 * Registers the route for use
	 */
	public function __construct() {
		add_action(
			'rest_api_init',
			function () {
				register_rest_route(
					Constants::SLUG,
					'/enabled-sites',
					array(
						'methods'             => 'GET',
						'callback'            => array( $this, 'get_sites' ),
						'permission_callback' => function () {
							return Helpers::has_access();
						},
					)
				);
			}
		);
	}


	/**
	 * Get Sites.
	 *
	 * @return void
	 */
	public static function get_sites() {
		$enabled_sites    = array();
		$enabled_site_ids = (array) get_network_option( null, Constants::EXTRA_BLOCK_SITES_OPTION, array() );
		foreach ( $enabled_site_ids as $id ) {
			$sites           = Helpers::get_sites_abbreviated( $id );
			$enabled_sites[] = $sites[0];
		}

		wp_send_json(
			$enabled_sites
		);
	}
}

