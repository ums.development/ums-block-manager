<?php
/**
 * Helper functions available for use in this plugin
 *
 * @package UMS\WordPress\BlockManager
 */

namespace UMS\WordPress\BlockManager;

/**
 * Helpers
 */
class Helpers {

	/**
	 * Has Access
	 *
	 * Determines if the user has access to use functions in this plugin
	 *
	 * @return bool
	 */
	public static function has_access(): bool {
		return is_user_logged_in() && current_user_can( 'setup_network' );
	}


	/**
	 * Gets the status of each of the configured blocks
	 *
	 * @return array block statuses.
	 */
	public static function get_enabled_blocks(): array {
		$global_enabled = (array) get_network_option( null, Constants::BLOCK_STATUS_OPTION, array() );
		$extra_sites    = (array) get_network_option( null, Constants::EXTRA_BLOCK_SITES_OPTION, array() );
		$current_site   = get_current_blog_id();
		$enabled_blocks = $global_enabled;

		if ( in_array( $current_site, $extra_sites, true ) ) {
			$extra_blocks   = (array) get_network_option( null, Constants::EXTRA_BLOCK_STATUS_OPTION, array() );
			$enabled_blocks = array_merge( $global_enabled, $extra_blocks );
		}

		return $enabled_blocks;
	}

	/**
	 * Gets the status of each of the configured blocks
	 *
	 * @param string $option Option to ge the blocks from.
	 *
	 * @return array block statuses.
	 */
	public static function get_enabled_blocks_by_option( string $option ): array {
		return (array) get_network_option( null, $option, array() );
	}


	/**
	 * Gets the enabled blocks in a json friendly way
	 *
	 * @return string enabled blocks json.
	 */
	public static function get_enabled_blocks_as_json() {
		$blocks = self::get_enabled_blocks();

		return self::blocks_to_json( $blocks );
	}


	/**
	 * Gets the enabled blocks in a json friendly way
	 *
	 * @param string $option_type option to get the enabled blocks for.
	 * @return string enabled blocks json.
	 */
	public static function get_enabled_blocks_by_option_as_json( string $option_type ) {
		$blocks = self::get_enabled_blocks_by_option( $option_type );
		return self::blocks_to_json( $blocks );
	}


	/**
	 * Convert the blocks object into a json string
	 *
	 * @param array $blocks blocks to convert.
	 * @return false|string
	 */
	public static function blocks_to_json( array $blocks ) {
		$json_ready_blocks = array();
		foreach ( $blocks as $key => $value ) {
			$json_ready_blocks[] = array(
				'name'   => $key,
				'parent' => $value['parent'],
				'status' => $value['status'],
			);
		}

		return wp_json_encode( $json_ready_blocks );
	}

	/**
	 * Gets the block data to toggle from the http request
	 *
	 * @param \WP_REST_Request $request Request.
	 * @return null | array block data.
	 */
	public static function get_block_data_from_request( \WP_REST_Request $request ): ?array {
		$body = json_decode( $request->get_body(), true );

		$block = array();
		if ( ! $body || strlen( $body['name'] ) <= 0 ) {
			return null;
		}

		$block['name']   = $body['name'];
		$block['parent'] = $body['parent'];

		return $block;
	}

	/**
	 * Toggles a block for a given option type
	 *
	 * @param string $option_type option type to toggle the block in.
	 * @param array  $block block to toggle.
	 *
	 * @return array Returns the block as it gets the status set.
	 */
	public static function toggle_block( string $option_type, array $block ): array {
		$block_name = $block['name'];

		$blocks = self::get_enabled_blocks_by_option( $option_type );

		$block_status = false;
		if ( isset( $blocks[ $block_name ] ) ) {
			$block_status = $blocks[ $block_name ]['status'];
		}

		if ( true === $block_status ) {
			$block['status'] = false;
			unset( $blocks[ $block_name ] );

			// When disabling a parent, ensure that all variations within are disabled as well.
			if ( null === $block['parent'] ) {
				foreach ( $blocks as $key => $value ) {
					if ( $value['parent'] === $block_name ) {
						unset( $blocks[ $key ] );
					}
				}
			}
		} else {
			$block['status']       = true;
			$blocks[ $block_name ] = $block;

			// Ensure that the parent is enabled, since we are enabling a variation.
			if ( null !== $block['parent'] && 0 >= $block['parent'] ) {
				$blocks[ $block['parent'] ] = array(
					'name'   => $block['parent'],
					'parent' => null,
					'status' => true,
				);
			}
		}

		update_network_option( null, $option_type, $blocks );

		return $block;
	}

	/**
	 * Get the Site or Sites
	 *
	 * @param ?int $blog_id optional site id; when null get all.
	 *
	 * @return array of sites, even if asked for a single one.
	 */
	public static function get_site_info( int $blog_id = null ): array {
		$query = array(
			'deleted'  => '0',
			'spam'     => '0',
			'archived' => '0',
			'orderby'  => array( 'domain', 'path' ),
			'order'    => 'ASC',
			'number'   => '10000',
		);

		if ( null !== $blog_id ) {
			$query['ID'] = $blog_id;
		}

		return get_sites( $query );
	}

	/**
	 * Get the Site or Sites with limited information, intended for json output
	 *
	 * @param ?int $blog_id optional site id; when null get all.
	 *
	 * @return array of sites, even if asked for a single one.
	 */
	public static function get_sites_abbreviated( int $blog_id = null ): array {
		$sites             = self::get_site_info( $blog_id );
		$sites_abbreviated = array();
		foreach ( $sites as $site => $value ) {
			$sites_abbreviated[] = array(
				'blog_id' => $value->blog_id,
				'domain'  => $value->domain,
				'path'    => $value->path,
			);
		}

		return $sites_abbreviated;
	}


	/**
	 * Does the given blog_id exist
	 *
	 * @param int $blog_id Blog ID.
	 *
	 * @return bool
	 */
	public static function does_site_exist( int $blog_id ) {
		$query = array(
			'deleted'  => '0',
			'spam'     => '0',
			'archived' => '0',
			'number'   => '1',
			'count'    => true,
			'ID'       => $blog_id,
		);

		return get_sites( $query ) > 0;
	}
}
