/**
 * Filters the gutenburg blocks.
 *
 * Intended to be enqueued by WordPress/Plugin.
 *
 * @file Gutenburg Block Filter.
 */

function blocks_available () {
	const blocks = wp.blocks.getBlockTypes();

	return !!(blocks && Array.isArray(blocks) && blocks.length > 0);
}

function get_all_blocks () {
	if (blocks_available()) {
		return wp.blocks.getBlockTypes();
	}

	return [];
}

function process_block (block) {
	let enabledBlockVariations = UMS_BLOCK_MANAGER_ENABLED_BLOCKS_ALL.filter(b => b.parent === block.name);
	block.variations.forEach(
		function (variation) {
			if (enabledBlockVariations.filter(bv => bv.name === variation.name).length === 0) {
				wp.blocks.unregisterBlockVariation(block.name, variation.name);
			}
		}
	);

	if (UMS_BLOCK_MANAGER_ENABLED_BLOCKS_ALL.filter(b => b.name === block.name).length === 0) {
		wp.blocks.unregisterBlockType(block.name);
	}
}

function disable_gutenburg_blocks () {
	let blocks = get_all_blocks();

	blocks.forEach(process_block);
}

wp.domReady(
	function () {
		let timeout = 500;
		if(blocks_available()){
			timeout = 0;
		}
		setTimeout(disable_gutenburg_blocks, timeout);
	}
);
