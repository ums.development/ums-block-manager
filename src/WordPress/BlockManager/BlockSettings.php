<?php
/**
 * Code for the Block Settings
 *
 * @package UMS\WordPress\BlockManager
 */

namespace UMS\WordPress\BlockManager;

/**
 * Code for the Block Settings
 */
class BlockSettings {



	/**
	 * Add the Admin Settings Page
	 *
	 * @throws \Twig\Error\LoaderError Loader Error.
	 * @throws \Twig\Error\RuntimeError Runtime Error.
	 * @throws \Twig\Error\SyntaxError Syntax Error.
	 */
	public static function add_admin_settings_page() {
		$te = Resources::get_instance()->get_template_engine();
        // phpcs:disable WordPress.Security.EscapeOutput.OutputNotEscaped
		echo $te->render(
			'blockSettings.twig',
			array(
				'enabledBlocks'      => Helpers::get_enabled_blocks_by_option_as_json( Constants::BLOCK_STATUS_OPTION ),
				'enabledExtraBlocks' => Helpers::get_enabled_blocks_by_option_as_json( Constants::EXTRA_BLOCK_STATUS_OPTION ),
			)
		);
        // phpcs:enable
	}

	/**
	 * Create the Network Admin Settings Page
	 *
	 * @return void
	 */
	public static function create_admin_settings_page() {
		if ( ! is_super_admin() ) {
			// only network admins have access to this setting.
			return;
		}

		add_submenu_page(
			get_admin_page_parent( 'settings.php' ),
			'UMS Block Manager',
			'UMS Block Manager',
			'manage_options',
			Constants::SLUG . '_block-settings',
			array( __NAMESPACE__ . '\BlockSettings', 'add_admin_settings_page' )
		);
	}

	/**
	 * Enqueue scripts/styles for the plugin
	 *
	 * @param string $hook The current page.
	 * @return void
	 */
	public static function enqueue( string $hook ) {

		if ( 'settings_page_ums-block-manager_block-settings' !== $hook ) {
			return;
		}

		do_action( 'enqueue_block_editor_assets' );
		wp_dequeue_script( Constants::SLUG . '-block_filter' );

		self::load_block_categories_provided_by_third_party_plugins();
		self::load_editor_scripts_provided_by_third_party_plugins();

		wp_enqueue_style(
			Constants::SLUG . '-network-admin-styles',
			plugins_url(
				'../../../dist/' . Constants::SLUG . '-network-admin.css',
				__FILE__
			),
			array(),
			Constants::VERSION
		);

		wp_enqueue_script(
			Constants::SLUG . '-network-admin-scripts',
			plugins_url( '../../../dist/' . Constants::SLUG . '-network-admin.js', __FILE__ ),
			array( 'wp-blocks', 'wp-element', 'wp-data', 'wp-edit-post', 'wp-components', 'wp-block-library' ),
			Constants::VERSION,
			true
		);

		wp_enqueue_style(
			Constants::SLUG . '-styles',
			plugins_url(
				'../../../assets/styles.css',
				__FILE__
			),
			array(),
			Constants::VERSION
		);
	}

	/**
	 * Loads any custom created categories from any 3rd party plugins
	 *
	 * @return void
	 */
	public static function load_block_categories_provided_by_third_party_plugins() {
		$block_categories = array();
		if ( function_exists( 'get_block_categories' ) ) {
			$block_categories = get_block_categories( get_post() );
		}

		wp_add_inline_script( 'wp-blocks', sprintf( 'wp.blocks.setCategories( %s );', wp_json_encode( $block_categories ) ), 'after' );
	}


	/**
	 * Loads the editor scripts for any 3rd party plugins that have them defined
	 *
	 * @return void
	 */
	public static function load_editor_scripts_provided_by_third_party_plugins() {
		$block_registry = \WP_Block_Type_Registry::get_instance();
		foreach ( $block_registry->get_all_registered() as $block_name => $block_type ) {
			if ( ! empty( $block_type->editor_script ) ) {
				wp_enqueue_script( $block_type->editor_script );
			}
		}
	}



}
