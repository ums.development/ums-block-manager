<?php
/*
Plugin Name: UMS Block Manager (Public Release)
Plugin URI:
Description: Plugin to manage a whitelist of Gutenberg blocks allowed to be used on UMS sites.
Version: 1.3.0
Licence: GPV v2 or Later
Author: UMS Web Development Team
Author URI: https://gojira.its.maine.edu/confluence/display/WB/
*/

/**
 * UMS Block Manager WP Plugin
 *
 * @package ums-block-manager
 */

use UMS\WordPress\BlockManager\API\EnabledSites;
use UMS\WordPress\BlockManager\API\ExtraToggle;
use UMS\WordPress\BlockManager\API\Site;
use UMS\WordPress\BlockManager\API\Toggle;
use UMS\WordPress\BlockManager\Constants;
use UMS\WordPress\BlockManager\Helpers;

require_once __DIR__ . '/vendor/autoload.php';

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}


/**
 * Initialize the plugin
 *
 * @return void
 */
function ums_block_manager_init() {
	include_once ABSPATH . 'wp-admin/includes/plugin.php';
	if ( is_plugin_active( 'gutenberg/gutenberg.php' ) || version_compare( get_bloginfo( 'version' ), Constants::MIN_WP_VERSION, '>=' ) ) {
		if ( Helpers::has_access() ) {
			add_action( 'network_admin_menu', array( 'UMS\WordPress\BlockManager\BlockSettings', 'create_admin_settings_page' ) );
			add_action( 'admin_enqueue_scripts', array( 'UMS\WordPress\BlockManager\BlockSettings', 'enqueue' ) );
		}
		add_action( 'enqueue_block_editor_assets', array( 'UMS\WordPress\BlockManager\EditorBlockFiltering', 'enqueue' ) );

		new Toggle();
		new ExtraToggle();
		new Site();
		new EnabledSites();
	}
}

add_action( 'plugins_loaded', 'ums_block_manager_init', 100 );
