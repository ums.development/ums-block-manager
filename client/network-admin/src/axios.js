import axios from "axios";

/* eslint-disable no-undef */
let Axios = axios.create({
	baseURL:
		"undefined" === typeof wpApiSettings || wpApiSettings === null
			? process.env.REACT_APP_WP_HOST
			: wpApiSettings.root,
	headers: {
		"X-WP-Nonce":
			"undefined" === typeof wpApiSettings || wpApiSettings === null
				? process.env.REACT_APP_WP_NONCE
				: wpApiSettings.nonce,
		"Content-Type": "application/json",
		...("undefined" === typeof wpApiSettings || wpApiSettings === null
			? {
					Authorization: "Basic " + btoa("admin:admin"),
			  }
			: {}),
	},
});
/* eslint-enable no-undef */

export default Axios;
