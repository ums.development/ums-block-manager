import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import axios from "../axios";

export const getAllSites = createAsyncThunk("blocks/getAllSites", async () => {
	const response = await axios.get("ums-block-manager/sites");
	return response.data;
});

export const sitesSlice = createSlice({
	name: "sites",
	initialState: {
		sites: [],
		isFetching: false,
	},
	reducers: {},
	extraReducers: (builder) => {
		builder.addCase(getAllSites.pending, (state) => {
			state.isFetching = true;
		});
		builder.addCase(getAllSites.fulfilled, (state, action) => {
			state.sites = action.payload;
			state.isFetching = false;
		});
		builder.addCase(getAllSites.rejected, (state) => {
			state.isFetching = null;
		});
	},
});
export default sitesSlice.reducer;
