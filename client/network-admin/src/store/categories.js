import { createSlice } from "@reduxjs/toolkit";
import getDefaultWPCategoryData from "./wpDefaultCategoryData";

function getGutenburgCategories() {
	/* eslint-disable no-undef */
	if ("undefined" === typeof wp) {
		return getDefaultWPCategoryData();
	} else {
		return getCategoryDataFromWP();
	}
	/* eslint-enable no-undef */
}

function getCategoryDataFromWP() {
	// Get WP Block Categories
	const categories = wp.blocks.getCategories(); //eslint-disable-line no-undef
	let wpCategories = "";

	if (categories) {
		// Sort categories by name.
		wpCategories = categories.sort(function (a, b) {
			const textA = a.title.toUpperCase();
			const textB = b.title.toUpperCase();
			return textA < textB ? -1 : textA > textB ? 1 : 0;
		});
	}

	return wpCategories;
}

export const categoriesSlice = createSlice({
	name: "categories",
	initialState: {
		categories: getGutenburgCategories(),
	},
	reducers: {},
});
export default categoriesSlice.reducer;
