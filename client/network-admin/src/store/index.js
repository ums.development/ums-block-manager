import { configureStore } from "@reduxjs/toolkit";
import blocksReducer from "./blocks";
import categoriesReducer from "./categories";
import sites from "./sites";
import enabledSites from "./enabledSites";

export default configureStore({
	reducer: {
		blocks: blocksReducer,
		categories: categoriesReducer,
		sites: sites,
		enabledSites: enabledSites,
	},
});
