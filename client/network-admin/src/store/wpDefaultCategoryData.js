export default function getDefaultWPBlockData() {
	return [
		{
			slug: "text",
			title: "Text",
		},
		{
			slug: "media",
			title: "Media",
		},
		{
			slug: "design",
			title: "Design",
		},
		{
			slug: "widgets",
			title: "Widgets",
		},
		{
			slug: "embed",
			title: "Embeds",
		},
		{
			slug: "reusable",
			title: "Reusable blocks",
		},
	];
}
