import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import getDefaultWPBlockData from "./wpDefaultBlockData";
import axios from "../axios";

/* eslint-disable no-undef */
if (wp && wp.blockLibrary) {
	// Load Block Library.
	wp.blockLibrary.registerCoreBlocks(); //eslint-disable-line no-undef
}
/* eslint-enable no-undef */

export const toggleBlock = createAsyncThunk(
	"blocks/toggleBlock",
	async (payload, thunkAPI) => {
		const response = await axios.post("ums-block-manager/toggle", payload);
		return response.data;
	}
);

export const toggleExtraBlock = createAsyncThunk(
	"blocks/extraToggleBlock",
	async (payload, thunkAPI) => {
		const response = await axios.post(
			"ums-block-manager/extra-toggle",
			payload
		);
		return response.data;
	}
);

function getGutenburgBlocks() {
	/* eslint-disable no-undef */
	if ("undefined" === typeof wp) {
		return getDefaultWPBlockData();
	} else {
		return getBlockDataFromWP();
	}
	/* eslint-enable no-undef */
}

function getEnabledBlocks() {
	/* eslint-disable no-undef */
	if ("undefined" === typeof UMS_BLOCK_MANAGER_ENABLED_BLOCKS) {
		return [
			{ name: "core/button", status: true },
			{ name: "buttons-horizontal", status: true },
		];
	} else {
		return UMS_BLOCK_MANAGER_ENABLED_BLOCKS;
	}
	/* eslint-enable no-undef */
}

function getEnabledExtraBlocks() {
	/* eslint-disable no-undef */
	if ("undefined" === typeof UMS_BLOCK_MANAGER_ENABLED_EXTRA_BLOCKS) {
		return [
			{ name: "core/columns", status: true },
			{ name: "core/more", status: true },
		];
	} else {
		return UMS_BLOCK_MANAGER_ENABLED_EXTRA_BLOCKS;
	}
	/* eslint-enable no-undef */
}

function getBlockDataFromWP() {
	// Get WP Block Info.
	const blocks = wp.blocks.getBlockTypes(); //eslint-disable-line no-undef
	let wpBlocks = "";
	if (blocks) {
		// Sort blocks by name.
		wpBlocks = blocks.sort(function (a, b) {
			const textA = a.title.toUpperCase();
			const textB = b.title.toUpperCase();
			return textA < textB ? -1 : textA > textB ? 1 : 0;
		});
	}

	return wpBlocks;
}

function getUnregisteredBlocks() {
	let allBlocks = getGutenburgBlocks();
	let enabledBlocks = getEnabledBlocks();
	return enabledBlocks.filter(
		(enabledBlock) =>
			!allBlocks.find(
				(allBlock) =>
					allBlock.name === enabledBlock.name &&
					allBlock.parent === enabledBlock.parent
			)
	);
}

function getUnregisteredExtraBlocks() {
	let allBlocks = getGutenburgBlocks();
	let enabledBlocks = getEnabledExtraBlocks();
	return enabledBlocks.filter(
		(enabledBlock) =>
			!allBlocks.find(
				(allBlock) =>
					allBlock.name === enabledBlock.name &&
					allBlock.parent === enabledBlock.parent
			)
	);
}

export const blocksSlice = createSlice({
	name: "blocks",
	initialState: {
		blocks: getGutenburgBlocks(),
		enabledBlocks: getEnabledBlocks(),
		enabledExtraBlocks: getEnabledExtraBlocks(),
		unregisteredBlocks: getUnregisteredBlocks(),
		unregisteredExtraBlocks: getUnregisteredExtraBlocks(),
	},
	reducers: {
		addUnregisteredBlock: (state, action) => {
			state.unregisteredBlocks.push(action.payload);
		},
		removeUnregisteredBlock: (state, action) => {
			state.unregisteredBlocks = state.unregisteredBlocks.filter(
				(eb) =>
					eb.name !== action.payload.name || eb.parent !== action.payload.parent
			);
		},
		addUnregisteredExtraBlock: (state, action) => {
			state.unregisteredExtraBlocks.push(action.payload);
		},
		removeUnregisteredExtraBlock: (state, action) => {
			state.unregisteredExtraBlocks = state.unregisteredExtraBlocks.filter(
				(eb) =>
					eb.name !== action.payload.name || eb.parent !== action.payload.parent
			);
		},
	},
});
export const {
	addUnregisteredBlock,
	removeUnregisteredBlock,
	addUnregisteredExtraBlock,
	removeUnregisteredExtraBlock,
} = blocksSlice.actions;
export default blocksSlice.reducer;
