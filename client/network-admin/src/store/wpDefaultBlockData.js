export default function getDefaultWPBlockData() {
	return [
		{
			name: "core/paragraph",
			icon: {
				src: {
					key: null,
					ref: null,
					props: {
						xmlns: "http://www.w3.org/2000/svg",
						viewBox: "0 0 24 24",
						children: {
							key: null,
							ref: null,
							props: {
								d: "M18.3 4H9.9v-.1l-.9.2c-2.3.4-4 2.4-4 4.8s1.7 4.4 4 4.8l.7.1V20h1.5V5.5h2.9V20h1.5V5.5h2.7V4z",
							},
							_owner: null,
						},
					},
					_owner: null,
				},
			},
			keywords: ["text"],
			attributes: {
				align: {
					type: "string",
				},
				content: {
					type: "string",
					source: "html",
					selector: "p",
					default: "",
				},
				dropCap: {
					type: "boolean",
					default: false,
				},
				placeholder: {
					type: "string",
				},
				direction: {
					type: "string",
					enum: ["ltr", "rtl"],
				},
				anchor: {
					type: "string",
					source: "attribute",
					attribute: "id",
					selector: "*",
				},
				className: {
					type: "string",
				},
				backgroundColor: {
					type: "string",
				},
				textColor: {
					type: "string",
				},
				fontSize: {
					type: "string",
				},
				style: {
					type: "object",
				},
			},
			providesContext: {},
			usesContext: [],
			supports: {
				anchor: true,
				className: false,
				color: {
					link: true,
				},
				fontSize: true,
				lineHeight: true,
				__experimentalSelector: "p",
				__unstablePasteTextInline: true,
			},
			styles: [],
			apiVersion: 2,
			category: "text",
			editorStyle: "wp-block-paragraph-editor",
			style: "wp-block-paragraph",
			title: "Paragraph",
			description: "Start with the building block of all narrative.",
			example: {
				attributes: {
					content:
						"In a village of La Mancha, the name of which I have no desire to call to mind, there lived not long since one of those gentlemen that keep a lance in the lance-rack, an old buckler, a lean hack, and a greyhound for coursing.",
					style: {
						typography: {
							fontSize: 28,
						},
					},
					dropCap: true,
				},
			},
			transforms: {
				from: [
					{
						type: "raw",
						priority: 20,
						selector: "p",
					},
				],
			},
			deprecated: [
				{
					attributes: {
						align: {
							type: "string",
						},
						content: {
							type: "string",
							source: "html",
							selector: "p",
							default: "",
						},
						dropCap: {
							type: "boolean",
							default: false,
						},
						placeholder: {
							type: "string",
						},
						textColor: {
							type: "string",
						},
						backgroundColor: {
							type: "string",
						},
						fontSize: {
							type: "string",
						},
						direction: {
							type: "string",
							enum: ["ltr", "rtl"],
						},
						customTextColor: {
							type: "string",
						},
						customBackgroundColor: {
							type: "string",
						},
						customFontSize: {
							type: "number",
						},
						className: {
							type: "string",
						},
					},
					supports: {
						className: false,
					},
				},
				{
					attributes: {
						align: {
							type: "string",
						},
						content: {
							type: "string",
							source: "html",
							selector: "p",
							default: "",
						},
						dropCap: {
							type: "boolean",
							default: false,
						},
						placeholder: {
							type: "string",
						},
						textColor: {
							type: "string",
						},
						backgroundColor: {
							type: "string",
						},
						fontSize: {
							type: "string",
						},
						direction: {
							type: "string",
							enum: ["ltr", "rtl"],
						},
						customTextColor: {
							type: "string",
						},
						customBackgroundColor: {
							type: "string",
						},
						customFontSize: {
							type: "number",
						},
						className: {
							type: "string",
						},
					},
					supports: {
						className: false,
					},
				},
				{
					attributes: {
						align: {
							type: "string",
						},
						content: {
							type: "string",
							source: "html",
							selector: "p",
							default: "",
						},
						dropCap: {
							type: "boolean",
							default: false,
						},
						placeholder: {
							type: "string",
						},
						textColor: {
							type: "string",
						},
						backgroundColor: {
							type: "string",
						},
						fontSize: {
							type: "string",
						},
						direction: {
							type: "string",
							enum: ["ltr", "rtl"],
						},
						customTextColor: {
							type: "string",
						},
						customBackgroundColor: {
							type: "string",
						},
						customFontSize: {
							type: "number",
						},
						width: {
							type: "string",
						},
						className: {
							type: "string",
						},
					},
					supports: {
						className: false,
					},
				},
				{
					attributes: {
						align: {
							type: "string",
						},
						content: {
							type: "string",
							source: "html",
							selector: "p",
							default: "",
						},
						dropCap: {
							type: "boolean",
							default: false,
						},
						placeholder: {
							type: "string",
						},
						textColor: {
							type: "string",
						},
						backgroundColor: {
							type: "string",
						},
						fontSize: {
							type: "number",
						},
						direction: {
							type: "string",
							enum: ["ltr", "rtl"],
						},
						className: {
							type: "string",
						},
					},
					supports: {
						className: false,
					},
				},
				{
					attributes: {
						align: {
							type: "string",
						},
						content: {
							type: "string",
							source: "html",
							default: "",
						},
						dropCap: {
							type: "boolean",
							default: false,
						},
						placeholder: {
							type: "string",
						},
						textColor: {
							type: "string",
						},
						backgroundColor: {
							type: "string",
						},
						fontSize: {
							type: "string",
						},
						direction: {
							type: "string",
							enum: ["ltr", "rtl"],
						},
						style: {
							type: "object",
						},
						className: {
							type: "string",
						},
					},
					supports: {
						className: false,
					},
				},
			],
			variations: [],
		},
		{
			name: "core/image",
			icon: {
				src: {
					key: null,
					ref: null,
					props: {
						viewBox: "0 0 24 24",
						xmlns: "http://www.w3.org/2000/svg",
						children: {
							key: null,
							ref: null,
							props: {
								d: "M19 3H5c-1.1 0-2 .9-2 2v14c0 1.1.9 2 2 2h14c1.1 0 2-.9 2-2V5c0-1.1-.9-2-2-2zM5 4.5h14c.3 0 .5.2.5.5v8.4l-3-2.9c-.3-.3-.8-.3-1 0L11.9 14 9 12c-.3-.2-.6-.2-.8 0l-3.6 2.6V5c-.1-.3.1-.5.4-.5zm14 15H5c-.3 0-.5-.2-.5-.5v-2.4l4.1-3 3 1.9c.3.2.7.2.9-.1L16 12l3.5 3.4V19c0 .3-.2.5-.5.5z",
							},
							_owner: null,
						},
					},
					_owner: null,
				},
			},
			keywords: ["img", "photo", "picture"],
			attributes: {
				align: {
					type: "string",
				},
				url: {
					type: "string",
					source: "attribute",
					selector: "img",
					attribute: "src",
				},
				alt: {
					type: "string",
					source: "attribute",
					selector: "img",
					attribute: "alt",
					default: "",
				},
				caption: {
					type: "string",
					source: "html",
					selector: "figcaption",
				},
				title: {
					type: "string",
					source: "attribute",
					selector: "img",
					attribute: "title",
				},
				href: {
					type: "string",
					source: "attribute",
					selector: "figure > a",
					attribute: "href",
				},
				rel: {
					type: "string",
					source: "attribute",
					selector: "figure > a",
					attribute: "rel",
				},
				linkClass: {
					type: "string",
					source: "attribute",
					selector: "figure > a",
					attribute: "class",
				},
				id: {
					type: "number",
				},
				width: {
					type: "number",
				},
				height: {
					type: "number",
				},
				sizeSlug: {
					type: "string",
				},
				linkDestination: {
					type: "string",
				},
				linkTarget: {
					type: "string",
					source: "attribute",
					selector: "figure > a",
					attribute: "target",
				},
				anchor: {
					type: "string",
					source: "attribute",
					attribute: "id",
					selector: "*",
				},
				className: {
					type: "string",
				},
				style: {
					type: "object",
				},
			},
			providesContext: {},
			usesContext: [],
			supports: {
				anchor: true,
				__experimentalBorder: {
					radius: true,
				},
			},
			styles: [
				{
					name: "default",
					label: "Default",
					isDefault: true,
				},
				{
					name: "rounded",
					label: "Rounded",
				},
			],
			apiVersion: 2,
			category: "media",
			editorStyle: "wp-block-image-editor",
			style: "wp-block-image",
			title: "Image",
			description: "Insert an image to make a visual statement.",
			example: {
				attributes: {
					sizeSlug: "large",
					url: "https://s.w.org/images/core/5.3/MtBlanc1.jpg",
					caption: "Mont Blanc appears—still, snowy, and serene.",
				},
			},
			transforms: {
				from: [
					{
						type: "raw",
					},
					{
						type: "files",
					},
					{
						type: "shortcode",
						tag: "caption",
						attributes: {
							url: {
								type: "string",
								source: "attribute",
								attribute: "src",
								selector: "img",
							},
							alt: {
								type: "string",
								source: "attribute",
								attribute: "alt",
								selector: "img",
							},
							caption: {},
							href: {},
							rel: {},
							linkClass: {},
							id: {
								type: "number",
							},
							align: {
								type: "string",
							},
						},
					},
				],
			},
			deprecated: [
				{
					attributes: {
						align: {
							type: "string",
						},
						url: {
							type: "string",
							source: "attribute",
							selector: "img",
							attribute: "src",
						},
						alt: {
							type: "string",
							source: "attribute",
							selector: "img",
							attribute: "alt",
							default: "",
						},
						caption: {
							type: "string",
							source: "html",
							selector: "figcaption",
						},
						href: {
							type: "string",
							source: "attribute",
							selector: "figure > a",
							attribute: "href",
						},
						rel: {
							type: "string",
							source: "attribute",
							selector: "figure > a",
							attribute: "rel",
						},
						linkClass: {
							type: "string",
							source: "attribute",
							selector: "figure > a",
							attribute: "class",
						},
						id: {
							type: "number",
						},
						width: {
							type: "number",
						},
						height: {
							type: "number",
						},
						linkDestination: {
							type: "string",
						},
						linkTarget: {
							type: "string",
							source: "attribute",
							selector: "figure > a",
							attribute: "target",
						},
						className: {
							type: "string",
						},
					},
				},
				{
					attributes: {
						align: {
							type: "string",
						},
						url: {
							type: "string",
							source: "attribute",
							selector: "img",
							attribute: "src",
						},
						alt: {
							type: "string",
							source: "attribute",
							selector: "img",
							attribute: "alt",
							default: "",
						},
						caption: {
							type: "string",
							source: "html",
							selector: "figcaption",
						},
						href: {
							type: "string",
							source: "attribute",
							selector: "figure > a",
							attribute: "href",
						},
						rel: {
							type: "string",
							source: "attribute",
							selector: "figure > a",
							attribute: "rel",
						},
						linkClass: {
							type: "string",
							source: "attribute",
							selector: "figure > a",
							attribute: "class",
						},
						id: {
							type: "number",
						},
						width: {
							type: "number",
						},
						height: {
							type: "number",
						},
						linkDestination: {
							type: "string",
						},
						linkTarget: {
							type: "string",
							source: "attribute",
							selector: "figure > a",
							attribute: "target",
						},
						className: {
							type: "string",
						},
					},
				},
				{
					attributes: {
						align: {
							type: "string",
						},
						url: {
							type: "string",
							source: "attribute",
							selector: "img",
							attribute: "src",
						},
						alt: {
							type: "string",
							source: "attribute",
							selector: "img",
							attribute: "alt",
							default: "",
						},
						caption: {
							type: "string",
							source: "html",
							selector: "figcaption",
						},
						href: {
							type: "string",
							source: "attribute",
							selector: "figure > a",
							attribute: "href",
						},
						rel: {
							type: "string",
							source: "attribute",
							selector: "figure > a",
							attribute: "rel",
						},
						linkClass: {
							type: "string",
							source: "attribute",
							selector: "figure > a",
							attribute: "class",
						},
						id: {
							type: "number",
						},
						width: {
							type: "number",
						},
						height: {
							type: "number",
						},
						linkDestination: {
							type: "string",
						},
						linkTarget: {
							type: "string",
							source: "attribute",
							selector: "figure > a",
							attribute: "target",
						},
						className: {
							type: "string",
						},
					},
				},
			],
			variations: [],
		},
		{
			name: "core/heading",
			icon: {
				src: {
					key: null,
					ref: null,
					props: {
						xmlns: "http://www.w3.org/2000/svg",
						viewBox: "0 0 24 24",
						children: {
							key: null,
							ref: null,
							props: {
								d: "M6.2 5.2v13.4l5.8-4.8 5.8 4.8V5.2z",
							},
							_owner: null,
						},
					},
					_owner: null,
				},
			},
			keywords: ["title", "subtitle"],
			attributes: {
				textAlign: {
					type: "string",
				},
				content: {
					type: "string",
					source: "html",
					selector: "h1,h2,h3,h4,h5,h6",
					default: "",
				},
				level: {
					type: "number",
					default: 2,
				},
				placeholder: {
					type: "string",
				},
				align: {
					type: "string",
					enum: ["left", "center", "right", "wide", "full", ""],
				},
				anchor: {
					type: "string",
					source: "attribute",
					attribute: "id",
					selector: "*",
				},
				className: {
					type: "string",
				},
				backgroundColor: {
					type: "string",
				},
				textColor: {
					type: "string",
				},
				fontSize: {
					type: "string",
				},
				style: {
					type: "object",
				},
			},
			providesContext: {},
			usesContext: [],
			supports: {
				align: ["wide", "full"],
				anchor: true,
				className: false,
				color: {
					link: true,
				},
				fontSize: true,
				lineHeight: true,
				__experimentalSelector: {
					"core/heading/h1": {
						selector: "h1",
						title: "h1",
						attributes: {
							level: 1,
						},
					},
					"core/heading/h2": {
						selector: "h2",
						title: "h2",
						attributes: {
							level: 2,
						},
					},
					"core/heading/h3": {
						selector: "h3",
						title: "h3",
						attributes: {
							level: 3,
						},
					},
					"core/heading/h4": {
						selector: "h4",
						title: "h4",
						attributes: {
							level: 4,
						},
					},
					"core/heading/h5": {
						selector: "h5",
						title: "h5",
						attributes: {
							level: 5,
						},
					},
					"core/heading/h6": {
						selector: "h6",
						title: "h6",
						attributes: {
							level: 6,
						},
					},
				},
				__unstablePasteTextInline: true,
			},
			styles: [],
			apiVersion: 2,
			category: "text",
			editorStyle: "wp-block-heading-editor",
			style: "wp-block-heading",
			title: "Heading",
			description:
				"Introduce new sections and organize content to help visitors (and search engines) understand the structure of your content.",
			example: {
				attributes: {
					content: "Code is Poetry",
					level: 2,
				},
			},
			transforms: {
				from: [
					{
						type: "block",
						isMultiBlock: true,
						blocks: ["core/paragraph"],
					},
					{
						type: "raw",
						selector: "h1,h2,h3,h4,h5,h6",
					},
					{
						type: "prefix",
						prefix: "#",
					},
					{
						type: "prefix",
						prefix: "##",
					},
					{
						type: "prefix",
						prefix: "###",
					},
					{
						type: "prefix",
						prefix: "####",
					},
					{
						type: "prefix",
						prefix: "#####",
					},
					{
						type: "prefix",
						prefix: "######",
					},
					{
						type: "enter",
						regExp: {},
					},
					{
						type: "enter",
						regExp: {},
					},
					{
						type: "enter",
						regExp: {},
					},
					{
						type: "enter",
						regExp: {},
					},
					{
						type: "enter",
						regExp: {},
					},
					{
						type: "enter",
						regExp: {},
					},
				],
				to: [
					{
						type: "block",
						isMultiBlock: true,
						blocks: ["core/paragraph"],
					},
				],
			},
			deprecated: [
				{
					attributes: {
						align: {
							type: "string",
						},
						content: {
							type: "string",
							source: "html",
							selector: "h1,h2,h3,h4,h5,h6",
							default: "",
						},
						level: {
							type: "number",
							default: 2,
						},
						placeholder: {
							type: "string",
						},
						anchor: {
							type: "string",
							source: "attribute",
							attribute: "id",
							selector: "*",
						},
						className: {
							type: "string",
						},
						backgroundColor: {
							type: "string",
						},
						textColor: {
							type: "string",
						},
						fontSize: {
							type: "string",
						},
						style: {
							type: "object",
						},
					},
					supports: {
						align: ["wide", "full"],
						anchor: true,
						className: false,
						color: {
							link: true,
						},
						fontSize: true,
						lineHeight: true,
						__experimentalSelector: {
							"core/heading/h1": "h1",
							"core/heading/h2": "h2",
							"core/heading/h3": "h3",
							"core/heading/h4": "h4",
							"core/heading/h5": "h5",
							"core/heading/h6": "h6",
						},
						__unstablePasteTextInline: true,
					},
				},
				{
					attributes: {
						align: {
							type: "string",
						},
						content: {
							type: "string",
							source: "html",
							selector: "h1,h2,h3,h4,h5,h6",
							default: "",
						},
						level: {
							type: "number",
							default: 2,
						},
						placeholder: {
							type: "string",
						},
						customTextColor: {
							type: "string",
						},
						textColor: {
							type: "string",
						},
						anchor: {
							type: "string",
							source: "attribute",
							attribute: "id",
							selector: "*",
						},
						className: {
							type: "string",
						},
					},
					supports: {
						className: false,
						anchor: true,
					},
				},
				{
					attributes: {
						align: {
							type: "string",
						},
						content: {
							type: "string",
							source: "html",
							selector: "h1,h2,h3,h4,h5,h6",
							default: "",
						},
						level: {
							type: "number",
							default: 2,
						},
						placeholder: {
							type: "string",
						},
						customTextColor: {
							type: "string",
						},
						textColor: {
							type: "string",
						},
						anchor: {
							type: "string",
							source: "attribute",
							attribute: "id",
							selector: "*",
						},
						className: {
							type: "string",
						},
					},
					supports: {
						className: false,
						anchor: true,
					},
				},
				{
					attributes: {
						align: {
							type: "string",
						},
						content: {
							type: "string",
							source: "html",
							selector: "h1,h2,h3,h4,h5,h6",
							default: "",
						},
						level: {
							type: "number",
							default: 2,
						},
						placeholder: {
							type: "string",
						},
						customTextColor: {
							type: "string",
						},
						textColor: {
							type: "string",
						},
						anchor: {
							type: "string",
							source: "attribute",
							attribute: "id",
							selector: "*",
						},
						className: {
							type: "string",
						},
					},
					supports: {
						className: false,
						anchor: true,
					},
				},
			],
			variations: [],
		},
		{
			name: "core/gallery",
			icon: {
				src: {
					key: null,
					ref: null,
					props: {
						viewBox: "0 0 24 24",
						xmlns: "http://www.w3.org/2000/svg",
						children: {
							key: null,
							ref: null,
							props: {
								d: "M20.2 8v11c0 .7-.6 1.2-1.2 1.2H6v1.5h13c1.5 0 2.7-1.2 2.7-2.8V8h-1.5zM18 16.4V4.6c0-.9-.7-1.6-1.6-1.6H4.6C3.7 3 3 3.7 3 4.6v11.8c0 .9.7 1.6 1.6 1.6h11.8c.9 0 1.6-.7 1.6-1.6zM4.5 4.6c0-.1.1-.1.1-.1h11.8c.1 0 .1.1.1.1V12l-2.3-1.7c-.3-.2-.6-.2-.9 0l-2.9 2.1L8 11.3c-.2-.1-.5-.1-.7 0l-2.9 1.5V4.6zm0 11.8v-1.8l3.2-1.7 2.4 1.2c.2.1.5.1.8-.1l2.8-2 2.8 2v2.5c0 .1-.1.1-.1.1H4.6c0-.1-.1-.2-.1-.2z",
							},
							_owner: null,
						},
					},
					_owner: null,
				},
			},
			keywords: ["images", "photos"],
			attributes: {
				images: {
					type: "array",
					default: [],
					source: "query",
					selector: ".blocks-gallery-item",
					query: {
						url: {
							type: "string",
							source: "attribute",
							selector: "img",
							attribute: "src",
						},
						fullUrl: {
							type: "string",
							source: "attribute",
							selector: "img",
							attribute: "data-full-url",
						},
						link: {
							type: "string",
							source: "attribute",
							selector: "img",
							attribute: "data-link",
						},
						alt: {
							type: "string",
							source: "attribute",
							selector: "img",
							attribute: "alt",
							default: "",
						},
						id: {
							type: "string",
							source: "attribute",
							selector: "img",
							attribute: "data-id",
						},
						caption: {
							type: "string",
							source: "html",
							selector: ".blocks-gallery-item__caption",
						},
					},
				},
				ids: {
					type: "array",
					items: {
						type: "number",
					},
					default: [],
				},
				columns: {
					type: "number",
					minimum: 1,
					maximum: 8,
				},
				caption: {
					type: "string",
					source: "html",
					selector: ".blocks-gallery-caption",
				},
				imageCrop: {
					type: "boolean",
					default: true,
				},
				linkTo: {
					type: "string",
				},
				sizeSlug: {
					type: "string",
					default: "large",
				},
				align: {
					type: "string",
					enum: ["left", "center", "right", "wide", "full", ""],
				},
				anchor: {
					type: "string",
					source: "attribute",
					attribute: "id",
					selector: "*",
				},
				className: {
					type: "string",
				},
			},
			providesContext: {},
			usesContext: [],
			supports: {
				anchor: true,
				align: true,
			},
			styles: [],
			apiVersion: 2,
			category: "media",
			editorStyle: "wp-block-gallery-editor",
			style: "wp-block-gallery",
			title: "Gallery",
			description: "Display multiple images in a rich gallery.",
			example: {
				attributes: {
					columns: 2,
					images: [
						{
							url: "https://s.w.org/images/core/5.3/Glacial_lakes%2C_Bhutan.jpg",
						},
						{
							url: "https://s.w.org/images/core/5.3/Sediment_off_the_Yucatan_Peninsula.jpg",
						},
					],
				},
			},
			transforms: {
				from: [
					{
						type: "block",
						isMultiBlock: true,
						blocks: ["core/image"],
					},
					{
						type: "shortcode",
						tag: "gallery",
						attributes: {
							images: {
								type: "array",
							},
							ids: {
								type: "array",
							},
							columns: {
								type: "number",
							},
							linkTo: {
								type: "string",
							},
						},
					},
					{
						type: "files",
					},
				],
				to: [
					{
						type: "block",
						blocks: ["core/image"],
					},
				],
			},
			deprecated: [
				{
					attributes: {
						images: {
							type: "array",
							default: [],
							source: "query",
							selector: ".blocks-gallery-item",
							query: {
								url: {
									type: "string",
									source: "attribute",
									selector: "img",
									attribute: "src",
								},
								fullUrl: {
									type: "string",
									source: "attribute",
									selector: "img",
									attribute: "data-full-url",
								},
								link: {
									type: "string",
									source: "attribute",
									selector: "img",
									attribute: "data-link",
								},
								alt: {
									type: "string",
									source: "attribute",
									selector: "img",
									attribute: "alt",
									default: "",
								},
								id: {
									type: "string",
									source: "attribute",
									selector: "img",
									attribute: "data-id",
								},
								caption: {
									type: "string",
									source: "html",
									selector: ".blocks-gallery-item__caption",
								},
							},
						},
						ids: {
							type: "array",
							items: {
								type: "number",
							},
							default: [],
						},
						columns: {
							type: "number",
							minimum: 1,
							maximum: 8,
						},
						caption: {
							type: "string",
							source: "html",
							selector: ".blocks-gallery-caption",
						},
						imageCrop: {
							type: "boolean",
							default: true,
						},
						linkTo: {
							type: "string",
							default: "none",
						},
						sizeSlug: {
							type: "string",
							default: "large",
						},
						align: {
							type: "string",
							enum: ["left", "center", "right", "wide", "full", ""],
						},
						className: {
							type: "string",
						},
					},
					supports: {
						align: true,
					},
				},
				{
					attributes: {
						images: {
							type: "array",
							default: [],
							source: "query",
							selector: ".blocks-gallery-item",
							query: {
								url: {
									source: "attribute",
									selector: "img",
									attribute: "src",
								},
								fullUrl: {
									source: "attribute",
									selector: "img",
									attribute: "data-full-url",
								},
								link: {
									source: "attribute",
									selector: "img",
									attribute: "data-link",
								},
								alt: {
									source: "attribute",
									selector: "img",
									attribute: "alt",
									default: "",
								},
								id: {
									source: "attribute",
									selector: "img",
									attribute: "data-id",
								},
								caption: {
									type: "string",
									source: "html",
									selector: ".blocks-gallery-item__caption",
								},
							},
						},
						ids: {
							type: "array",
							default: [],
						},
						columns: {
							type: "number",
						},
						caption: {
							type: "string",
							source: "html",
							selector: ".blocks-gallery-caption",
						},
						imageCrop: {
							type: "boolean",
							default: true,
						},
						linkTo: {
							type: "string",
							default: "none",
						},
						align: {
							type: "string",
							enum: ["left", "center", "right", "wide", "full", ""],
						},
						className: {
							type: "string",
						},
					},
					supports: {
						align: true,
					},
				},
				{
					attributes: {
						images: {
							type: "array",
							default: [],
							source: "query",
							selector: "ul.wp-block-gallery .blocks-gallery-item",
							query: {
								url: {
									source: "attribute",
									selector: "img",
									attribute: "src",
								},
								fullUrl: {
									source: "attribute",
									selector: "img",
									attribute: "data-full-url",
								},
								alt: {
									source: "attribute",
									selector: "img",
									attribute: "alt",
									default: "",
								},
								id: {
									source: "attribute",
									selector: "img",
									attribute: "data-id",
								},
								link: {
									source: "attribute",
									selector: "img",
									attribute: "data-link",
								},
								caption: {
									type: "array",
									source: "children",
									selector: "figcaption",
								},
							},
						},
						ids: {
							type: "array",
							default: [],
						},
						columns: {
							type: "number",
						},
						imageCrop: {
							type: "boolean",
							default: true,
						},
						linkTo: {
							type: "string",
							default: "none",
						},
						align: {
							type: "string",
							enum: ["left", "center", "right", "wide", "full", ""],
						},
						className: {
							type: "string",
						},
					},
					supports: {
						align: true,
					},
				},
				{
					attributes: {
						images: {
							type: "array",
							default: [],
							source: "query",
							selector: "ul.wp-block-gallery .blocks-gallery-item",
							query: {
								url: {
									source: "attribute",
									selector: "img",
									attribute: "src",
								},
								alt: {
									source: "attribute",
									selector: "img",
									attribute: "alt",
									default: "",
								},
								id: {
									source: "attribute",
									selector: "img",
									attribute: "data-id",
								},
								link: {
									source: "attribute",
									selector: "img",
									attribute: "data-link",
								},
								caption: {
									type: "array",
									source: "children",
									selector: "figcaption",
								},
							},
						},
						columns: {
							type: "number",
						},
						imageCrop: {
							type: "boolean",
							default: true,
						},
						linkTo: {
							type: "string",
							default: "none",
						},
						align: {
							type: "string",
							enum: ["left", "center", "right", "wide", "full", ""],
						},
						className: {
							type: "string",
						},
					},
					supports: {
						align: true,
					},
				},
				{
					attributes: {
						images: {
							type: "array",
							default: [],
							source: "query",
							selector: "div.wp-block-gallery figure.blocks-gallery-image img",
							query: {
								url: {
									source: "attribute",
									attribute: "src",
								},
								alt: {
									source: "attribute",
									attribute: "alt",
									default: "",
								},
								id: {
									source: "attribute",
									attribute: "data-id",
								},
							},
						},
						columns: {
							type: "number",
						},
						imageCrop: {
							type: "boolean",
							default: true,
						},
						linkTo: {
							type: "string",
							default: "none",
						},
						align: {
							type: "string",
							default: "none",
						},
						className: {
							type: "string",
						},
					},
					supports: {
						align: true,
					},
				},
			],
			variations: [],
		},
		{
			name: "core/list",
			icon: {
				src: {
					key: null,
					ref: null,
					props: {
						viewBox: "0 0 24 24",
						xmlns: "http://www.w3.org/2000/svg",
						children: {
							key: null,
							ref: null,
							props: {
								d: "M4 4v1.5h16V4H4zm8 8.5h8V11h-8v1.5zM4 20h16v-1.5H4V20zm4-8c0-1.1-.9-2-2-2s-2 .9-2 2 .9 2 2 2 2-.9 2-2z",
							},
							_owner: null,
						},
					},
					_owner: null,
				},
			},
			keywords: ["bullet list", "ordered list", "numbered list"],
			attributes: {
				ordered: {
					type: "boolean",
					default: false,
				},
				values: {
					type: "string",
					source: "html",
					selector: "ol,ul",
					multiline: "li",
					__unstableMultilineWrapperTags: ["ol", "ul"],
					default: "",
				},
				type: {
					type: "string",
				},
				start: {
					type: "number",
				},
				reversed: {
					type: "boolean",
				},
				anchor: {
					type: "string",
					source: "attribute",
					attribute: "id",
					selector: "*",
				},
				className: {
					type: "string",
				},
				backgroundColor: {
					type: "string",
				},
				textColor: {
					type: "string",
				},
				gradient: {
					type: "string",
				},
				fontSize: {
					type: "string",
				},
				style: {
					type: "object",
				},
			},
			providesContext: {},
			usesContext: [],
			supports: {
				anchor: true,
				className: false,
				fontSize: true,
				color: {
					gradients: true,
				},
				__unstablePasteTextInline: true,
			},
			styles: [],
			apiVersion: 2,
			category: "text",
			editorStyle: "wp-block-list-editor",
			style: "wp-block-list",
			title: "List",
			description: "Create a bulleted or numbered list.",
			example: {
				attributes: {
					values:
						"<li>Alice.</li><li>The White Rabbit.</li><li>The Cheshire Cat.</li><li>The Mad Hatter.</li><li>The Queen of Hearts.</li>",
				},
			},
			transforms: {
				from: [
					{
						type: "block",
						isMultiBlock: true,
						blocks: ["core/paragraph", "core/heading"],
					},
					{
						type: "block",
						blocks: ["core/quote", "core/pullquote"],
					},
					{
						type: "raw",
						selector: "ol,ul",
					},
					{
						type: "prefix",
						prefix: "*",
					},
					{
						type: "prefix",
						prefix: "-",
					},
					{
						type: "prefix",
						prefix: "1.",
					},
					{
						type: "prefix",
						prefix: "1)",
					},
				],
				to: [
					{
						type: "block",
						blocks: ["core/paragraph"],
					},
					{
						type: "block",
						blocks: ["core/heading"],
					},
					{
						type: "block",
						blocks: ["core/quote"],
					},
					{
						type: "block",
						blocks: ["core/pullquote"],
					},
				],
			},
			variations: [],
		},
		{
			name: "core/quote",
			icon: {
				src: {
					key: null,
					ref: null,
					props: {
						viewBox: "0 0 24 24",
						xmlns: "http://www.w3.org/2000/svg",
						children: {
							key: null,
							ref: null,
							props: {
								d: "M13 6v6h5.2v4c0 .8-.2 1.4-.5 1.7-.6.6-1.6.6-2.5.5h-.3v1.5h.5c1 0 2.3-.1 3.3-1 .6-.6 1-1.6 1-2.8V6H13zm-9 6h5.2v4c0 .8-.2 1.4-.5 1.7-.6.6-1.6.6-2.5.5h-.3v1.5h.5c1 0 2.3-.1 3.3-1 .6-.6 1-1.6 1-2.8V6H4v6z",
							},
							_owner: null,
						},
					},
					_owner: null,
				},
			},
			keywords: ["blockquote", "cite"],
			attributes: {
				value: {
					type: "string",
					source: "html",
					selector: "blockquote",
					multiline: "p",
					default: "",
				},
				citation: {
					type: "string",
					source: "html",
					selector: "cite",
					default: "",
				},
				align: {
					type: "string",
				},
				anchor: {
					type: "string",
					source: "attribute",
					attribute: "id",
					selector: "*",
				},
				className: {
					type: "string",
				},
			},
			providesContext: {},
			usesContext: [],
			supports: {
				anchor: true,
			},
			styles: [
				{
					name: "default",
					label: "Default",
					isDefault: true,
				},
				{
					name: "large",
					label: "Large",
				},
			],
			apiVersion: 2,
			category: "text",
			editorStyle: "wp-block-quote-editor",
			style: "wp-block-quote",
			title: "Quote",
			description:
				'Give quoted text visual emphasis. "In quoting others, we cite ourselves." — Julio Cortázar',
			example: {
				attributes: {
					value: "<p>In quoting others, we cite ourselves.</p>",
					citation: "Julio Cortázar",
					className: "is-style-large",
				},
			},
			transforms: {
				from: [
					{
						type: "block",
						isMultiBlock: true,
						blocks: ["core/paragraph"],
					},
					{
						type: "block",
						blocks: ["core/heading"],
					},
					{
						type: "block",
						blocks: ["core/pullquote"],
					},
					{
						type: "prefix",
						prefix: ">",
					},
					{
						type: "raw",
					},
				],
				to: [
					{
						type: "block",
						blocks: ["core/paragraph"],
					},
					{
						type: "block",
						blocks: ["core/heading"],
					},
					{
						type: "block",
						blocks: ["core/pullquote"],
					},
				],
			},
			deprecated: [
				{
					attributes: {
						value: {
							type: "string",
							source: "html",
							selector: "blockquote",
							multiline: "p",
							default: "",
						},
						citation: {
							type: "string",
							source: "html",
							selector: "cite",
							default: "",
						},
						align: {
							type: "string",
						},
						className: {
							type: "string",
						},
					},
				},
				{
					attributes: {
						value: {
							type: "string",
							source: "html",
							selector: "blockquote",
							multiline: "p",
							default: "",
						},
						citation: {
							type: "string",
							source: "html",
							selector: "cite",
							default: "",
						},
						align: {
							type: "string",
						},
						style: {
							type: "number",
							default: 1,
						},
						className: {
							type: "string",
						},
					},
				},
				{
					attributes: {
						value: {
							type: "string",
							source: "html",
							selector: "blockquote",
							multiline: "p",
							default: "",
						},
						citation: {
							type: "string",
							source: "html",
							selector: "footer",
							default: "",
						},
						align: {
							type: "string",
						},
						style: {
							type: "number",
							default: 1,
						},
						className: {
							type: "string",
						},
					},
				},
			],
			variations: [],
		},
		{
			name: "core/shortcode",
			icon: {
				src: {
					key: null,
					ref: null,
					props: {
						viewBox: "0 0 24 24",
						xmlns: "http://www.w3.org/2000/svg",
						children: {
							key: null,
							ref: null,
							props: {
								d: "M16 4.2v1.5h2.5v12.5H16v1.5h4V4.2h-4zM4.2 19.8h4v-1.5H5.8V5.8h2.5V4.2h-4l-.1 15.6zm5.1-3.1l1.4.6 4-10-1.4-.6-4 10z",
							},
							_owner: null,
						},
					},
					_owner: null,
				},
			},
			keywords: [],
			attributes: {
				text: {
					type: "string",
					source: "html",
				},
			},
			providesContext: {},
			usesContext: [],
			supports: {
				className: false,
				customClassName: false,
				html: false,
			},
			styles: [],
			apiVersion: 2,
			category: "widgets",
			editorStyle: "wp-block-shortcode-editor",
			title: "Shortcode",
			description:
				"Insert additional custom elements with a WordPress shortcode.",
			transforms: {
				from: [
					{
						type: "shortcode",
						tag: "[a-z][a-z0-9_-]*",
						attributes: {
							text: {
								type: "string",
							},
						},
						priority: 20,
					},
				],
			},
			variations: [],
		},
		{
			name: "core/archives",
			icon: {
				src: {
					key: null,
					ref: null,
					props: {
						viewBox: "0 0 24 24",
						xmlns: "http://www.w3.org/2000/svg",
						children: {
							key: null,
							ref: null,
							props: {
								d: "M19 6.2h-5.9l-.6-1.1c-.3-.7-1-1.1-1.8-1.1H5c-1.1 0-2 .9-2 2v11.8c0 1.1.9 2 2 2h14c1.1 0 2-.9 2-2V8.2c0-1.1-.9-2-2-2zm.5 11.6c0 .3-.2.5-.5.5H5c-.3 0-.5-.2-.5-.5V6c0-.3.2-.5.5-.5h5.8c.2 0 .4.1.4.3l1 2H19c.3 0 .5.2.5.5v9.5zM8 12.8h8v-1.5H8v1.5zm0 3h8v-1.5H8v1.5z",
							},
							_owner: null,
						},
					},
					_owner: null,
				},
			},
			keywords: [],
			attributes: {
				displayAsDropdown: {
					type: "boolean",
					default: false,
				},
				showPostCounts: {
					type: "boolean",
					default: false,
				},
				align: {
					type: "string",
					enum: ["left", "center", "right", "wide", "full", ""],
				},
				className: {
					type: "string",
				},
			},
			providesContext: {},
			usesContext: [],
			supports: {
				align: true,
				html: false,
			},
			styles: [],
			apiVersion: 2,
			category: "widgets",
			editorStyle: "wp-block-archives-editor",
			title: "Archives",
			description: "Display a monthly archive of your posts.",
			example: {},
			variations: [],
		},
		{
			name: "core/audio",
			icon: {
				src: {
					key: null,
					ref: null,
					props: {
						viewBox: "0 0 24 24",
						xmlns: "http://www.w3.org/2000/svg",
						children: {
							key: null,
							ref: null,
							props: {
								d: "M17.7 4.3c-1.2 0-2.8 0-3.8 1-.6.6-.9 1.5-.9 2.6V14c-.6-.6-1.5-1-2.5-1C8.6 13 7 14.6 7 16.5S8.6 20 10.5 20c1.5 0 2.8-1 3.3-2.3.5-.8.7-1.8.7-2.5V7.9c0-.7.2-1.2.5-1.6.6-.6 1.8-.6 2.8-.6h.3V4.3h-.4z",
							},
							_owner: null,
						},
					},
					_owner: null,
				},
			},
			keywords: ["music", "sound", "podcast", "recording"],
			attributes: {
				src: {
					type: "string",
					source: "attribute",
					selector: "audio",
					attribute: "src",
				},
				caption: {
					type: "string",
					source: "html",
					selector: "figcaption",
				},
				id: {
					type: "number",
				},
				autoplay: {
					type: "boolean",
					source: "attribute",
					selector: "audio",
					attribute: "autoplay",
				},
				loop: {
					type: "boolean",
					source: "attribute",
					selector: "audio",
					attribute: "loop",
				},
				preload: {
					type: "string",
					source: "attribute",
					selector: "audio",
					attribute: "preload",
				},
				align: {
					type: "string",
					enum: ["left", "center", "right", "wide", "full", ""],
				},
				anchor: {
					type: "string",
					source: "attribute",
					attribute: "id",
					selector: "*",
				},
				className: {
					type: "string",
				},
			},
			providesContext: {},
			usesContext: [],
			supports: {
				anchor: true,
				align: true,
			},
			styles: [],
			apiVersion: 2,
			category: "media",
			editorStyle: "wp-block-audio-editor",
			style: "wp-block-audio",
			title: "Audio",
			description: "Embed a simple audio player.",
			transforms: {
				from: [
					{
						type: "files",
					},
					{
						type: "shortcode",
						tag: "audio",
						attributes: {
							src: {
								type: "string",
							},
							loop: {
								type: "string",
							},
							autoplay: {
								type: "string",
							},
							preload: {
								type: "string",
							},
						},
					},
				],
			},
			deprecated: [
				{
					attributes: {
						src: {
							type: "string",
							source: "attribute",
							selector: "audio",
							attribute: "src",
						},
						caption: {
							type: "string",
							source: "html",
							selector: "figcaption",
						},
						id: {
							type: "number",
						},
						autoplay: {
							type: "boolean",
							source: "attribute",
							selector: "audio",
							attribute: "autoplay",
						},
						loop: {
							type: "boolean",
							source: "attribute",
							selector: "audio",
							attribute: "loop",
						},
						preload: {
							type: "string",
							source: "attribute",
							selector: "audio",
							attribute: "preload",
						},
						align: {
							type: "string",
							enum: ["left", "center", "right", "wide", "full", ""],
						},
						className: {
							type: "string",
						},
					},
					supports: {
						align: true,
					},
				},
			],
			variations: [],
		},
		{
			name: "core/button",
			icon: {
				src: {
					key: null,
					ref: null,
					props: {
						viewBox: "0 0 24 24",
						xmlns: "http://www.w3.org/2000/svg",
						children: {
							key: null,
							ref: null,
							props: {
								d: "M19 6.5H5c-1.1 0-2 .9-2 2v7c0 1.1.9 2 2 2h14c1.1 0 2-.9 2-2v-7c0-1.1-.9-2-2-2zm.5 9c0 .3-.2.5-.5.5H5c-.3 0-.5-.2-.5-.5v-7c0-.3.2-.5.5-.5h14c.3 0 .5.2.5.5v7zM8 13h8v-1.5H8V13z",
							},
							_owner: null,
						},
					},
					_owner: null,
				},
			},
			keywords: ["link"],
			attributes: {
				url: {
					type: "string",
					source: "attribute",
					selector: "a",
					attribute: "href",
				},
				title: {
					type: "string",
					source: "attribute",
					selector: "a",
					attribute: "title",
				},
				text: {
					type: "string",
					source: "html",
					selector: "a",
				},
				linkTarget: {
					type: "string",
					source: "attribute",
					selector: "a",
					attribute: "target",
				},
				rel: {
					type: "string",
					source: "attribute",
					selector: "a",
					attribute: "rel",
				},
				placeholder: {
					type: "string",
				},
				borderRadius: {
					type: "number",
				},
				style: {
					type: "object",
				},
				backgroundColor: {
					type: "string",
				},
				textColor: {
					type: "string",
				},
				gradient: {
					type: "string",
				},
				width: {
					type: "number",
				},
				align: {
					type: "string",
					enum: ["left", "center", "right", "wide", "full", ""],
				},
				anchor: {
					type: "string",
					source: "attribute",
					attribute: "id",
					selector: "*",
				},
				className: {
					type: "string",
				},
			},
			providesContext: {},
			usesContext: [],
			supports: {
				anchor: true,
				align: true,
				alignWide: false,
				reusable: false,
				__experimentalSelector: ".wp-block-button > a",
			},
			styles: [
				{
					name: "fill",
					label: "Fill",
					isDefault: true,
				},
				{
					name: "outline",
					label: "Outline",
				},
			],
			apiVersion: 2,
			category: "design",
			parent: ["core/buttons"],
			editorStyle: "wp-block-button-editor",
			style: "wp-block-button",
			title: "Button",
			description: "Prompt visitors to take action with a button-style link.",
			example: {
				attributes: {
					className: "is-style-fill",
					backgroundColor: "vivid-green-cyan",
					text: "Call to Action",
				},
			},
			deprecated: [
				{
					attributes: {
						url: {
							type: "string",
							source: "attribute",
							selector: "a",
							attribute: "href",
						},
						title: {
							type: "string",
							source: "attribute",
							selector: "a",
							attribute: "title",
						},
						text: {
							type: "string",
							source: "html",
							selector: "a",
						},
						linkTarget: {
							type: "string",
							source: "attribute",
							selector: "a",
							attribute: "target",
						},
						rel: {
							type: "string",
							source: "attribute",
							selector: "a",
							attribute: "rel",
						},
						placeholder: {
							type: "string",
						},
						borderRadius: {
							type: "number",
						},
						backgroundColor: {
							type: "string",
						},
						textColor: {
							type: "string",
						},
						gradient: {
							type: "string",
						},
						style: {
							type: "object",
						},
						align: {
							type: "string",
							enum: ["left", "center", "right", "wide", "full", ""],
						},
						className: {
							type: "string",
						},
					},
					supports: {
						align: true,
						alignWide: false,
						color: {
							gradients: true,
						},
					},
				},
				{
					attributes: {
						url: {
							type: "string",
							source: "attribute",
							selector: "a",
							attribute: "href",
						},
						title: {
							type: "string",
							source: "attribute",
							selector: "a",
							attribute: "title",
						},
						text: {
							type: "string",
							source: "html",
							selector: "a",
						},
						linkTarget: {
							type: "string",
							source: "attribute",
							selector: "a",
							attribute: "target",
						},
						rel: {
							type: "string",
							source: "attribute",
							selector: "a",
							attribute: "rel",
						},
						placeholder: {
							type: "string",
						},
						borderRadius: {
							type: "number",
						},
						backgroundColor: {
							type: "string",
						},
						textColor: {
							type: "string",
						},
						customBackgroundColor: {
							type: "string",
						},
						customTextColor: {
							type: "string",
						},
						customGradient: {
							type: "string",
						},
						gradient: {
							type: "string",
						},
						align: {
							type: "string",
							enum: ["left", "center", "right", "wide", "full", ""],
						},
						className: {
							type: "string",
						},
					},
					supports: {
						align: true,
						alignWide: false,
					},
				},
				{
					attributes: {
						url: {
							type: "string",
							source: "attribute",
							selector: "a",
							attribute: "href",
						},
						title: {
							type: "string",
							source: "attribute",
							selector: "a",
							attribute: "title",
						},
						text: {
							type: "string",
							source: "html",
							selector: "a",
						},
						align: {
							type: "string",
							default: "none",
						},
						backgroundColor: {
							type: "string",
						},
						textColor: {
							type: "string",
						},
						customBackgroundColor: {
							type: "string",
						},
						customTextColor: {
							type: "string",
						},
						linkTarget: {
							type: "string",
							source: "attribute",
							selector: "a",
							attribute: "target",
						},
						rel: {
							type: "string",
							source: "attribute",
							selector: "a",
							attribute: "rel",
						},
						placeholder: {
							type: "string",
						},
						className: {
							type: "string",
						},
					},
				},
				{
					attributes: {
						url: {
							type: "string",
							source: "attribute",
							selector: "a",
							attribute: "href",
						},
						title: {
							type: "string",
							source: "attribute",
							selector: "a",
							attribute: "title",
						},
						text: {
							type: "string",
							source: "html",
							selector: "a",
						},
						align: {
							type: "string",
							default: "none",
						},
						backgroundColor: {
							type: "string",
						},
						textColor: {
							type: "string",
						},
						customBackgroundColor: {
							type: "string",
						},
						customTextColor: {
							type: "string",
						},
						className: {
							type: "string",
						},
					},
				},
				{
					attributes: {
						url: {
							type: "string",
							source: "attribute",
							selector: "a",
							attribute: "href",
						},
						title: {
							type: "string",
							source: "attribute",
							selector: "a",
							attribute: "title",
						},
						text: {
							type: "string",
							source: "html",
							selector: "a",
						},
						color: {
							type: "string",
						},
						textColor: {
							type: "string",
						},
						align: {
							type: "string",
							default: "none",
						},
						className: {
							type: "string",
						},
					},
				},
				{
					attributes: {
						url: {
							type: "string",
							source: "attribute",
							selector: "a",
							attribute: "href",
						},
						title: {
							type: "string",
							source: "attribute",
							selector: "a",
							attribute: "title",
						},
						text: {
							type: "string",
							source: "html",
							selector: "a",
						},
						color: {
							type: "string",
						},
						textColor: {
							type: "string",
						},
						align: {
							type: "string",
							default: "none",
						},
						className: {
							type: "string",
						},
					},
				},
			],
			variations: [],
		},
		{
			name: "core/buttons",
			icon: {
				src: {
					key: null,
					ref: null,
					props: {
						viewBox: "0 0 24 24",
						xmlns: "http://www.w3.org/2000/svg",
						children: {
							key: null,
							ref: null,
							props: {
								d: "M19 6.5H5c-1.1 0-2 .9-2 2v7c0 1.1.9 2 2 2h14c1.1 0 2-.9 2-2v-7c0-1.1-.9-2-2-2zm.5 9c0 .3-.2.5-.5.5H5c-.3 0-.5-.2-.5-.5v-7c0-.3.2-.5.5-.5h14c.3 0 .5.2.5.5v7zM8 13h8v-1.5H8V13z",
							},
							_owner: null,
						},
					},
					_owner: null,
				},
			},
			keywords: ["link"],
			attributes: {
				contentJustification: {
					type: "string",
				},
				orientation: {
					type: "string",
					default: "horizontal",
				},
				align: {
					type: "string",
					enum: ["left", "center", "right", "wide", "full", ""],
				},
				anchor: {
					type: "string",
					source: "attribute",
					attribute: "id",
					selector: "*",
				},
				className: {
					type: "string",
				},
			},
			providesContext: {},
			usesContext: [],
			supports: {
				anchor: true,
				align: ["wide", "full"],
			},
			styles: [],
			apiVersion: 2,
			category: "design",
			editorStyle: "wp-block-buttons-editor",
			style: "wp-block-buttons",
			title: "Buttons",
			description:
				"Prompt visitors to take action with a group of button-style links.",
			example: {
				innerBlocks: [
					{
						name: "core/button",
						attributes: {
							text: "Find out more",
						},
					},
					{
						name: "core/button",
						attributes: {
							text: "Contact us",
						},
					},
				],
			},
			deprecated: [
				{
					attributes: {
						align: {
							type: "string",
							enum: ["left", "center", "right", "wide", "full", ""],
						},
						anchor: {
							type: "string",
							source: "attribute",
							attribute: "id",
							selector: "*",
						},
						className: {
							type: "string",
						},
					},
					supports: {
						align: ["center", "left", "right"],
						anchor: true,
					},
				},
			],
			transforms: {
				from: [
					{
						type: "block",
						isMultiBlock: true,
						blocks: ["core/button"],
					},
				],
			},
			variations: [
				{
					name: "buttons-horizontal",
					isDefault: true,
					title: "Horizontal",
					description: "Buttons shown in a row.",
					attributes: {
						orientation: "horizontal",
					},
					scope: ["transform"],
				},
				{
					name: "buttons-vertical",
					title: "Vertical",
					description: "Buttons shown in a column.",
					attributes: {
						orientation: "vertical",
					},
					scope: ["transform"],
				},
			],
		},
		{
			name: "core/calendar",
			icon: {
				src: {
					key: null,
					ref: null,
					props: {
						viewBox: "0 0 24 24",
						xmlns: "http://www.w3.org/2000/svg",
						children: {
							key: null,
							ref: null,
							props: {
								d: "M19 3H5c-1.1 0-2 .9-2 2v14c0 1.1.9 2 2 2h14c1.1 0 2-.9 2-2V5c0-1.1-.9-2-2-2zm.5 16c0 .3-.2.5-.5.5H5c-.3 0-.5-.2-.5-.5V7h15v12zM9 10H7v2h2v-2zm0 4H7v2h2v-2zm4-4h-2v2h2v-2zm4 0h-2v2h2v-2zm-4 4h-2v2h2v-2zm4 0h-2v2h2v-2z",
							},
							_owner: null,
						},
					},
					_owner: null,
				},
			},
			keywords: ["posts", "archive"],
			attributes: {
				month: {
					type: "integer",
				},
				year: {
					type: "integer",
				},
				align: {
					type: "string",
					enum: ["left", "center", "right", "wide", "full", ""],
				},
				className: {
					type: "string",
				},
			},
			providesContext: {},
			usesContext: [],
			supports: {
				align: true,
			},
			styles: [],
			apiVersion: 2,
			category: "widgets",
			style: "wp-block-calendar",
			title: "Calendar",
			description: "A calendar of your site’s posts.",
			example: {},
			variations: [],
		},
		{
			name: "core/categories",
			icon: {
				src: {
					key: null,
					ref: null,
					props: {
						viewBox: "0 0 24 24",
						xmlns: "http://www.w3.org/2000/svg",
						children: {
							key: null,
							ref: null,
							props: {
								d: "M19 3H5c-1.1 0-2 .9-2 2v14c0 1.1.9 2 2 2h14c1.1 0 2-.9 2-2V5c0-1.1-.9-2-2-2zm.5 16c0 .3-.2.5-.5.5H5c-.3 0-.5-.2-.5-.5V7h15v12zM9 10H7v2h2v-2zm0 4H7v2h2v-2zm4-4h-2v2h2v-2zm4 0h-2v2h2v-2zm-4 4h-2v2h2v-2zm4 0h-2v2h2v-2z",
							},
							_owner: null,
						},
					},
					_owner: null,
				},
			},
			keywords: [],
			attributes: {
				displayAsDropdown: {
					type: "boolean",
					default: false,
				},
				showHierarchy: {
					type: "boolean",
					default: false,
				},
				showPostCounts: {
					type: "boolean",
					default: false,
				},
				align: {
					type: "string",
					enum: ["left", "center", "right", "wide", "full", ""],
				},
				className: {
					type: "string",
				},
			},
			providesContext: {},
			usesContext: [],
			supports: {
				align: true,
				html: false,
			},
			styles: [],
			apiVersion: 2,
			category: "widgets",
			editorStyle: "wp-block-categories-editor",
			style: "wp-block-categories",
			title: "Categories",
			description: "Display a list of all categories.",
			example: {},
			variations: [],
		},
		{
			name: "core/code",
			icon: {
				src: {
					key: null,
					ref: null,
					props: {
						viewBox: "0 0 24 24",
						xmlns: "http://www.w3.org/2000/svg",
						children: {
							key: null,
							ref: null,
							props: {
								d: "M20.8 10.7l-4.3-4.3-1.1 1.1 4.3 4.3c.1.1.1.3 0 .4l-4.3 4.3 1.1 1.1 4.3-4.3c.7-.8.7-1.9 0-2.6zM4.2 11.8l4.3-4.3-1-1-4.3 4.3c-.7.7-.7 1.8 0 2.5l4.3 4.3 1.1-1.1-4.3-4.3c-.2-.1-.2-.3-.1-.4z",
							},
							_owner: null,
						},
					},
					_owner: null,
				},
			},
			keywords: [],
			attributes: {
				content: {
					type: "string",
					source: "html",
					selector: "code",
				},
				anchor: {
					type: "string",
					source: "attribute",
					attribute: "id",
					selector: "*",
				},
				className: {
					type: "string",
				},
				fontSize: {
					type: "string",
				},
				style: {
					type: "object",
				},
			},
			providesContext: {},
			usesContext: [],
			supports: {
				anchor: true,
				fontSize: true,
			},
			styles: [],
			apiVersion: 2,
			category: "text",
			style: "wp-block-code",
			title: "Code",
			description: "Display code snippets that respect your spacing and tabs.",
			example: {
				attributes: {
					content:
						'// A "block" is the abstract term used\n// to describe units of markup that\n// when composed together, form the\n// content or layout of a page.\nregisterBlockType( name, settings );',
				},
			},
			transforms: {
				from: [
					{
						type: "enter",
						regExp: {},
					},
					{
						type: "block",
						blocks: ["core/html"],
					},
					{
						type: "raw",
						schema: {
							pre: {
								children: {
									code: {
										children: {
											"#text": {},
										},
									},
								},
							},
						},
					},
				],
			},
			variations: [],
		},
		{
			name: "core/columns",
			icon: {
				src: {
					key: null,
					ref: null,
					props: {
						viewBox: "0 0 24 24",
						xmlns: "http://www.w3.org/2000/svg",
						children: {
							key: null,
							ref: null,
							props: {
								d: "M19 6H6c-1.1 0-2 .9-2 2v9c0 1.1.9 2 2 2h13c1.1 0 2-.9 2-2V8c0-1.1-.9-2-2-2zm-4.1 1.5v10H10v-10h4.9zM5.5 17V8c0-.3.2-.5.5-.5h2.5v10H6c-.3 0-.5-.2-.5-.5zm14 0c0 .3-.2.5-.5.5h-2.6v-10H19c.3 0 .5.2.5.5v9z",
							},
							_owner: null,
						},
					},
					_owner: null,
				},
			},
			keywords: [],
			attributes: {
				verticalAlignment: {
					type: "string",
				},
				align: {
					type: "string",
					enum: ["left", "center", "right", "wide", "full", ""],
				},
				anchor: {
					type: "string",
					source: "attribute",
					attribute: "id",
					selector: "*",
				},
				className: {
					type: "string",
				},
				backgroundColor: {
					type: "string",
				},
				textColor: {
					type: "string",
				},
				gradient: {
					type: "string",
				},
				style: {
					type: "object",
				},
			},
			providesContext: {},
			usesContext: [],
			supports: {
				anchor: true,
				align: ["wide", "full"],
				html: false,
				color: {
					gradients: true,
					link: true,
				},
			},
			styles: [],
			apiVersion: 2,
			category: "design",
			editorStyle: "wp-block-columns-editor",
			style: "wp-block-columns",
			title: "Columns",
			description:
				"Add a block that displays content in multiple columns, then add whatever content blocks you’d like.",
			variations: [
				{
					name: "one-column-full",
					title: "100",
					description: "One column",
					icon: {
						key: null,
						ref: null,
						props: {
							width: "48",
							height: "48",
							viewBox: "0 0 48 48",
							xmlns: "http://www.w3.org/2000/svg",
							children: {
								key: null,
								ref: null,
								props: {
									fillRule: "evenodd",
									clipRule: "evenodd",
									d: "m39.0625 14h-30.0625v20.0938h30.0625zm-30.0625-2c-1.10457 0-2 .8954-2 2v20.0938c0 1.1045.89543 2 2 2h30.0625c1.1046 0 2-.8955 2-2v-20.0938c0-1.1046-.8954-2-2-2z",
								},
								_owner: null,
							},
						},
						_owner: null,
					},
					innerBlocks: [["core/column"]],
					scope: ["block"],
				},
				{
					name: "two-columns-equal",
					title: "50 / 50",
					description: "Two columns; equal split",
					icon: {
						key: null,
						ref: null,
						props: {
							width: "48",
							height: "48",
							viewBox: "0 0 48 48",
							xmlns: "http://www.w3.org/2000/svg",
							children: {
								key: null,
								ref: null,
								props: {
									fillRule: "evenodd",
									clipRule: "evenodd",
									d: "M39 12C40.1046 12 41 12.8954 41 14V34C41 35.1046 40.1046 36 39 36H9C7.89543 36 7 35.1046 7 34V14C7 12.8954 7.89543 12 9 12H39ZM39 34V14H25V34H39ZM23 34H9V14H23V34Z",
								},
								_owner: null,
							},
						},
						_owner: null,
					},
					isDefault: true,
					innerBlocks: [["core/column"], ["core/column"]],
					scope: ["block"],
				},
				{
					name: "two-columns-one-third-two-thirds",
					title: "30 / 70",
					description: "Two columns; one-third, two-thirds split",
					icon: {
						key: null,
						ref: null,
						props: {
							width: "48",
							height: "48",
							viewBox: "0 0 48 48",
							xmlns: "http://www.w3.org/2000/svg",
							children: {
								key: null,
								ref: null,
								props: {
									fillRule: "evenodd",
									clipRule: "evenodd",
									d: "M39 12C40.1046 12 41 12.8954 41 14V34C41 35.1046 40.1046 36 39 36H9C7.89543 36 7 35.1046 7 34V14C7 12.8954 7.89543 12 9 12H39ZM39 34V14H20V34H39ZM18 34H9V14H18V34Z",
								},
								_owner: null,
							},
						},
						_owner: null,
					},
					innerBlocks: [
						[
							"core/column",
							{
								width: "33.33%",
							},
						],
						[
							"core/column",
							{
								width: "66.66%",
							},
						],
					],
					scope: ["block"],
				},
				{
					name: "two-columns-two-thirds-one-third",
					title: "70 / 30",
					description: "Two columns; two-thirds, one-third split",
					icon: {
						key: null,
						ref: null,
						props: {
							width: "48",
							height: "48",
							viewBox: "0 0 48 48",
							xmlns: "http://www.w3.org/2000/svg",
							children: {
								key: null,
								ref: null,
								props: {
									fillRule: "evenodd",
									clipRule: "evenodd",
									d: "M39 12C40.1046 12 41 12.8954 41 14V34C41 35.1046 40.1046 36 39 36H9C7.89543 36 7 35.1046 7 34V14C7 12.8954 7.89543 12 9 12H39ZM39 34V14H30V34H39ZM28 34H9V14H28V34Z",
								},
								_owner: null,
							},
						},
						_owner: null,
					},
					innerBlocks: [
						[
							"core/column",
							{
								width: "66.66%",
							},
						],
						[
							"core/column",
							{
								width: "33.33%",
							},
						],
					],
					scope: ["block"],
				},
				{
					name: "three-columns-equal",
					title: "33 / 33 / 33",
					description: "Three columns; equal split",
					icon: {
						key: null,
						ref: null,
						props: {
							width: "48",
							height: "48",
							viewBox: "0 0 48 48",
							xmlns: "http://www.w3.org/2000/svg",
							children: {
								key: null,
								ref: null,
								props: {
									fillRule: "evenodd",
									d: "M41 14a2 2 0 0 0-2-2H9a2 2 0 0 0-2 2v20a2 2 0 0 0 2 2h30a2 2 0 0 0 2-2V14zM28.5 34h-9V14h9v20zm2 0V14H39v20h-8.5zm-13 0H9V14h8.5v20z",
								},
								_owner: null,
							},
						},
						_owner: null,
					},
					innerBlocks: [["core/column"], ["core/column"], ["core/column"]],
					scope: ["block"],
				},
				{
					name: "three-columns-wider-center",
					title: "25 / 50 / 25",
					description: "Three columns; wide center column",
					icon: {
						key: null,
						ref: null,
						props: {
							width: "48",
							height: "48",
							viewBox: "0 0 48 48",
							xmlns: "http://www.w3.org/2000/svg",
							children: {
								key: null,
								ref: null,
								props: {
									fillRule: "evenodd",
									d: "M41 14a2 2 0 0 0-2-2H9a2 2 0 0 0-2 2v20a2 2 0 0 0 2 2h30a2 2 0 0 0 2-2V14zM31 34H17V14h14v20zm2 0V14h6v20h-6zm-18 0H9V14h6v20z",
								},
								_owner: null,
							},
						},
						_owner: null,
					},
					innerBlocks: [
						[
							"core/column",
							{
								width: "25%",
							},
						],
						[
							"core/column",
							{
								width: "50%",
							},
						],
						[
							"core/column",
							{
								width: "25%",
							},
						],
					],
					scope: ["block"],
				},
			],
			example: {
				innerBlocks: [
					{
						name: "core/column",
						innerBlocks: [
							{
								name: "core/paragraph",
								attributes: {
									content:
										"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent et eros eu felis.",
								},
							},
							{
								name: "core/image",
								attributes: {
									url: "https://s.w.org/images/core/5.3/Windbuchencom.jpg",
								},
							},
							{
								name: "core/paragraph",
								attributes: {
									content:
										"Suspendisse commodo neque lacus, a dictum orci interdum et.",
								},
							},
						],
					},
					{
						name: "core/column",
						innerBlocks: [
							{
								name: "core/paragraph",
								attributes: {
									content:
										"Etiam et egestas lorem. Vivamus sagittis sit amet dolor quis lobortis. Integer sed fermentum arcu, id vulputate lacus. Etiam fermentum sem eu quam hendrerit.",
								},
							},
							{
								name: "core/paragraph",
								attributes: {
									content:
										"Nam risus massa, ullamcorper consectetur eros fermentum, porta aliquet ligula. Sed vel mauris nec enim.",
								},
							},
						],
					},
				],
			},
			deprecated: [
				{
					attributes: {
						verticalAlignment: {
							type: "string",
						},
						backgroundColor: {
							type: "string",
						},
						customBackgroundColor: {
							type: "string",
						},
						customTextColor: {
							type: "string",
						},
						textColor: {
							type: "string",
						},
						className: {
							type: "string",
						},
					},
				},
				{
					attributes: {
						columns: {
							type: "number",
							default: 2,
						},
						className: {
							type: "string",
						},
					},
				},
				{
					attributes: {
						columns: {
							type: "number",
							default: 2,
						},
						className: {
							type: "string",
						},
					},
				},
			],
			transforms: {
				from: [
					{
						type: "block",
						isMultiBlock: true,
						blocks: ["*"],
					},
				],
			},
		},
		{
			name: "core/column",
			icon: {
				src: {
					key: null,
					ref: null,
					props: {
						xmlns: "http://www.w3.org/2000/svg",
						viewBox: "0 0 24 24",
						children: {
							key: null,
							ref: null,
							props: {
								d: "M19 6H6c-1.1 0-2 .9-2 2v9c0 1.1.9 2 2 2h13c1.1 0 2-.9 2-2V8c0-1.1-.9-2-2-2zM6 17.5c-.3 0-.5-.2-.5-.5V8c0-.3.2-.5.5-.5h3v10H6zm13.5-.5c0 .3-.2.5-.5.5h-3v-10h3c.3 0 .5.2.5.5v9z",
							},
							_owner: null,
						},
					},
					_owner: null,
				},
			},
			keywords: [],
			attributes: {
				verticalAlignment: {
					type: "string",
				},
				width: {
					type: "string",
				},
				templateLock: {
					type: "string",
				},
				anchor: {
					type: "string",
					source: "attribute",
					attribute: "id",
					selector: "*",
				},
				className: {
					type: "string",
				},
			},
			providesContext: {},
			usesContext: [],
			supports: {
				anchor: true,
				reusable: false,
				html: false,
			},
			styles: [],
			apiVersion: 2,
			category: "text",
			parent: ["core/columns"],
			title: "Column",
			description: "A single column within a columns block.",
			deprecated: [
				{
					attributes: {
						verticalAlignment: {
							type: "string",
						},
						width: {
							type: "number",
							min: 0,
							max: 100,
						},
						className: {
							type: "string",
						},
					},
				},
			],
			variations: [],
		},
		{
			name: "core/cover",
			icon: {
				src: {
					key: null,
					ref: null,
					props: {
						xmlns: "http://www.w3.org/2000/svg",
						viewBox: "0 0 24 24",
						children: {
							key: null,
							ref: null,
							props: {
								d: "M18.7 3H5.3C4 3 3 4 3 5.3v13.4C3 20 4 21 5.3 21h13.4c1.3 0 2.3-1 2.3-2.3V5.3C21 4 20 3 18.7 3zm.8 15.7c0 .4-.4.8-.8.8H5.3c-.4 0-.8-.4-.8-.8V5.3c0-.4.4-.8.8-.8h6.2v8.9l2.5-3.1 2.5 3.1V4.5h2.2c.4 0 .8.4.8.8v13.4z",
							},
							_owner: null,
						},
					},
					_owner: null,
				},
			},
			keywords: [],
			attributes: {
				url: {
					type: "string",
				},
				id: {
					type: "number",
				},
				hasParallax: {
					type: "boolean",
					default: false,
				},
				isRepeated: {
					type: "boolean",
					default: false,
				},
				dimRatio: {
					type: "number",
					default: 50,
				},
				overlayColor: {
					type: "string",
				},
				customOverlayColor: {
					type: "string",
				},
				backgroundType: {
					type: "string",
					default: "image",
				},
				focalPoint: {
					type: "object",
				},
				minHeight: {
					type: "number",
				},
				minHeightUnit: {
					type: "string",
				},
				gradient: {
					type: "string",
				},
				customGradient: {
					type: "string",
				},
				contentPosition: {
					type: "string",
				},
				align: {
					type: "string",
					enum: ["left", "center", "right", "wide", "full", ""],
				},
				anchor: {
					type: "string",
					source: "attribute",
					attribute: "id",
					selector: "*",
				},
				className: {
					type: "string",
				},
				style: {
					type: "object",
				},
			},
			providesContext: {},
			usesContext: [],
			supports: {
				anchor: true,
				align: true,
				html: false,
				spacing: {
					padding: true,
				},
			},
			styles: [],
			apiVersion: 2,
			category: "media",
			editorStyle: "wp-block-cover-editor",
			style: "wp-block-cover",
			title: "Cover",
			description:
				"Add an image or video with a text overlay — great for headers.",
			example: {
				attributes: {
					customOverlayColor: "#065174",
					dimRatio: 40,
					url: "https://s.w.org/images/core/5.3/Windbuchencom.jpg",
				},
				innerBlocks: [
					{
						name: "core/paragraph",
						attributes: {
							customFontSize: 48,
							content: "<strong>Snow Patrol</strong>",
							align: "center",
						},
					},
				],
			},
			transforms: {
				from: [
					{
						type: "block",
						blocks: ["core/image"],
					},
					{
						type: "block",
						blocks: ["core/video"],
					},
				],
				to: [
					{
						type: "block",
						blocks: ["core/image"],
					},
					{
						type: "block",
						blocks: ["core/video"],
					},
				],
			},
			deprecated: [
				{
					attributes: {
						url: {
							type: "string",
						},
						id: {
							type: "number",
						},
						hasParallax: {
							type: "boolean",
							default: false,
						},
						dimRatio: {
							type: "number",
							default: 50,
						},
						overlayColor: {
							type: "string",
						},
						customOverlayColor: {
							type: "string",
						},
						backgroundType: {
							type: "string",
							default: "image",
						},
						focalPoint: {
							type: "object",
						},
						title: {
							type: "string",
							source: "html",
							selector: "p",
						},
						contentAlign: {
							type: "string",
							default: "center",
						},
						isRepeated: {
							type: "boolean",
							default: false,
						},
						minHeight: {
							type: "number",
						},
						minHeightUnit: {
							type: "string",
						},
						gradient: {
							type: "string",
						},
						customGradient: {
							type: "string",
						},
						contentPosition: {
							type: "string",
						},
						align: {
							type: "string",
							enum: ["left", "center", "right", "wide", "full", ""],
						},
						className: {
							type: "string",
						},
					},
					supports: {
						align: true,
					},
				},
				{
					attributes: {
						url: {
							type: "string",
						},
						id: {
							type: "number",
						},
						hasParallax: {
							type: "boolean",
							default: false,
						},
						dimRatio: {
							type: "number",
							default: 50,
						},
						overlayColor: {
							type: "string",
						},
						customOverlayColor: {
							type: "string",
						},
						backgroundType: {
							type: "string",
							default: "image",
						},
						focalPoint: {
							type: "object",
						},
						title: {
							type: "string",
							source: "html",
							selector: "p",
						},
						contentAlign: {
							type: "string",
							default: "center",
						},
						minHeight: {
							type: "number",
						},
						gradient: {
							type: "string",
						},
						customGradient: {
							type: "string",
						},
						align: {
							type: "string",
							enum: ["left", "center", "right", "wide", "full", ""],
						},
						className: {
							type: "string",
						},
					},
					supports: {
						align: true,
					},
				},
				{
					attributes: {
						url: {
							type: "string",
						},
						id: {
							type: "number",
						},
						hasParallax: {
							type: "boolean",
							default: false,
						},
						dimRatio: {
							type: "number",
							default: 50,
						},
						overlayColor: {
							type: "string",
						},
						customOverlayColor: {
							type: "string",
						},
						backgroundType: {
							type: "string",
							default: "image",
						},
						focalPoint: {
							type: "object",
						},
						title: {
							type: "string",
							source: "html",
							selector: "p",
						},
						contentAlign: {
							type: "string",
							default: "center",
						},
						minHeight: {
							type: "number",
						},
						gradient: {
							type: "string",
						},
						customGradient: {
							type: "string",
						},
						align: {
							type: "string",
							enum: ["left", "center", "right", "wide", "full", ""],
						},
						className: {
							type: "string",
						},
					},
					supports: {
						align: true,
					},
				},
				{
					attributes: {
						url: {
							type: "string",
						},
						id: {
							type: "number",
						},
						hasParallax: {
							type: "boolean",
							default: false,
						},
						dimRatio: {
							type: "number",
							default: 50,
						},
						overlayColor: {
							type: "string",
						},
						customOverlayColor: {
							type: "string",
						},
						backgroundType: {
							type: "string",
							default: "image",
						},
						focalPoint: {
							type: "object",
						},
						title: {
							type: "string",
							source: "html",
							selector: "p",
						},
						contentAlign: {
							type: "string",
							default: "center",
						},
						align: {
							type: "string",
							enum: ["left", "center", "right", "wide", "full", ""],
						},
						className: {
							type: "string",
						},
					},
					supports: {
						align: true,
					},
				},
				{
					attributes: {
						url: {
							type: "string",
						},
						id: {
							type: "number",
						},
						hasParallax: {
							type: "boolean",
							default: false,
						},
						dimRatio: {
							type: "number",
							default: 50,
						},
						overlayColor: {
							type: "string",
						},
						customOverlayColor: {
							type: "string",
						},
						backgroundType: {
							type: "string",
							default: "image",
						},
						focalPoint: {
							type: "object",
						},
						title: {
							type: "string",
							source: "html",
							selector: "p",
						},
						contentAlign: {
							type: "string",
							default: "center",
						},
						align: {
							type: "string",
						},
						className: {
							type: "string",
						},
					},
					supports: {
						className: false,
					},
				},
				{
					attributes: {
						url: {
							type: "string",
						},
						id: {
							type: "number",
						},
						hasParallax: {
							type: "boolean",
							default: false,
						},
						dimRatio: {
							type: "number",
							default: 50,
						},
						overlayColor: {
							type: "string",
						},
						customOverlayColor: {
							type: "string",
						},
						backgroundType: {
							type: "string",
							default: "image",
						},
						focalPoint: {
							type: "object",
						},
						title: {
							type: "string",
							source: "html",
							selector: "h2",
						},
						align: {
							type: "string",
						},
						contentAlign: {
							type: "string",
							default: "center",
						},
						className: {
							type: "string",
						},
					},
					supports: {
						className: false,
					},
				},
			],
			variations: [],
		},
		{
			name: "core/embed",
			icon: {
				src: {
					key: null,
					ref: null,
					props: {
						viewBox: "0 0 24 24",
						xmlns: "http://www.w3.org/2000/svg",
						children: {
							key: null,
							ref: null,
							props: {
								d: "M19 3H5c-1.1 0-2 .9-2 2v14c0 1.1.9 2 2 2h14c1.1 0 2-.9 2-2V5c0-1.1-.9-2-2-2zm.5 16c0 .3-.2.5-.5.5H5c-.3 0-.5-.2-.5-.5V9.8l4.7-5.3H19c.3 0 .5.2.5.5v14zm-6-9.5L16 12l-2.5 2.8 1.1 1L18 12l-3.5-3.5-1 1zm-3 0l-1-1L6 12l3.5 3.8 1.1-1L8 12l2.5-2.5z",
							},
							_owner: null,
						},
					},
					_owner: null,
				},
			},
			keywords: [],
			attributes: {
				url: {
					type: "string",
				},
				caption: {
					type: "string",
					source: "html",
					selector: "figcaption",
				},
				type: {
					type: "string",
				},
				providerNameSlug: {
					type: "string",
				},
				allowResponsive: {
					type: "boolean",
					default: true,
				},
				responsive: {
					type: "boolean",
					default: false,
				},
				previewable: {
					type: "boolean",
					default: true,
				},
				align: {
					type: "string",
					enum: ["left", "center", "right", "wide", "full", ""],
				},
				className: {
					type: "string",
				},
			},
			providesContext: {},
			usesContext: [],
			supports: {
				align: true,
			},
			styles: [],
			apiVersion: 2,
			category: "embed",
			editorStyle: "wp-block-embed-editor",
			style: "wp-block-embed",
			title: "Embed",
			description:
				"Add a block that displays content pulled from other sites, like Twitter, Instagram or YouTube.",
			transforms: {
				from: [
					{
						type: "raw",
					},
				],
				to: [
					{
						type: "block",
						blocks: ["core/paragraph"],
					},
				],
			},
			variations: [
				{
					name: "twitter",
					title: "Twitter",
					icon: {
						foreground: "#1da1f2",
						src: {
							key: null,
							ref: null,
							props: {
								xmlns: "http://www.w3.org/2000/svg",
								viewBox: "0 0 24 24",
								children: {
									key: null,
									ref: null,
									props: {
										children: {
											key: null,
											ref: null,
											props: {
												d: "M22.23 5.924c-.736.326-1.527.547-2.357.646.847-.508 1.498-1.312 1.804-2.27-.793.47-1.67.812-2.606.996C18.325 4.498 17.258 4 16.078 4c-2.266 0-4.103 1.837-4.103 4.103 0 .322.036.635.106.935-3.41-.17-6.433-1.804-8.457-4.287-.353.607-.556 1.312-.556 2.064 0 1.424.724 2.68 1.825 3.415-.673-.022-1.305-.207-1.86-.514v.052c0 1.988 1.415 3.647 3.293 4.023-.344.095-.707.145-1.08.145-.265 0-.522-.026-.773-.074.522 1.63 2.038 2.817 3.833 2.85-1.404 1.1-3.174 1.757-5.096 1.757-.332 0-.66-.02-.98-.057 1.816 1.164 3.973 1.843 6.29 1.843 7.547 0 11.675-6.252 11.675-11.675 0-.178-.004-.355-.012-.53.802-.578 1.497-1.3 2.047-2.124z",
											},
											_owner: null,
										},
									},
									_owner: null,
								},
							},
							_owner: null,
						},
					},
					keywords: ["tweet", "social"],
					description: "Embed a tweet.",
					patterns: [{}],
					attributes: {
						providerNameSlug: "twitter",
						responsive: true,
					},
				},
				{
					name: "youtube",
					title: "YouTube",
					icon: {
						foreground: "#ff0000",
						src: {
							key: null,
							ref: null,
							props: {
								viewBox: "0 0 24 24",
								children: {
									key: null,
									ref: null,
									props: {
										d: "M21.8 8s-.195-1.377-.795-1.984c-.76-.797-1.613-.8-2.004-.847-2.798-.203-6.996-.203-6.996-.203h-.01s-4.197 0-6.996.202c-.39.046-1.242.05-2.003.846C2.395 6.623 2.2 8 2.2 8S2 9.62 2 11.24v1.517c0 1.618.2 3.237.2 3.237s.195 1.378.795 1.985c.76.797 1.76.77 2.205.855 1.6.153 6.8.2 6.8.2s4.203-.005 7-.208c.392-.047 1.244-.05 2.005-.847.6-.607.795-1.985.795-1.985s.2-1.618.2-3.237v-1.517C22 9.62 21.8 8 21.8 8zM9.935 14.595v-5.62l5.403 2.82-5.403 2.8z",
									},
									_owner: null,
								},
							},
							_owner: null,
						},
					},
					keywords: ["music", "video"],
					description: "Embed a YouTube video.",
					patterns: [{}, {}],
					attributes: {
						providerNameSlug: "youtube",
						responsive: true,
					},
				},
				{
					name: "facebook",
					title: "Facebook",
					icon: {
						foreground: "#3b5998",
						src: {
							key: null,
							ref: null,
							props: {
								viewBox: "0 0 24 24",
								children: {
									key: null,
									ref: null,
									props: {
										d: "M20 3H4c-.6 0-1 .4-1 1v16c0 .5.4 1 1 1h8.6v-7h-2.3v-2.7h2.3v-2c0-2.3 1.4-3.6 3.5-3.6 1 0 1.8.1 2.1.1v2.4h-1.4c-1.1 0-1.3.5-1.3 1.3v1.7h2.7l-.4 2.8h-2.3v7H20c.5 0 1-.4 1-1V4c0-.6-.4-1-1-1z",
									},
									_owner: null,
								},
							},
							_owner: null,
						},
					},
					keywords: ["social"],
					description: "Embed a Facebook post.",
					scope: ["block"],
					patterns: [],
					attributes: {
						providerNameSlug: "facebook",
						previewable: false,
						responsive: true,
					},
				},
				{
					name: "instagram",
					title: "Instagram",
					icon: {
						key: null,
						ref: null,
						props: {
							viewBox: "0 0 24 24",
							children: {
								key: null,
								ref: null,
								props: {
									children: {
										key: null,
										ref: null,
										props: {
											d: "M12 4.622c2.403 0 2.688.01 3.637.052.877.04 1.354.187 1.67.31.42.163.72.358 1.036.673.315.315.51.615.673 1.035.123.317.27.794.31 1.67.043.95.052 1.235.052 3.638s-.01 2.688-.052 3.637c-.04.877-.187 1.354-.31 1.67-.163.42-.358.72-.673 1.036-.315.315-.615.51-1.035.673-.317.123-.794.27-1.67.31-.95.043-1.234.052-3.638.052s-2.688-.01-3.637-.052c-.877-.04-1.354-.187-1.67-.31-.42-.163-.72-.358-1.036-.673-.315-.315-.51-.615-.673-1.035-.123-.317-.27-.794-.31-1.67-.043-.95-.052-1.235-.052-3.638s.01-2.688.052-3.637c.04-.877.187-1.354.31-1.67.163-.42.358-.72.673-1.036.315-.315.615-.51 1.035-.673.317-.123.794-.27 1.67-.31.95-.043 1.235-.052 3.638-.052M12 3c-2.444 0-2.75.01-3.71.054s-1.613.196-2.185.418c-.592.23-1.094.538-1.594 1.04-.5.5-.807 1-1.037 1.593-.223.572-.375 1.226-.42 2.184C3.01 9.25 3 9.555 3 12s.01 2.75.054 3.71.196 1.613.418 2.186c.23.592.538 1.094 1.038 1.594s1.002.808 1.594 1.038c.572.222 1.227.375 2.185.418.96.044 1.266.054 3.71.054s2.75-.01 3.71-.054 1.613-.196 2.186-.418c.592-.23 1.094-.538 1.594-1.038s.808-1.002 1.038-1.594c.222-.572.375-1.227.418-2.185.044-.96.054-1.266.054-3.71s-.01-2.75-.054-3.71-.196-1.613-.418-2.186c-.23-.592-.538-1.094-1.038-1.594s-1.002-.808-1.594-1.038c-.572-.222-1.227-.375-2.185-.418C14.75 3.01 14.445 3 12 3zm0 4.378c-2.552 0-4.622 2.07-4.622 4.622s2.07 4.622 4.622 4.622 4.622-2.07 4.622-4.622S14.552 7.378 12 7.378zM12 15c-1.657 0-3-1.343-3-3s1.343-3 3-3 3 1.343 3 3-1.343 3-3 3zm4.804-8.884c-.596 0-1.08.484-1.08 1.08s.484 1.08 1.08 1.08c.596 0 1.08-.484 1.08-1.08s-.483-1.08-1.08-1.08z",
										},
										_owner: null,
									},
								},
								_owner: null,
							},
						},
						_owner: null,
					},
					keywords: ["image", "social"],
					description: "Embed an Instagram post.",
					scope: ["block"],
					patterns: [],
					attributes: {
						providerNameSlug: "instagram",
						responsive: true,
					},
				},
				{
					name: "wordpress",
					title: "WordPress",
					icon: {
						foreground: "#0073AA",
						src: {
							key: null,
							ref: null,
							props: {
								viewBox: "0 0 24 24",
								children: {
									key: null,
									ref: null,
									props: {
										children: {
											key: null,
											ref: null,
											props: {
												d: "M12.158 12.786l-2.698 7.84c.806.236 1.657.365 2.54.365 1.047 0 2.05-.18 2.986-.51-.024-.037-.046-.078-.065-.123l-2.762-7.57zM3.008 12c0 3.56 2.07 6.634 5.068 8.092L3.788 8.342c-.5 1.117-.78 2.354-.78 3.658zm15.06-.454c0-1.112-.398-1.88-.74-2.48-.456-.74-.883-1.368-.883-2.11 0-.825.627-1.595 1.51-1.595.04 0 .078.006.116.008-1.598-1.464-3.73-2.36-6.07-2.36-3.14 0-5.904 1.613-7.512 4.053.21.008.41.012.58.012.94 0 2.395-.114 2.395-.114.484-.028.54.684.057.74 0 0-.487.058-1.03.086l3.275 9.74 1.968-5.902-1.4-3.838c-.485-.028-.944-.085-.944-.085-.486-.03-.43-.77.056-.742 0 0 1.484.114 2.368.114.94 0 2.397-.114 2.397-.114.486-.028.543.684.058.74 0 0-.488.058-1.03.086l3.25 9.665.897-2.997c.456-1.17.684-2.137.684-2.907zm1.82-3.86c.04.286.06.593.06.924 0 .912-.17 1.938-.683 3.22l-2.746 7.94c2.672-1.558 4.47-4.454 4.47-7.77 0-1.564-.4-3.033-1.1-4.314zM12 22C6.486 22 2 17.514 2 12S6.486 2 12 2s10 4.486 10 10-4.486 10-10 10z",
											},
											_owner: null,
										},
									},
									_owner: null,
								},
							},
							_owner: null,
						},
					},
					keywords: ["post", "blog"],
					description: "Embed a WordPress post.",
					attributes: {
						providerNameSlug: "wordpress",
					},
				},
				{
					name: "soundcloud",
					title: "SoundCloud",
					icon: {
						key: null,
						ref: null,
						props: {
							viewBox: "0 0 24 24",
							xmlns: "http://www.w3.org/2000/svg",
							children: {
								key: null,
								ref: null,
								props: {
									d: "M19 3H5c-1.1 0-2 .9-2 2v14c0 1.1.9 2 2 2h14c1.1 0 2-.9 2-2V5c0-1.1-.9-2-2-2zm.5 16c0 .3-.2.5-.5.5H5c-.3 0-.5-.2-.5-.5V9.8l4.7-5.3H19c.3 0 .5.2.5.5v14zM13.2 7.7c-.4.4-.7 1.1-.7 1.9v3.7c-.4-.3-.8-.4-1.3-.4-1.2 0-2.2 1-2.2 2.2 0 1.2 1 2.2 2.2 2.2.5 0 1-.2 1.4-.5.9-.6 1.4-1.6 1.4-2.6V9.6c0-.4.1-.6.2-.8.3-.3 1-.3 1.6-.3h.2V7h-.2c-.7 0-1.8 0-2.6.7z",
								},
								_owner: null,
							},
						},
						_owner: null,
					},
					keywords: ["music", "audio"],
					description: "Embed SoundCloud content.",
					patterns: [{}],
					attributes: {
						providerNameSlug: "soundcloud",
						responsive: true,
					},
				},
				{
					name: "spotify",
					title: "Spotify",
					icon: {
						foreground: "#1db954",
						src: {
							key: null,
							ref: null,
							props: {
								viewBox: "0 0 24 24",
								children: {
									key: null,
									ref: null,
									props: {
										d: "M12 2C6.477 2 2 6.477 2 12s4.477 10 10 10 10-4.477 10-10S17.523 2 12 2m4.586 14.424c-.18.295-.563.387-.857.207-2.35-1.434-5.305-1.76-8.786-.963-.335.077-.67-.133-.746-.47-.077-.334.132-.67.47-.745 3.808-.87 7.076-.496 9.712 1.115.293.18.386.563.206.857M17.81 13.7c-.226.367-.706.482-1.072.257-2.687-1.652-6.785-2.13-9.965-1.166-.413.127-.848-.106-.973-.517-.125-.413.108-.848.52-.973 3.632-1.102 8.147-.568 11.234 1.328.366.226.48.707.256 1.072m.105-2.835C14.692 8.95 9.375 8.775 6.297 9.71c-.493.15-1.016-.13-1.166-.624-.148-.495.13-1.017.625-1.167 3.532-1.073 9.404-.866 13.115 1.337.445.264.59.838.327 1.282-.264.443-.838.59-1.282.325",
									},
									_owner: null,
								},
							},
							_owner: null,
						},
					},
					keywords: ["music", "audio"],
					description: "Embed Spotify content.",
					patterns: [{}],
					attributes: {
						providerNameSlug: "spotify",
						responsive: true,
					},
				},
				{
					name: "flickr",
					title: "Flickr",
					icon: {
						key: null,
						ref: null,
						props: {
							viewBox: "0 0 24 24",
							children: {
								key: null,
								ref: null,
								props: {
									d: "m6.5 7c-2.75 0-5 2.25-5 5s2.25 5 5 5 5-2.25 5-5-2.25-5-5-5zm11 0c-2.75 0-5 2.25-5 5s2.25 5 5 5 5-2.25 5-5-2.25-5-5-5z",
								},
								_owner: null,
							},
						},
						_owner: null,
					},
					keywords: ["image"],
					description: "Embed Flickr content.",
					patterns: [{}, {}],
					attributes: {
						providerNameSlug: "flickr",
						responsive: true,
					},
				},
				{
					name: "vimeo",
					title: "Vimeo",
					icon: {
						foreground: "#1ab7ea",
						src: {
							key: null,
							ref: null,
							props: {
								xmlns: "http://www.w3.org/2000/svg",
								viewBox: "0 0 24 24",
								children: {
									key: null,
									ref: null,
									props: {
										children: {
											key: null,
											ref: null,
											props: {
												d: "M22.396 7.164c-.093 2.026-1.507 4.8-4.245 8.32C15.323 19.16 12.93 21 10.97 21c-1.214 0-2.24-1.12-3.08-3.36-.56-2.052-1.118-4.105-1.68-6.158-.622-2.24-1.29-3.36-2.004-3.36-.156 0-.7.328-1.634.98l-.978-1.26c1.027-.903 2.04-1.806 3.037-2.71C6 3.95 7.03 3.328 7.716 3.265c1.62-.156 2.616.95 2.99 3.32.404 2.558.685 4.148.84 4.77.468 2.12.982 3.18 1.543 3.18.435 0 1.09-.687 1.963-2.064.872-1.376 1.34-2.422 1.402-3.142.125-1.187-.343-1.782-1.4-1.782-.5 0-1.013.115-1.542.34 1.023-3.35 2.977-4.976 5.862-4.883 2.14.063 3.148 1.45 3.024 4.16z",
											},
											_owner: null,
										},
									},
									_owner: null,
								},
							},
							_owner: null,
						},
					},
					keywords: ["video"],
					description: "Embed a Vimeo video.",
					patterns: [{}],
					attributes: {
						providerNameSlug: "vimeo",
						responsive: true,
					},
				},
				{
					name: "animoto",
					title: "Animoto",
					icon: {
						key: null,
						ref: null,
						props: {
							viewBox: "0 0 24 24",
							children: [
								{
									key: null,
									ref: null,
									props: {
										d: "m.0206909 21 19.8160091-13.07806 3.5831 6.20826z",
										fill: "#4bc7ee",
									},
									_owner: null,
								},
								{
									key: null,
									ref: null,
									props: {
										d: "m23.7254 19.0205-10.1074-17.18468c-.6421-1.114428-1.7087-1.114428-2.3249 0l-11.2931 19.16418h22.5655c1.279 0 1.8019-.8905 1.1599-1.9795z",
										fill: "#d4cdcb",
									},
									_owner: null,
								},
								{
									key: null,
									ref: null,
									props: {
										d: "m.0206909 21 15.2439091-16.38571 4.3029 7.32271z",
										fill: "#c3d82e",
									},
									_owner: null,
								},
								{
									key: null,
									ref: null,
									props: {
										d: "m13.618 1.83582c-.6421-1.114428-1.7087-1.114428-2.3249 0l-11.2931 19.16418 15.2646-16.38573z",
										fill: "#e4ecb0",
									},
									_owner: null,
								},
								{
									key: null,
									ref: null,
									props: {
										d: "m.0206909 21 19.5468091-9.063 1.6621 2.8344z",
										fill: "#209dbd",
									},
									_owner: null,
								},
								{
									key: null,
									ref: null,
									props: {
										d: "m.0206909 21 17.9209091-11.82623 1.6259 2.76323z",
										fill: "#7cb3c9",
									},
									_owner: null,
								},
							],
						},
						_owner: null,
					},
					description: "Embed an Animoto video.",
					patterns: [{}],
					attributes: {
						providerNameSlug: "animoto",
						responsive: true,
					},
				},
				{
					name: "cloudup",
					title: "Cloudup",
					icon: {
						key: null,
						ref: null,
						props: {
							viewBox: "0 0 24 24",
							xmlns: "http://www.w3.org/2000/svg",
							children: {
								key: null,
								ref: null,
								props: {
									d: "M19 3H5c-1.1 0-2 .9-2 2v14c0 1.1.9 2 2 2h14c1.1 0 2-.9 2-2V5c0-1.1-.9-2-2-2zm.5 16c0 .3-.2.5-.5.5H5c-.3 0-.5-.2-.5-.5V9.8l4.7-5.3H19c.3 0 .5.2.5.5v14zm-6-9.5L16 12l-2.5 2.8 1.1 1L18 12l-3.5-3.5-1 1zm-3 0l-1-1L6 12l3.5 3.8 1.1-1L8 12l2.5-2.5z",
								},
								_owner: null,
							},
						},
						_owner: null,
					},
					description: "Embed Cloudup content.",
					patterns: [{}],
					attributes: {
						providerNameSlug: "cloudup",
						responsive: true,
					},
				},
				{
					name: "collegehumor",
					title: "CollegeHumor",
					icon: {
						key: null,
						ref: null,
						props: {
							viewBox: "0 0 24 24",
							xmlns: "http://www.w3.org/2000/svg",
							children: {
								key: null,
								ref: null,
								props: {
									d: "M19 3H5c-1.1 0-2 .9-2 2v14c0 1.1.9 2 2 2h14c1.1 0 2-.9 2-2V5c0-1.1-.9-2-2-2zm.5 16c0 .3-.2.5-.5.5H5c-.3 0-.5-.2-.5-.5V9.8l4.7-5.3H19c.3 0 .5.2.5.5v14zM10 15l5-3-5-3v6z",
								},
								_owner: null,
							},
						},
						_owner: null,
					},
					description: "Embed CollegeHumor content.",
					scope: ["block"],
					patterns: [],
					attributes: {
						providerNameSlug: "collegehumor",
						responsive: true,
					},
				},
				{
					name: "crowdsignal",
					title: "Crowdsignal",
					icon: {
						key: null,
						ref: null,
						props: {
							viewBox: "0 0 24 24",
							xmlns: "http://www.w3.org/2000/svg",
							children: {
								key: null,
								ref: null,
								props: {
									d: "M19 3H5c-1.1 0-2 .9-2 2v14c0 1.1.9 2 2 2h14c1.1 0 2-.9 2-2V5c0-1.1-.9-2-2-2zm.5 16c0 .3-.2.5-.5.5H5c-.3 0-.5-.2-.5-.5V9.8l4.7-5.3H19c.3 0 .5.2.5.5v14zm-6-9.5L16 12l-2.5 2.8 1.1 1L18 12l-3.5-3.5-1 1zm-3 0l-1-1L6 12l3.5 3.8 1.1-1L8 12l2.5-2.5z",
								},
								_owner: null,
							},
						},
						_owner: null,
					},
					keywords: ["polldaddy", "survey"],
					description: "Embed Crowdsignal (formerly Polldaddy) content.",
					patterns: [{}],
					attributes: {
						providerNameSlug: "crowdsignal",
						responsive: true,
					},
				},
				{
					name: "dailymotion",
					title: "Dailymotion",
					icon: {
						key: null,
						ref: null,
						props: {
							viewBox: "0 0 24 24",
							children: {
								key: null,
								ref: null,
								props: {
									d: "m12.1479 18.5957c-2.4949 0-4.28131-1.7558-4.28131-4.0658 0-2.2176 1.78641-4.0965 4.09651-4.0965 2.2793 0 4.0349 1.7864 4.0349 4.1581 0 2.2794-1.7556 4.0042-3.8501 4.0042zm8.3521-18.5957-4.5329 1v7c-1.1088-1.41691-2.8028-1.8787-4.8049-1.8787-2.09443 0-3.97329.76993-5.5133 2.27917-1.72483 1.66323-2.6489 3.78863-2.6489 6.16033 0 2.5873.98562 4.8049 2.89526 6.499 1.44763 1.2936 3.17251 1.9402 5.17454 1.9402 1.9713 0 3.4498-.5236 4.8973-1.9402v1.9402h4.5329c0-7.6359 0-15.3641 0-23z",
									fill: "#333436",
								},
								_owner: null,
							},
						},
						_owner: null,
					},
					keywords: ["video"],
					description: "Embed a Dailymotion video.",
					patterns: [{}],
					attributes: {
						providerNameSlug: "dailymotion",
						responsive: true,
					},
				},
				{
					name: "imgur",
					title: "Imgur",
					icon: {
						key: null,
						ref: null,
						props: {
							viewBox: "0 0 24 24",
							xmlns: "http://www.w3.org/2000/svg",
							children: {
								key: null,
								ref: null,
								props: {
									d: "M19 3H5c-1.1 0-2 .9-2 2v14c0 1.1.9 2 2 2h14c1.1 0 2-.9 2-2V5c0-1.1-.9-2-2-2zM9.2 4.5H19c.3 0 .5.2.5.5v8.4l-3-2.9c-.3-.3-.8-.3-1 0L11.9 14 9 12c-.3-.2-.6-.2-.8 0l-3.6 2.6V9.8l4.6-5.3zm9.8 15H5c-.3 0-.5-.2-.5-.5v-2.4l4.1-3 3 1.9c.3.2.7.2.9-.1L16 12l3.5 3.4V19c0 .3-.2.5-.5.5z",
								},
								_owner: null,
							},
						},
						_owner: null,
					},
					description: "Embed Imgur content.",
					patterns: [{}],
					attributes: {
						providerNameSlug: "imgur",
						responsive: true,
					},
				},
				{
					name: "issuu",
					title: "Issuu",
					icon: {
						key: null,
						ref: null,
						props: {
							viewBox: "0 0 24 24",
							xmlns: "http://www.w3.org/2000/svg",
							children: {
								key: null,
								ref: null,
								props: {
									d: "M19 3H5c-1.1 0-2 .9-2 2v14c0 1.1.9 2 2 2h14c1.1 0 2-.9 2-2V5c0-1.1-.9-2-2-2zm.5 16c0 .3-.2.5-.5.5H5c-.3 0-.5-.2-.5-.5V9.8l4.7-5.3H19c.3 0 .5.2.5.5v14zm-6-9.5L16 12l-2.5 2.8 1.1 1L18 12l-3.5-3.5-1 1zm-3 0l-1-1L6 12l3.5 3.8 1.1-1L8 12l2.5-2.5z",
								},
								_owner: null,
							},
						},
						_owner: null,
					},
					description: "Embed Issuu content.",
					patterns: [{}],
					attributes: {
						providerNameSlug: "issuu",
						responsive: true,
					},
				},
				{
					name: "kickstarter",
					title: "Kickstarter",
					icon: {
						key: null,
						ref: null,
						props: {
							viewBox: "0 0 24 24",
							xmlns: "http://www.w3.org/2000/svg",
							children: {
								key: null,
								ref: null,
								props: {
									d: "M19 3H5c-1.1 0-2 .9-2 2v14c0 1.1.9 2 2 2h14c1.1 0 2-.9 2-2V5c0-1.1-.9-2-2-2zm.5 16c0 .3-.2.5-.5.5H5c-.3 0-.5-.2-.5-.5V9.8l4.7-5.3H19c.3 0 .5.2.5.5v14zm-6-9.5L16 12l-2.5 2.8 1.1 1L18 12l-3.5-3.5-1 1zm-3 0l-1-1L6 12l3.5 3.8 1.1-1L8 12l2.5-2.5z",
								},
								_owner: null,
							},
						},
						_owner: null,
					},
					description: "Embed Kickstarter content.",
					patterns: [{}, {}],
					attributes: {
						providerNameSlug: "kickstarter",
						responsive: true,
					},
				},
				{
					name: "meetup-com",
					title: "Meetup.com",
					icon: {
						key: null,
						ref: null,
						props: {
							viewBox: "0 0 24 24",
							xmlns: "http://www.w3.org/2000/svg",
							children: {
								key: null,
								ref: null,
								props: {
									d: "M19 3H5c-1.1 0-2 .9-2 2v14c0 1.1.9 2 2 2h14c1.1 0 2-.9 2-2V5c0-1.1-.9-2-2-2zm.5 16c0 .3-.2.5-.5.5H5c-.3 0-.5-.2-.5-.5V9.8l4.7-5.3H19c.3 0 .5.2.5.5v14zm-6-9.5L16 12l-2.5 2.8 1.1 1L18 12l-3.5-3.5-1 1zm-3 0l-1-1L6 12l3.5 3.8 1.1-1L8 12l2.5-2.5z",
								},
								_owner: null,
							},
						},
						_owner: null,
					},
					description: "Embed Meetup.com content.",
					patterns: [{}],
					attributes: {
						providerNameSlug: "meetup-com",
						responsive: true,
					},
				},
				{
					name: "mixcloud",
					title: "Mixcloud",
					icon: {
						key: null,
						ref: null,
						props: {
							viewBox: "0 0 24 24",
							xmlns: "http://www.w3.org/2000/svg",
							children: {
								key: null,
								ref: null,
								props: {
									d: "M19 3H5c-1.1 0-2 .9-2 2v14c0 1.1.9 2 2 2h14c1.1 0 2-.9 2-2V5c0-1.1-.9-2-2-2zm.5 16c0 .3-.2.5-.5.5H5c-.3 0-.5-.2-.5-.5V9.8l4.7-5.3H19c.3 0 .5.2.5.5v14zM13.2 7.7c-.4.4-.7 1.1-.7 1.9v3.7c-.4-.3-.8-.4-1.3-.4-1.2 0-2.2 1-2.2 2.2 0 1.2 1 2.2 2.2 2.2.5 0 1-.2 1.4-.5.9-.6 1.4-1.6 1.4-2.6V9.6c0-.4.1-.6.2-.8.3-.3 1-.3 1.6-.3h.2V7h-.2c-.7 0-1.8 0-2.6.7z",
								},
								_owner: null,
							},
						},
						_owner: null,
					},
					keywords: ["music", "audio"],
					description: "Embed Mixcloud content.",
					patterns: [{}],
					attributes: {
						providerNameSlug: "mixcloud",
						responsive: true,
					},
				},
				{
					name: "reddit",
					title: "Reddit",
					icon: {
						key: null,
						ref: null,
						props: {
							viewBox: "0 0 24 24",
							children: {
								key: null,
								ref: null,
								props: {
									d: "M22 11.816c0-1.256-1.02-2.277-2.277-2.277-.593 0-1.122.24-1.526.613-1.48-.965-3.455-1.594-5.647-1.69l1.17-3.702 3.18.75c.01 1.027.847 1.86 1.877 1.86 1.035 0 1.877-.84 1.877-1.877 0-1.035-.842-1.877-1.877-1.877-.77 0-1.43.466-1.72 1.13L13.55 3.92c-.204-.047-.4.067-.46.26l-1.35 4.27c-2.317.037-4.412.67-5.97 1.67-.402-.355-.917-.58-1.493-.58C3.02 9.54 2 10.56 2 11.815c0 .814.433 1.523 1.078 1.925-.037.222-.06.445-.06.673 0 3.292 4.01 5.97 8.94 5.97s8.94-2.678 8.94-5.97c0-.214-.02-.424-.052-.632.687-.39 1.154-1.12 1.154-1.964zm-3.224-7.422c.606 0 1.1.493 1.1 1.1s-.493 1.1-1.1 1.1-1.1-.494-1.1-1.1.493-1.1 1.1-1.1zm-16 7.422c0-.827.673-1.5 1.5-1.5.313 0 .598.103.838.27-.85.675-1.477 1.478-1.812 2.36-.32-.274-.525-.676-.525-1.13zm9.183 7.79c-4.502 0-8.165-2.33-8.165-5.193S7.457 9.22 11.96 9.22s8.163 2.33 8.163 5.193-3.663 5.193-8.164 5.193zM20.635 13c-.326-.89-.948-1.7-1.797-2.383.247-.186.55-.3.882-.3.827 0 1.5.672 1.5 1.5 0 .482-.23.91-.586 1.184zm-11.64 1.704c-.76 0-1.397-.616-1.397-1.376 0-.76.636-1.397 1.396-1.397.76 0 1.376.638 1.376 1.398 0 .76-.616 1.376-1.376 1.376zm7.405-1.376c0 .76-.615 1.376-1.375 1.376s-1.4-.616-1.4-1.376c0-.76.64-1.397 1.4-1.397.76 0 1.376.638 1.376 1.398zm-1.17 3.38c.15.152.15.398 0 .55-.675.674-1.728 1.002-3.22 1.002l-.01-.002-.012.002c-1.492 0-2.544-.328-3.218-1.002-.152-.152-.152-.398 0-.55.152-.152.4-.15.55 0 .52.52 1.394.775 2.67.775l.01.002.01-.002c1.276 0 2.15-.253 2.67-.775.15-.152.398-.152.55 0z",
								},
								_owner: null,
							},
						},
						_owner: null,
					},
					description: "Embed a Reddit thread.",
					patterns: [{}],
					attributes: {
						providerNameSlug: "reddit",
						responsive: true,
					},
				},
				{
					name: "reverbnation",
					title: "ReverbNation",
					icon: {
						key: null,
						ref: null,
						props: {
							viewBox: "0 0 24 24",
							xmlns: "http://www.w3.org/2000/svg",
							children: {
								key: null,
								ref: null,
								props: {
									d: "M19 3H5c-1.1 0-2 .9-2 2v14c0 1.1.9 2 2 2h14c1.1 0 2-.9 2-2V5c0-1.1-.9-2-2-2zm.5 16c0 .3-.2.5-.5.5H5c-.3 0-.5-.2-.5-.5V9.8l4.7-5.3H19c.3 0 .5.2.5.5v14zM13.2 7.7c-.4.4-.7 1.1-.7 1.9v3.7c-.4-.3-.8-.4-1.3-.4-1.2 0-2.2 1-2.2 2.2 0 1.2 1 2.2 2.2 2.2.5 0 1-.2 1.4-.5.9-.6 1.4-1.6 1.4-2.6V9.6c0-.4.1-.6.2-.8.3-.3 1-.3 1.6-.3h.2V7h-.2c-.7 0-1.8 0-2.6.7z",
								},
								_owner: null,
							},
						},
						_owner: null,
					},
					description: "Embed ReverbNation content.",
					patterns: [{}],
					attributes: {
						providerNameSlug: "reverbnation",
						responsive: true,
					},
				},
				{
					name: "screencast",
					title: "Screencast",
					icon: {
						key: null,
						ref: null,
						props: {
							viewBox: "0 0 24 24",
							xmlns: "http://www.w3.org/2000/svg",
							children: {
								key: null,
								ref: null,
								props: {
									d: "M19 3H5c-1.1 0-2 .9-2 2v14c0 1.1.9 2 2 2h14c1.1 0 2-.9 2-2V5c0-1.1-.9-2-2-2zm.5 16c0 .3-.2.5-.5.5H5c-.3 0-.5-.2-.5-.5V9.8l4.7-5.3H19c.3 0 .5.2.5.5v14zM10 15l5-3-5-3v6z",
								},
								_owner: null,
							},
						},
						_owner: null,
					},
					description: "Embed Screencast content.",
					patterns: [{}],
					attributes: {
						providerNameSlug: "screencast",
						responsive: true,
					},
				},
				{
					name: "scribd",
					title: "Scribd",
					icon: {
						key: null,
						ref: null,
						props: {
							viewBox: "0 0 24 24",
							xmlns: "http://www.w3.org/2000/svg",
							children: {
								key: null,
								ref: null,
								props: {
									d: "M19 3H5c-1.1 0-2 .9-2 2v14c0 1.1.9 2 2 2h14c1.1 0 2-.9 2-2V5c0-1.1-.9-2-2-2zm.5 16c0 .3-.2.5-.5.5H5c-.3 0-.5-.2-.5-.5V9.8l4.7-5.3H19c.3 0 .5.2.5.5v14zm-6-9.5L16 12l-2.5 2.8 1.1 1L18 12l-3.5-3.5-1 1zm-3 0l-1-1L6 12l3.5 3.8 1.1-1L8 12l2.5-2.5z",
								},
								_owner: null,
							},
						},
						_owner: null,
					},
					description: "Embed Scribd content.",
					patterns: [{}],
					attributes: {
						providerNameSlug: "scribd",
						responsive: true,
					},
				},
				{
					name: "slideshare",
					title: "Slideshare",
					icon: {
						key: null,
						ref: null,
						props: {
							viewBox: "0 0 24 24",
							xmlns: "http://www.w3.org/2000/svg",
							children: {
								key: null,
								ref: null,
								props: {
									d: "M19 3H5c-1.1 0-2 .9-2 2v14c0 1.1.9 2 2 2h14c1.1 0 2-.9 2-2V5c0-1.1-.9-2-2-2zm.5 16c0 .3-.2.5-.5.5H5c-.3 0-.5-.2-.5-.5V9.8l4.7-5.3H19c.3 0 .5.2.5.5v14zm-6-9.5L16 12l-2.5 2.8 1.1 1L18 12l-3.5-3.5-1 1zm-3 0l-1-1L6 12l3.5 3.8 1.1-1L8 12l2.5-2.5z",
								},
								_owner: null,
							},
						},
						_owner: null,
					},
					description: "Embed Slideshare content.",
					patterns: [{}],
					attributes: {
						providerNameSlug: "slideshare",
						responsive: true,
					},
				},
				{
					name: "smugmug",
					title: "SmugMug",
					icon: {
						key: null,
						ref: null,
						props: {
							viewBox: "0 0 24 24",
							xmlns: "http://www.w3.org/2000/svg",
							children: {
								key: null,
								ref: null,
								props: {
									d: "M19 3H5c-1.1 0-2 .9-2 2v14c0 1.1.9 2 2 2h14c1.1 0 2-.9 2-2V5c0-1.1-.9-2-2-2zM9.2 4.5H19c.3 0 .5.2.5.5v8.4l-3-2.9c-.3-.3-.8-.3-1 0L11.9 14 9 12c-.3-.2-.6-.2-.8 0l-3.6 2.6V9.8l4.6-5.3zm9.8 15H5c-.3 0-.5-.2-.5-.5v-2.4l4.1-3 3 1.9c.3.2.7.2.9-.1L16 12l3.5 3.4V19c0 .3-.2.5-.5.5z",
								},
								_owner: null,
							},
						},
						_owner: null,
					},
					description: "Embed SmugMug content.",
					patterns: [{}],
					attributes: {
						providerNameSlug: "smugmug",
						previewable: false,
						responsive: true,
					},
				},
				{
					name: "speaker-deck",
					title: "Speaker Deck",
					icon: {
						key: null,
						ref: null,
						props: {
							viewBox: "0 0 24 24",
							xmlns: "http://www.w3.org/2000/svg",
							children: {
								key: null,
								ref: null,
								props: {
									d: "M19 3H5c-1.1 0-2 .9-2 2v14c0 1.1.9 2 2 2h14c1.1 0 2-.9 2-2V5c0-1.1-.9-2-2-2zm.5 16c0 .3-.2.5-.5.5H5c-.3 0-.5-.2-.5-.5V9.8l4.7-5.3H19c.3 0 .5.2.5.5v14zm-6-9.5L16 12l-2.5 2.8 1.1 1L18 12l-3.5-3.5-1 1zm-3 0l-1-1L6 12l3.5 3.8 1.1-1L8 12l2.5-2.5z",
								},
								_owner: null,
							},
						},
						_owner: null,
					},
					description: "Embed Speaker Deck content.",
					patterns: [{}],
					attributes: {
						providerNameSlug: "speaker-deck",
						responsive: true,
					},
				},
				{
					name: "tiktok",
					title: "TikTok",
					icon: {
						key: null,
						ref: null,
						props: {
							viewBox: "0 0 24 24",
							xmlns: "http://www.w3.org/2000/svg",
							children: {
								key: null,
								ref: null,
								props: {
									d: "M19 3H5c-1.1 0-2 .9-2 2v14c0 1.1.9 2 2 2h14c1.1 0 2-.9 2-2V5c0-1.1-.9-2-2-2zm.5 16c0 .3-.2.5-.5.5H5c-.3 0-.5-.2-.5-.5V9.8l4.7-5.3H19c.3 0 .5.2.5.5v14zM10 15l5-3-5-3v6z",
								},
								_owner: null,
							},
						},
						_owner: null,
					},
					keywords: ["video"],
					description: "Embed a TikTok video.",
					patterns: [{}],
					attributes: {
						providerNameSlug: "tiktok",
						responsive: true,
					},
				},
				{
					name: "ted",
					title: "TED",
					icon: {
						key: null,
						ref: null,
						props: {
							viewBox: "0 0 24 24",
							xmlns: "http://www.w3.org/2000/svg",
							children: {
								key: null,
								ref: null,
								props: {
									d: "M19 3H5c-1.1 0-2 .9-2 2v14c0 1.1.9 2 2 2h14c1.1 0 2-.9 2-2V5c0-1.1-.9-2-2-2zm.5 16c0 .3-.2.5-.5.5H5c-.3 0-.5-.2-.5-.5V9.8l4.7-5.3H19c.3 0 .5.2.5.5v14zM10 15l5-3-5-3v6z",
								},
								_owner: null,
							},
						},
						_owner: null,
					},
					description: "Embed a TED video.",
					patterns: [{}],
					attributes: {
						providerNameSlug: "ted",
						responsive: true,
					},
				},
				{
					name: "tumblr",
					title: "Tumblr",
					icon: {
						foreground: "#35465c",
						src: {
							key: null,
							ref: null,
							props: {
								viewBox: "0 0 24 24",
								children: {
									key: null,
									ref: null,
									props: {
										d: "M19 3H5a2 2 0 00-2 2v14c0 1.1.9 2 2 2h14a2 2 0 002-2V5a2 2 0 00-2-2zm-5.69 14.66c-2.72 0-3.1-1.9-3.1-3.16v-3.56H8.49V8.99c1.7-.62 2.54-1.99 2.64-2.87 0-.06.06-.41.06-.58h1.9v3.1h2.17v2.3h-2.18v3.1c0 .47.13 1.3 1.2 1.26h1.1v2.36c-1.01.02-2.07 0-2.07 0z",
									},
									_owner: null,
								},
							},
							_owner: null,
						},
					},
					keywords: ["social"],
					description: "Embed a Tumblr post.",
					patterns: [{}],
					attributes: {
						providerNameSlug: "tumblr",
						responsive: true,
					},
				},
				{
					name: "videopress",
					title: "VideoPress",
					icon: {
						key: null,
						ref: null,
						props: {
							viewBox: "0 0 24 24",
							xmlns: "http://www.w3.org/2000/svg",
							children: {
								key: null,
								ref: null,
								props: {
									d: "M19 3H5c-1.1 0-2 .9-2 2v14c0 1.1.9 2 2 2h14c1.1 0 2-.9 2-2V5c0-1.1-.9-2-2-2zm.5 16c0 .3-.2.5-.5.5H5c-.3 0-.5-.2-.5-.5V9.8l4.7-5.3H19c.3 0 .5.2.5.5v14zM10 15l5-3-5-3v6z",
								},
								_owner: null,
							},
						},
						_owner: null,
					},
					keywords: ["video"],
					description: "Embed a VideoPress video.",
					patterns: [{}],
					attributes: {
						providerNameSlug: "videopress",
						responsive: true,
					},
				},
				{
					name: "wordpress-tv",
					title: "WordPress.tv",
					icon: {
						key: null,
						ref: null,
						props: {
							viewBox: "0 0 24 24",
							xmlns: "http://www.w3.org/2000/svg",
							children: {
								key: null,
								ref: null,
								props: {
									d: "M19 3H5c-1.1 0-2 .9-2 2v14c0 1.1.9 2 2 2h14c1.1 0 2-.9 2-2V5c0-1.1-.9-2-2-2zm.5 16c0 .3-.2.5-.5.5H5c-.3 0-.5-.2-.5-.5V9.8l4.7-5.3H19c.3 0 .5.2.5.5v14zM10 15l5-3-5-3v6z",
								},
								_owner: null,
							},
						},
						_owner: null,
					},
					description: "Embed a WordPress.tv video.",
					patterns: [{}],
					attributes: {
						providerNameSlug: "wordpress-tv",
						responsive: true,
					},
				},
				{
					name: "amazon-kindle",
					title: "Amazon Kindle",
					icon: {
						key: null,
						ref: null,
						props: {
							viewBox: "0 0 24 24",
							children: [
								{
									key: null,
									ref: null,
									props: {
										d: "M18.42 14.58c-.51-.66-1.05-1.23-1.05-2.5V7.87c0-1.8.15-3.45-1.2-4.68-1.05-1.02-2.79-1.35-4.14-1.35-2.6 0-5.52.96-6.12 4.14-.06.36.18.54.4.57l2.66.3c.24-.03.42-.27.48-.5.24-1.12 1.17-1.63 2.2-1.63.56 0 1.22.21 1.55.7.4.56.33 1.31.33 1.97v.36c-1.59.18-3.66.27-5.16.93a4.63 4.63 0 0 0-2.93 4.44c0 2.82 1.8 4.23 4.1 4.23 1.95 0 3.03-.45 4.53-1.98.51.72.66 1.08 1.59 1.83.18.09.45.09.63-.1v.04l2.1-1.8c.24-.21.2-.48.03-.75zm-5.4-1.2c-.45.75-1.14 1.23-1.92 1.23-1.05 0-1.65-.81-1.65-1.98 0-2.31 2.1-2.73 4.08-2.73v.6c0 1.05.03 1.92-.5 2.88z",
									},
									_owner: null,
								},
								{
									key: null,
									ref: null,
									props: {
										d: "M21.69 19.2a17.62 17.62 0 0 1-21.6-1.57c-.23-.2 0-.5.28-.33a23.88 23.88 0 0 0 20.93 1.3c.45-.19.84.3.39.6z",
									},
									_owner: null,
								},
								{
									key: null,
									ref: null,
									props: {
										d: "M22.8 17.96c-.36-.45-2.22-.2-3.1-.12-.23.03-.3-.18-.05-.36 1.5-1.05 3.96-.75 4.26-.39.3.36-.1 2.82-1.5 4.02-.21.18-.42.1-.3-.15.3-.8 1.02-2.58.69-3z",
									},
									_owner: null,
								},
							],
						},
						_owner: null,
					},
					keywords: ["ebook"],
					description: "Embed Amazon Kindle content.",
					patterns: [{}, {}],
					attributes: {
						providerNameSlug: "amazon-kindle",
					},
				},
			],
			deprecated: [
				{
					attributes: {
						url: {
							type: "string",
						},
						caption: {
							type: "string",
							source: "html",
							selector: "figcaption",
						},
						type: {
							type: "string",
						},
						providerNameSlug: {
							type: "string",
						},
						allowResponsive: {
							type: "boolean",
							default: true,
						},
						responsive: {
							type: "boolean",
							default: false,
						},
						previewable: {
							type: "boolean",
							default: true,
						},
						className: {
							type: "string",
						},
					},
				},
			],
		},
		{
			name: "core/file",
			icon: {
				src: {
					key: null,
					ref: null,
					props: {
						viewBox: "0 0 24 24",
						xmlns: "http://www.w3.org/2000/svg",
						children: {
							key: null,
							ref: null,
							props: {
								d: "M19 6.2h-5.9l-.6-1.1c-.3-.7-1-1.1-1.8-1.1H5c-1.1 0-2 .9-2 2v11.8c0 1.1.9 2 2 2h14c1.1 0 2-.9 2-2V8.2c0-1.1-.9-2-2-2zm.5 11.6c0 .3-.2.5-.5.5H5c-.3 0-.5-.2-.5-.5V6c0-.3.2-.5.5-.5h5.8c.2 0 .4.1.4.3l1 2H19c.3 0 .5.2.5.5v9.5z",
							},
							_owner: null,
						},
					},
					_owner: null,
				},
			},
			keywords: ["document", "pdf", "download"],
			attributes: {
				id: {
					type: "number",
				},
				href: {
					type: "string",
				},
				fileName: {
					type: "string",
					source: "html",
					selector: "a:not([download])",
				},
				textLinkHref: {
					type: "string",
					source: "attribute",
					selector: "a:not([download])",
					attribute: "href",
				},
				textLinkTarget: {
					type: "string",
					source: "attribute",
					selector: "a:not([download])",
					attribute: "target",
				},
				showDownloadButton: {
					type: "boolean",
					default: true,
				},
				downloadButtonText: {
					type: "string",
					source: "html",
					selector: "a[download]",
				},
				align: {
					type: "string",
					enum: ["left", "center", "right", "wide", "full", ""],
				},
				anchor: {
					type: "string",
					source: "attribute",
					attribute: "id",
					selector: "*",
				},
				className: {
					type: "string",
				},
			},
			providesContext: {},
			usesContext: [],
			supports: {
				anchor: true,
				align: true,
			},
			styles: [],
			apiVersion: 2,
			category: "media",
			editorStyle: "wp-block-file-editor",
			style: "wp-block-file",
			title: "File",
			description: "Add a link to a downloadable file.",
			transforms: {
				from: [
					{
						type: "files",
						priority: 15,
					},
					{
						type: "block",
						blocks: ["core/audio"],
					},
					{
						type: "block",
						blocks: ["core/video"],
					},
					{
						type: "block",
						blocks: ["core/image"],
					},
				],
				to: [
					{
						type: "block",
						blocks: ["core/audio"],
					},
					{
						type: "block",
						blocks: ["core/video"],
					},
					{
						type: "block",
						blocks: ["core/image"],
					},
				],
			},
			variations: [],
		},
		{
			name: "core/group",
			icon: {
				src: {
					key: null,
					ref: null,
					props: {
						viewBox: "0 0 24 24",
						xmlns: "http://www.w3.org/2000/svg",
						children: {
							key: null,
							ref: null,
							props: {
								d: "M18 4h-7c-1.1 0-2 .9-2 2v3H6c-1.1 0-2 .9-2 2v7c0 1.1.9 2 2 2h7c1.1 0 2-.9 2-2v-3h3c1.1 0 2-.9 2-2V6c0-1.1-.9-2-2-2zm-4.5 14c0 .3-.2.5-.5.5H6c-.3 0-.5-.2-.5-.5v-7c0-.3.2-.5.5-.5h3V13c0 1.1.9 2 2 2h2.5v3zm0-4.5H11c-.3 0-.5-.2-.5-.5v-2.5H13c.3 0 .5.2.5.5v2.5zm5-.5c0 .3-.2.5-.5.5h-3V11c0-1.1-.9-2-2-2h-2.5V6c0-.3.2-.5.5-.5h7c.3 0 .5.2.5.5v7z",
							},
							_owner: null,
						},
					},
					_owner: null,
				},
			},
			keywords: ["container", "wrapper", "row", "section"],
			attributes: {
				tagName: {
					type: "string",
					default: "div",
				},
				templateLock: {
					type: "string",
				},
				align: {
					type: "string",
					enum: ["left", "center", "right", "wide", "full", ""],
				},
				anchor: {
					type: "string",
					source: "attribute",
					attribute: "id",
					selector: "*",
				},
				className: {
					type: "string",
				},
				backgroundColor: {
					type: "string",
				},
				textColor: {
					type: "string",
				},
				gradient: {
					type: "string",
				},
				style: {
					type: "object",
				},
			},
			providesContext: {},
			usesContext: [],
			supports: {
				align: ["wide", "full"],
				anchor: true,
				html: false,
				color: {
					gradients: true,
					link: true,
				},
				spacing: {
					padding: true,
				},
				__experimentalBorder: {
					radius: true,
				},
			},
			styles: [],
			apiVersion: 2,
			category: "design",
			editorStyle: "wp-block-group-editor",
			style: "wp-block-group",
			title: "Group",
			description: "Combine blocks into a group.",
			example: {
				attributes: {
					style: {
						color: {
							text: "#000000",
							background: "#ffffff",
						},
					},
				},
				innerBlocks: [
					{
						name: "core/paragraph",
						attributes: {
							customTextColor: "#cf2e2e",
							fontSize: "large",
							content: "One.",
						},
					},
					{
						name: "core/paragraph",
						attributes: {
							customTextColor: "#ff6900",
							fontSize: "large",
							content: "Two.",
						},
					},
					{
						name: "core/paragraph",
						attributes: {
							customTextColor: "#fcb900",
							fontSize: "large",
							content: "Three.",
						},
					},
					{
						name: "core/paragraph",
						attributes: {
							customTextColor: "#00d084",
							fontSize: "large",
							content: "Four.",
						},
					},
					{
						name: "core/paragraph",
						attributes: {
							customTextColor: "#0693e3",
							fontSize: "large",
							content: "Five.",
						},
					},
					{
						name: "core/paragraph",
						attributes: {
							customTextColor: "#9b51e0",
							fontSize: "large",
							content: "Six.",
						},
					},
				],
			},
			transforms: {
				from: [
					{
						type: "block",
						isMultiBlock: true,
						blocks: ["*"],
					},
				],
			},
			deprecated: [
				{
					attributes: {
						backgroundColor: {
							type: "string",
						},
						customBackgroundColor: {
							type: "string",
						},
						textColor: {
							type: "string",
						},
						customTextColor: {
							type: "string",
						},
						align: {
							type: "string",
							enum: ["left", "center", "right", "wide", "full", ""],
						},
						anchor: {
							type: "string",
							source: "attribute",
							attribute: "id",
							selector: "*",
						},
						className: {
							type: "string",
						},
					},
					supports: {
						align: ["wide", "full"],
						anchor: true,
						html: false,
					},
				},
				{
					attributes: {
						backgroundColor: {
							type: "string",
						},
						customBackgroundColor: {
							type: "string",
						},
						textColor: {
							type: "string",
						},
						customTextColor: {
							type: "string",
						},
						align: {
							type: "string",
							enum: ["left", "center", "right", "wide", "full", ""],
						},
						anchor: {
							type: "string",
							source: "attribute",
							attribute: "id",
							selector: "*",
						},
						className: {
							type: "string",
						},
					},
					supports: {
						align: ["wide", "full"],
						anchor: true,
						html: false,
					},
				},
				{
					attributes: {
						backgroundColor: {
							type: "string",
						},
						customBackgroundColor: {
							type: "string",
						},
						align: {
							type: "string",
							enum: ["left", "center", "right", "wide", "full", ""],
						},
						anchor: {
							type: "string",
							source: "attribute",
							attribute: "id",
							selector: "*",
						},
						className: {
							type: "string",
						},
					},
					supports: {
						align: ["wide", "full"],
						anchor: true,
						html: false,
					},
				},
			],
			variations: [],
		},
		{
			name: "core/freeform",
			icon: {
				src: {
					key: null,
					ref: null,
					props: {
						viewBox: "0 0 24 24",
						xmlns: "http://www.w3.org/2000/svg",
						children: {
							key: null,
							ref: null,
							props: {
								d: "M20 6H4c-1.1 0-2 .9-2 2v9c0 1.1.9 2 2 2h16c1.1 0 2-.9 2-2V8c0-1.1-.9-2-2-2zm.5 11c0 .3-.2.5-.5.5H4c-.3 0-.5-.2-.5-.5V8c0-.3.2-.5.5-.5h16c.3 0 .5.2.5.5v9zM10 10H8v2h2v-2zm-5 2h2v-2H5v2zm8-2h-2v2h2v-2zm-5 6h8v-2H8v2zm6-4h2v-2h-2v2zm3 0h2v-2h-2v2zm0 4h2v-2h-2v2zM5 16h2v-2H5v2z",
							},
							_owner: null,
						},
					},
					_owner: null,
				},
			},
			keywords: [],
			attributes: {
				content: {
					type: "string",
					source: "html",
				},
			},
			providesContext: {},
			usesContext: [],
			supports: {
				className: false,
				customClassName: false,
				reusable: false,
			},
			styles: [],
			apiVersion: 2,
			category: "text",
			editorStyle: "wp-block-freeform-editor",
			title: "Classic",
			description: "Use the classic WordPress editor.",
			variations: [],
		},
		{
			name: "core/html",
			icon: {
				src: {
					key: null,
					ref: null,
					props: {
						viewBox: "0 0 24 24",
						xmlns: "http://www.w3.org/2000/svg",
						children: {
							key: null,
							ref: null,
							props: {
								d: "M4.8 11.4H2.1V9H1v6h1.1v-2.6h2.7V15h1.1V9H4.8v2.4zm1.9-1.3h1.7V15h1.1v-4.9h1.7V9H6.7v1.1zM16.2 9l-1.5 2.7L13.3 9h-.9l-.8 6h1.1l.5-4 1.5 2.8 1.5-2.8.5 4h1.1L17 9h-.8zm3.8 5V9h-1.1v6h3.6v-1H20z",
							},
							_owner: null,
						},
					},
					_owner: null,
				},
			},
			keywords: ["embed"],
			attributes: {
				content: {
					type: "string",
					source: "html",
				},
			},
			providesContext: {},
			usesContext: [],
			supports: {
				customClassName: false,
				className: false,
				html: false,
			},
			styles: [],
			apiVersion: 2,
			category: "widgets",
			editorStyle: "wp-block-html-editor",
			title: "Custom HTML",
			description: "Add custom HTML code and preview it as you edit.",
			example: {
				attributes: {
					content:
						"<marquee>Welcome to the wonderful world of blocks…</marquee>",
				},
			},
			transforms: {
				from: [
					{
						type: "block",
						blocks: ["core/code"],
					},
				],
			},
			variations: [],
		},
		{
			name: "core/media-text",
			icon: {
				src: {
					key: null,
					ref: null,
					props: {
						xmlns: "http://www.w3.org/2000/svg",
						viewBox: "0 0 24 24",
						children: {
							key: null,
							ref: null,
							props: {
								d: "M4 17h7V6H4v11zm9-10v1.5h7V7h-7zm0 5.5h7V11h-7v1.5zm0 4h7V15h-7v1.5z",
							},
							_owner: null,
						},
					},
					_owner: null,
				},
			},
			keywords: ["image", "video"],
			attributes: {
				align: {
					type: "string",
					default: "wide",
				},
				mediaAlt: {
					type: "string",
					source: "attribute",
					selector: "figure img",
					attribute: "alt",
					default: "",
				},
				mediaPosition: {
					type: "string",
					default: "left",
				},
				mediaId: {
					type: "number",
				},
				mediaUrl: {
					type: "string",
					source: "attribute",
					selector: "figure video,figure img",
					attribute: "src",
				},
				mediaLink: {
					type: "string",
				},
				linkDestination: {
					type: "string",
				},
				linkTarget: {
					type: "string",
					source: "attribute",
					selector: "figure a",
					attribute: "target",
				},
				href: {
					type: "string",
					source: "attribute",
					selector: "figure a",
					attribute: "href",
				},
				rel: {
					type: "string",
					source: "attribute",
					selector: "figure a",
					attribute: "rel",
				},
				linkClass: {
					type: "string",
					source: "attribute",
					selector: "figure a",
					attribute: "class",
				},
				mediaType: {
					type: "string",
				},
				mediaWidth: {
					type: "number",
					default: 50,
				},
				mediaSizeSlug: {
					type: "string",
				},
				isStackedOnMobile: {
					type: "boolean",
					default: true,
				},
				verticalAlignment: {
					type: "string",
				},
				imageFill: {
					type: "boolean",
				},
				focalPoint: {
					type: "object",
				},
				anchor: {
					type: "string",
					source: "attribute",
					attribute: "id",
					selector: "*",
				},
				className: {
					type: "string",
				},
				backgroundColor: {
					type: "string",
				},
				textColor: {
					type: "string",
				},
				gradient: {
					type: "string",
				},
				style: {
					type: "object",
				},
			},
			providesContext: {},
			usesContext: [],
			supports: {
				anchor: true,
				align: ["wide", "full"],
				html: false,
				color: {
					gradients: true,
					link: true,
				},
			},
			styles: [],
			apiVersion: 2,
			category: "media",
			editorStyle: "wp-block-media-text-editor",
			style: "wp-block-media-text",
			title: "Media & Text",
			description: "Set media and words side-by-side for a richer layout.",
			example: {
				attributes: {
					mediaType: "image",
					mediaUrl:
						"https://s.w.org/images/core/5.3/Biologia_Centrali-Americana_-_Cantorchilus_semibadius_1902.jpg",
				},
				innerBlocks: [
					{
						name: "core/paragraph",
						attributes: {
							content: "The wren<br>Earns his living<br>Noiselessly.",
						},
					},
					{
						name: "core/paragraph",
						attributes: {
							content: "— Kobayashi Issa (一茶)",
						},
					},
				],
			},
			transforms: {
				from: [
					{
						type: "block",
						blocks: ["core/image"],
					},
					{
						type: "block",
						blocks: ["core/video"],
					},
				],
				to: [
					{
						type: "block",
						blocks: ["core/image"],
					},
					{
						type: "block",
						blocks: ["core/video"],
					},
				],
			},
			deprecated: [
				{
					attributes: {
						align: {
							type: "string",
							default: "wide",
						},
						backgroundColor: {
							type: "string",
						},
						mediaAlt: {
							type: "string",
							source: "attribute",
							selector: "figure img",
							attribute: "alt",
							default: "",
						},
						mediaPosition: {
							type: "string",
							default: "left",
						},
						mediaId: {
							type: "number",
						},
						mediaType: {
							type: "string",
						},
						mediaWidth: {
							type: "number",
							default: 50,
						},
						isStackedOnMobile: {
							type: "boolean",
							default: true,
						},
						customBackgroundColor: {
							type: "string",
						},
						mediaLink: {
							type: "string",
						},
						linkDestination: {
							type: "string",
						},
						linkTarget: {
							type: "string",
							source: "attribute",
							selector: "figure a",
							attribute: "target",
						},
						href: {
							type: "string",
							source: "attribute",
							selector: "figure a",
							attribute: "href",
						},
						rel: {
							type: "string",
							source: "attribute",
							selector: "figure a",
							attribute: "rel",
						},
						linkClass: {
							type: "string",
							source: "attribute",
							selector: "figure a",
							attribute: "class",
						},
						verticalAlignment: {
							type: "string",
						},
						imageFill: {
							type: "boolean",
						},
						focalPoint: {
							type: "object",
						},
						className: {
							type: "string",
						},
					},
				},
				{
					attributes: {
						align: {
							type: "string",
							default: "wide",
						},
						backgroundColor: {
							type: "string",
						},
						mediaAlt: {
							type: "string",
							source: "attribute",
							selector: "figure img",
							attribute: "alt",
							default: "",
						},
						mediaPosition: {
							type: "string",
							default: "left",
						},
						mediaId: {
							type: "number",
						},
						mediaType: {
							type: "string",
						},
						mediaWidth: {
							type: "number",
							default: 50,
						},
						isStackedOnMobile: {
							type: "boolean",
							default: true,
						},
						customBackgroundColor: {
							type: "string",
						},
						mediaUrl: {
							type: "string",
							source: "attribute",
							selector: "figure video,figure img",
							attribute: "src",
						},
						verticalAlignment: {
							type: "string",
						},
						imageFill: {
							type: "boolean",
						},
						focalPoint: {
							type: "object",
						},
						className: {
							type: "string",
						},
					},
				},
				{
					attributes: {
						align: {
							type: "string",
							default: "wide",
						},
						backgroundColor: {
							type: "string",
						},
						mediaAlt: {
							type: "string",
							source: "attribute",
							selector: "figure img",
							attribute: "alt",
							default: "",
						},
						mediaPosition: {
							type: "string",
							default: "left",
						},
						mediaId: {
							type: "number",
						},
						mediaType: {
							type: "string",
						},
						mediaWidth: {
							type: "number",
							default: 50,
						},
						isStackedOnMobile: {
							type: "boolean",
							default: true,
						},
						customBackgroundColor: {
							type: "string",
						},
						mediaUrl: {
							type: "string",
							source: "attribute",
							selector: "figure video,figure img",
							attribute: "src",
						},
						className: {
							type: "string",
						},
					},
				},
			],
			variations: [],
		},
		{
			name: "core/latest-comments",
			icon: {
				src: {
					key: null,
					ref: null,
					props: {
						viewBox: "0 0 24 24",
						xmlns: "http://www.w3.org/2000/svg",
						children: {
							key: null,
							ref: null,
							props: {
								d: "M18 4H6c-1.1 0-2 .9-2 2v12.9c0 .6.5 1.1 1.1 1.1.3 0 .5-.1.8-.3L8.5 17H18c1.1 0 2-.9 2-2V6c0-1.1-.9-2-2-2zm.5 11c0 .3-.2.5-.5.5H7.9l-2.4 2.4V6c0-.3.2-.5.5-.5h12c.3 0 .5.2.5.5v9z",
							},
							_owner: null,
						},
					},
					_owner: null,
				},
			},
			keywords: ["recent comments"],
			attributes: {
				commentsToShow: {
					type: "number",
					default: 5,
					minimum: 1,
					maximum: 100,
				},
				displayAvatar: {
					type: "boolean",
					default: true,
				},
				displayDate: {
					type: "boolean",
					default: true,
				},
				displayExcerpt: {
					type: "boolean",
					default: true,
				},
				align: {
					type: "string",
					enum: ["left", "center", "right", "wide", "full", ""],
				},
				className: {
					type: "string",
				},
			},
			providesContext: {},
			usesContext: [],
			supports: {
				align: true,
				html: false,
			},
			styles: [],
			apiVersion: 2,
			category: "widgets",
			editorStyle: "wp-block-latest-comments-editor",
			style: "wp-block-latest-comments",
			title: "Latest Comments",
			description: "Display a list of your most recent comments.",
			example: {},
			variations: [],
		},
		{
			name: "core/latest-posts",
			icon: {
				src: {
					key: null,
					ref: null,
					props: {
						viewBox: "0 0 24 24",
						xmlns: "http://www.w3.org/2000/svg",
						children: {
							key: null,
							ref: null,
							props: {
								d: "M18 4H6c-1.1 0-2 .9-2 2v12c0 1.1.9 2 2 2h12c1.1 0 2-.9 2-2V6c0-1.1-.9-2-2-2zm.5 14c0 .3-.2.5-.5.5H6c-.3 0-.5-.2-.5-.5V6c0-.3.2-.5.5-.5h12c.3 0 .5.2.5.5v12zM7 11h2V9H7v2zm0 4h2v-2H7v2zm3-4h7V9h-7v2zm0 4h7v-2h-7v2z",
							},
							_owner: null,
						},
					},
					_owner: null,
				},
			},
			keywords: ["recent posts"],
			attributes: {
				categories: {
					type: "array",
					items: {
						type: "object",
					},
				},
				selectedAuthor: {
					type: "number",
				},
				postsToShow: {
					type: "number",
					default: 5,
				},
				displayPostContent: {
					type: "boolean",
					default: false,
				},
				displayPostContentRadio: {
					type: "string",
					default: "excerpt",
				},
				excerptLength: {
					type: "number",
					default: 55,
				},
				displayAuthor: {
					type: "boolean",
					default: false,
				},
				displayPostDate: {
					type: "boolean",
					default: false,
				},
				postLayout: {
					type: "string",
					default: "list",
				},
				columns: {
					type: "number",
					default: 3,
				},
				order: {
					type: "string",
					default: "desc",
				},
				orderBy: {
					type: "string",
					default: "date",
				},
				displayFeaturedImage: {
					type: "boolean",
					default: false,
				},
				featuredImageAlign: {
					type: "string",
					enum: ["left", "center", "right"],
				},
				featuredImageSizeSlug: {
					type: "string",
					default: "thumbnail",
				},
				featuredImageSizeWidth: {
					type: "number",
					default: null,
				},
				featuredImageSizeHeight: {
					type: "number",
					default: null,
				},
				addLinkToFeaturedImage: {
					type: "boolean",
					default: false,
				},
				align: {
					type: "string",
					enum: ["left", "center", "right", "wide", "full", ""],
				},
				className: {
					type: "string",
				},
			},
			providesContext: {},
			usesContext: [],
			supports: {
				align: true,
				html: false,
			},
			styles: [],
			apiVersion: 2,
			category: "widgets",
			editorStyle: "wp-block-latest-posts-editor",
			style: "wp-block-latest-posts",
			title: "Latest Posts",
			description: "Display a list of your most recent posts.",
			example: {},
			deprecated: [
				{
					attributes: {
						categories: {
							type: "string",
						},
						selectedAuthor: {
							type: "number",
						},
						postsToShow: {
							type: "number",
							default: 5,
						},
						displayPostContent: {
							type: "boolean",
							default: false,
						},
						displayPostContentRadio: {
							type: "string",
							default: "excerpt",
						},
						excerptLength: {
							type: "number",
							default: 55,
						},
						displayAuthor: {
							type: "boolean",
							default: false,
						},
						displayPostDate: {
							type: "boolean",
							default: false,
						},
						postLayout: {
							type: "string",
							default: "list",
						},
						columns: {
							type: "number",
							default: 3,
						},
						order: {
							type: "string",
							default: "desc",
						},
						orderBy: {
							type: "string",
							default: "date",
						},
						displayFeaturedImage: {
							type: "boolean",
							default: false,
						},
						featuredImageAlign: {
							type: "string",
							enum: ["left", "center", "right"],
						},
						featuredImageSizeSlug: {
							type: "string",
							default: "thumbnail",
						},
						featuredImageSizeWidth: {
							type: "number",
							default: null,
						},
						featuredImageSizeHeight: {
							type: "number",
							default: null,
						},
						addLinkToFeaturedImage: {
							type: "boolean",
							default: false,
						},
						align: {
							type: "string",
							enum: ["left", "center", "right", "wide", "full", ""],
						},
						className: {
							type: "string",
						},
					},
					supports: {
						align: true,
						html: false,
					},
				},
			],
			variations: [],
		},
		{
			name: "core/missing",
			icon: {
				src: {
					key: null,
					ref: null,
					props: {
						xmlns: "http://www.w3.org/2000/svg",
						viewBox: "0 0 24 24",
						children: {
							key: null,
							ref: null,
							props: {
								d: "M19 8h-1V6h-5v2h-2V6H6v2H5c-1.1 0-2 .9-2 2v8c0 1.1.9 2 2 2h14c1.1 0 2-.9 2-2v-8c0-1.1-.9-2-2-2zm.5 10c0 .3-.2.5-.5.5H5c-.3 0-.5-.2-.5-.5v-8c0-.3.2-.5.5-.5h14c.3 0 .5.2.5.5v8z",
							},
							_owner: null,
						},
					},
					_owner: null,
				},
			},
			keywords: [],
			attributes: {
				originalName: {
					type: "string",
				},
				originalUndelimitedContent: {
					type: "string",
				},
				originalContent: {
					type: "string",
					source: "html",
				},
			},
			providesContext: {},
			usesContext: [],
			supports: {
				className: false,
				customClassName: false,
				inserter: false,
				html: false,
				reusable: false,
			},
			styles: [],
			apiVersion: 2,
			category: "text",
			title: "Unsupported",
			description: "Your site doesn’t include support for this block.",
			variations: [],
		},
		{
			name: "core/more",
			icon: {
				src: {
					key: null,
					ref: null,
					props: {
						viewBox: "0 0 24 24",
						xmlns: "http://www.w3.org/2000/svg",
						children: {
							key: null,
							ref: null,
							props: {
								d: "M4 9v1.5h16V9H4zm12 5.5h4V13h-4v1.5zm-6 0h4V13h-4v1.5zm-6 0h4V13H4v1.5z",
							},
							_owner: null,
						},
					},
					_owner: null,
				},
			},
			keywords: ["read more"],
			attributes: {
				customText: {
					type: "string",
				},
				noTeaser: {
					type: "boolean",
					default: false,
				},
			},
			providesContext: {},
			usesContext: [],
			supports: {
				customClassName: false,
				className: false,
				html: false,
				multiple: false,
			},
			styles: [],
			apiVersion: 2,
			category: "design",
			editorStyle: "wp-block-more-editor",
			title: "More",
			description:
				"Content before this block will be shown in the excerpt on your archives page.",
			example: {},
			transforms: {
				from: [
					{
						type: "raw",
						schema: {
							"wp-block": {
								attributes: ["data-block"],
							},
						},
					},
				],
			},
			variations: [],
		},
		{
			name: "core/nextpage",
			icon: {
				src: {
					key: null,
					ref: null,
					props: {
						xmlns: "http://www.w3.org/2000/svg",
						viewBox: "0 0 24 24",
						children: {
							key: null,
							ref: null,
							props: {
								d: "M7.8 6c0-.7.6-1.2 1.2-1.2h6c.7 0 1.2.6 1.2 1.2v3h1.5V6c0-1.5-1.2-2.8-2.8-2.8H9C7.5 3.2 6.2 4.5 6.2 6v3h1.5V6zm8.4 11c0 .7-.6 1.2-1.2 1.2H9c-.7 0-1.2-.6-1.2-1.2v-3H6.2v3c0 1.5 1.2 2.8 2.8 2.8h6c1.5 0 2.8-1.2 2.8-2.8v-3h-1.5v3zM4 11v1h16v-1H4z",
							},
							_owner: null,
						},
					},
					_owner: null,
				},
			},
			keywords: ["next page", "pagination"],
			attributes: {},
			providesContext: {},
			usesContext: [],
			supports: {
				customClassName: false,
				className: false,
				html: false,
			},
			styles: [],
			apiVersion: 2,
			category: "design",
			parent: ["core/post-content"],
			editorStyle: "wp-block-nextpage-editor",
			title: "Page Break",
			description: "Separate your content into a multi-page experience.",
			example: {},
			transforms: {
				from: [
					{
						type: "raw",
						schema: {
							"wp-block": {
								attributes: ["data-block"],
							},
						},
					},
				],
			},
			variations: [],
		},
		{
			name: "core/preformatted",
			icon: {
				src: {
					key: null,
					ref: null,
					props: {
						viewBox: "0 0 24 24",
						xmlns: "http://www.w3.org/2000/svg",
						children: {
							key: null,
							ref: null,
							props: {
								d: "M18 4H6c-1.1 0-2 .9-2 2v12c0 1.1.9 2 2 2h12c1.1 0 2-.9 2-2V6c0-1.1-.9-2-2-2zm.5 14c0 .3-.2.5-.5.5H6c-.3 0-.5-.2-.5-.5V6c0-.3.2-.5.5-.5h12c.3 0 .5.2.5.5v12zM7 16.5h6V15H7v1.5zm4-4h6V11h-6v1.5zM9 11H7v1.5h2V11zm6 5.5h2V15h-2v1.5z",
							},
							_owner: null,
						},
					},
					_owner: null,
				},
			},
			keywords: [],
			attributes: {
				content: {
					type: "string",
					source: "html",
					selector: "pre",
					default: "",
					__unstablePreserveWhiteSpace: true,
				},
				anchor: {
					type: "string",
					source: "attribute",
					attribute: "id",
					selector: "*",
				},
				className: {
					type: "string",
				},
				fontSize: {
					type: "string",
				},
				style: {
					type: "object",
				},
			},
			providesContext: {},
			usesContext: [],
			supports: {
				anchor: true,
				fontSize: true,
			},
			styles: [],
			apiVersion: 2,
			category: "text",
			style: "wp-block-preformatted",
			title: "Preformatted",
			description:
				"Add text that respects your spacing and tabs, and also allows styling.",
			example: {
				attributes: {
					content:
						"EXT. XANADU - FAINT DAWN - 1940 (MINIATURE)\nWindow, very small in the distance, illuminated.\nAll around this is an almost totally black screen. Now, as the camera moves slowly towards the window which is almost a postage stamp in the frame, other forms appear;",
				},
			},
			transforms: {
				from: [
					{
						type: "block",
						blocks: ["core/code", "core/paragraph"],
					},
					{
						type: "raw",
					},
				],
				to: [
					{
						type: "block",
						blocks: ["core/paragraph"],
					},
					{
						type: "block",
						blocks: ["core/code"],
					},
				],
			},
			variations: [],
		},
		{
			name: "core/pullquote",
			icon: {
				src: {
					key: null,
					ref: null,
					props: {
						viewBox: "0 0 24 24",
						xmlns: "http://www.w3.org/2000/svg",
						children: {
							key: null,
							ref: null,
							props: {
								d: "M18 8H6c-1.1 0-2 .9-2 2v4c0 1.1.9 2 2 2h12c1.1 0 2-.9 2-2v-4c0-1.1-.9-2-2-2zm.5 6c0 .3-.2.5-.5.5H6c-.3 0-.5-.2-.5-.5v-4c0-.3.2-.5.5-.5h12c.3 0 .5.2.5.5v4zM4 4v1.5h16V4H4zm0 16h16v-1.5H4V20z",
							},
							_owner: null,
						},
					},
					_owner: null,
				},
			},
			keywords: [],
			attributes: {
				value: {
					type: "string",
					source: "html",
					selector: "blockquote",
					multiline: "p",
				},
				citation: {
					type: "string",
					source: "html",
					selector: "cite",
					default: "",
				},
				mainColor: {
					type: "string",
				},
				customMainColor: {
					type: "string",
				},
				textColor: {
					type: "string",
				},
				customTextColor: {
					type: "string",
				},
				align: {
					type: "string",
					enum: ["left", "center", "right", "wide", "full", ""],
				},
				anchor: {
					type: "string",
					source: "attribute",
					attribute: "id",
					selector: "*",
				},
				className: {
					type: "string",
				},
			},
			providesContext: {},
			usesContext: [],
			supports: {
				anchor: true,
				align: ["left", "right", "wide", "full"],
			},
			styles: [
				{
					name: "default",
					label: "Default",
					isDefault: true,
				},
				{
					name: "solid-color",
					label: "Solid color",
				},
			],
			apiVersion: 2,
			category: "text",
			editorStyle: "wp-block-pullquote-editor",
			style: "wp-block-pullquote",
			title: "Pullquote",
			description: "Give special visual emphasis to a quote from your text.",
			example: {
				attributes: {
					value:
						"<p>One of the hardest things to do in technology is disrupt yourself.</p>",
					citation: "Matt Mullenweg",
				},
			},
			transforms: {
				from: [
					{
						type: "block",
						isMultiBlock: true,
						blocks: ["core/paragraph"],
					},
					{
						type: "block",
						blocks: ["core/heading"],
					},
				],
				to: [
					{
						type: "block",
						blocks: ["core/paragraph"],
					},
					{
						type: "block",
						blocks: ["core/heading"],
					},
				],
			},
			deprecated: [
				{
					attributes: {
						value: {
							type: "string",
							source: "html",
							selector: "blockquote",
							multiline: "p",
						},
						citation: {
							type: "string",
							source: "html",
							selector: "cite",
							default: "",
						},
						mainColor: {
							type: "string",
						},
						customMainColor: {
							type: "string",
						},
						textColor: {
							type: "string",
						},
						customTextColor: {
							type: "string",
						},
						figureStyle: {
							source: "attribute",
							selector: "figure",
							attribute: "style",
						},
						className: {
							type: "string",
						},
					},
				},
				{
					attributes: {
						value: {
							type: "string",
							source: "html",
							selector: "blockquote",
							multiline: "p",
						},
						citation: {
							type: "string",
							source: "html",
							selector: "cite",
							default: "",
						},
						mainColor: {
							type: "string",
						},
						customMainColor: {
							type: "string",
						},
						textColor: {
							type: "string",
						},
						customTextColor: {
							type: "string",
						},
						className: {
							type: "string",
						},
					},
				},
				{
					attributes: {
						value: {
							type: "string",
							source: "html",
							selector: "blockquote",
							multiline: "p",
						},
						citation: {
							type: "string",
							source: "html",
							selector: "cite",
							default: "",
						},
						mainColor: {
							type: "string",
						},
						customMainColor: {
							type: "string",
						},
						textColor: {
							type: "string",
						},
						customTextColor: {
							type: "string",
						},
						className: {
							type: "string",
						},
					},
				},
				{
					attributes: {
						value: {
							type: "string",
							source: "html",
							selector: "blockquote",
							multiline: "p",
						},
						citation: {
							type: "string",
							source: "html",
							selector: "footer",
						},
						mainColor: {
							type: "string",
						},
						customMainColor: {
							type: "string",
						},
						textColor: {
							type: "string",
						},
						customTextColor: {
							type: "string",
						},
						align: {
							type: "string",
							default: "none",
						},
						className: {
							type: "string",
						},
					},
				},
			],
			variations: [],
		},
		{
			name: "core/rss",
			icon: {
				src: {
					key: null,
					ref: null,
					props: {
						xmlns: "http://www.w3.org/2000/svg",
						viewBox: "0 0 24 24",
						children: {
							key: null,
							ref: null,
							props: {
								d: "M5 10.2h-.8v1.5H5c1.9 0 3.8.8 5.1 2.1 1.4 1.4 2.1 3.2 2.1 5.1v.8h1.5V19c0-2.3-.9-4.5-2.6-6.2-1.6-1.6-3.8-2.6-6.1-2.6zm10.4-1.6C12.6 5.8 8.9 4.2 5 4.2h-.8v1.5H5c3.5 0 6.9 1.4 9.4 3.9s3.9 5.8 3.9 9.4v.8h1.5V19c0-3.9-1.6-7.6-4.4-10.4zM4 20h3v-3H4v3z",
							},
							_owner: null,
						},
					},
					_owner: null,
				},
			},
			keywords: ["atom", "feed"],
			attributes: {
				columns: {
					type: "number",
					default: 2,
				},
				blockLayout: {
					type: "string",
					default: "list",
				},
				feedURL: {
					type: "string",
					default: "",
				},
				itemsToShow: {
					type: "number",
					default: 5,
				},
				displayExcerpt: {
					type: "boolean",
					default: false,
				},
				displayAuthor: {
					type: "boolean",
					default: false,
				},
				displayDate: {
					type: "boolean",
					default: false,
				},
				excerptLength: {
					type: "number",
					default: 55,
				},
				align: {
					type: "string",
					enum: ["left", "center", "right", "wide", "full", ""],
				},
				className: {
					type: "string",
				},
			},
			providesContext: {},
			usesContext: [],
			supports: {
				align: true,
				html: false,
			},
			styles: [],
			apiVersion: 2,
			category: "widgets",
			editorStyle: "wp-block-rss-editor",
			style: "wp-block-rss",
			title: "RSS",
			description: "Display entries from any RSS or Atom feed.",
			example: {
				attributes: {
					feedURL: "https://wordpress.org",
				},
			},
			variations: [],
		},
		{
			name: "core/search",
			icon: {
				src: {
					key: null,
					ref: null,
					props: {
						xmlns: "http://www.w3.org/2000/svg",
						viewBox: "0 0 24 24",
						children: {
							key: null,
							ref: null,
							props: {
								d: "M13.5 6C10.5 6 8 8.5 8 11.5c0 1.1.3 2.1.9 3l-3.4 3 1 1.1 3.4-2.9c1 .9 2.2 1.4 3.6 1.4 3 0 5.5-2.5 5.5-5.5C19 8.5 16.5 6 13.5 6zm0 9.5c-2.2 0-4-1.8-4-4s1.8-4 4-4 4 1.8 4 4-1.8 4-4 4z",
							},
							_owner: null,
						},
					},
					_owner: null,
				},
			},
			keywords: ["find"],
			attributes: {
				label: {
					type: "string",
				},
				showLabel: {
					type: "boolean",
					default: true,
				},
				placeholder: {
					type: "string",
					default: "",
				},
				width: {
					type: "number",
				},
				widthUnit: {
					type: "string",
				},
				buttonText: {
					type: "string",
				},
				buttonPosition: {
					type: "string",
					default: "button-outside",
				},
				buttonUseIcon: {
					type: "boolean",
					default: false,
				},
				align: {
					type: "string",
					enum: ["left", "center", "right", "wide", "full", ""],
				},
				className: {
					type: "string",
				},
			},
			providesContext: {},
			usesContext: [],
			supports: {
				align: ["left", "center", "right"],
				html: false,
			},
			styles: [],
			apiVersion: 2,
			category: "widgets",
			editorStyle: "wp-block-search-editor",
			style: "wp-block-search",
			title: "Search",
			description: "Help visitors find your content.",
			example: {},
			variations: [
				{
					name: "default",
					isDefault: true,
					attributes: {
						buttonText: "Search",
						label: "Search",
					},
				},
			],
		},
		{
			name: "core/separator",
			icon: {
				src: {
					key: null,
					ref: null,
					props: {
						viewBox: "0 0 24 24",
						xmlns: "http://www.w3.org/2000/svg",
						children: {
							key: null,
							ref: null,
							props: {
								d: "M20.2 7v4H3.8V7H2.2v9h1.6v-3.5h16.4V16h1.6V7z",
							},
							_owner: null,
						},
					},
					_owner: null,
				},
			},
			keywords: ["horizontal-line", "hr", "divider"],
			attributes: {
				color: {
					type: "string",
				},
				customColor: {
					type: "string",
				},
				align: {
					type: "string",
					enum: ["left", "center", "right", "wide", "full", ""],
				},
				anchor: {
					type: "string",
					source: "attribute",
					attribute: "id",
					selector: "*",
				},
				className: {
					type: "string",
				},
			},
			providesContext: {},
			usesContext: [],
			supports: {
				anchor: true,
				align: ["center", "wide", "full"],
			},
			styles: [
				{
					name: "default",
					label: "Default",
					isDefault: true,
				},
				{
					name: "wide",
					label: "Wide Line",
				},
				{
					name: "dots",
					label: "Dots",
				},
			],
			apiVersion: 2,
			category: "design",
			editorStyle: "wp-block-separator-editor",
			style: "wp-block-separator",
			title: "Separator",
			description:
				"Create a break between ideas or sections with a horizontal separator.",
			example: {
				attributes: {
					customColor: "#065174",
					className: "is-style-wide",
				},
			},
			transforms: {
				from: [
					{
						type: "enter",
						regExp: {},
					},
					{
						type: "raw",
						selector: "hr",
						schema: {
							hr: {},
						},
					},
				],
			},
			variations: [],
		},
		{
			name: "core/block",
			icon: {
				src: {
					key: null,
					ref: null,
					props: {
						xmlns: "http://www.w3.org/2000/svg",
						viewBox: "0 0 24 24",
						children: {
							key: null,
							ref: null,
							props: {
								d: "M19 8h-1V6h-5v2h-2V6H6v2H5c-1.1 0-2 .9-2 2v8c0 1.1.9 2 2 2h14c1.1 0 2-.9 2-2v-8c0-1.1-.9-2-2-2zm.5 10c0 .3-.2.5-.5.5H5c-.3 0-.5-.2-.5-.5v-8c0-.3.2-.5.5-.5h14c.3 0 .5.2.5.5v8z",
							},
							_owner: null,
						},
					},
					_owner: null,
				},
			},
			keywords: [],
			attributes: {
				ref: {
					type: "number",
				},
			},
			providesContext: {},
			usesContext: [],
			supports: {
				customClassName: false,
				html: false,
				inserter: false,
			},
			styles: [],
			apiVersion: 2,
			category: "reusable",
			editorStyle: "wp-block-editor",
			title: "Reusable Block",
			description:
				"Create and save content to reuse across your site. Update the block, and the changes apply everywhere it’s used.",
			variations: [],
		},
		{
			name: "core/social-links",
			icon: {
				src: {
					key: null,
					ref: null,
					props: {
						viewBox: "0 0 24 24",
						xmlns: "http://www.w3.org/2000/svg",
						children: {
							key: null,
							ref: null,
							props: {
								d: "M9 11.8l6.1-4.5c.1.4.4.7.9.7h2c.6 0 1-.4 1-1V5c0-.6-.4-1-1-1h-2c-.6 0-1 .4-1 1v.4l-6.4 4.8c-.2-.1-.4-.2-.6-.2H6c-.6 0-1 .4-1 1v2c0 .6.4 1 1 1h2c.2 0 .4-.1.6-.2l6.4 4.8v.4c0 .6.4 1 1 1h2c.6 0 1-.4 1-1v-2c0-.6-.4-1-1-1h-2c-.5 0-.8.3-.9.7L9 12.2v-.4z",
							},
							_owner: null,
						},
					},
					_owner: null,
				},
			},
			keywords: ["links"],
			attributes: {
				iconColor: {
					type: "string",
				},
				customIconColor: {
					type: "string",
				},
				iconColorValue: {
					type: "string",
				},
				iconBackgroundColor: {
					type: "string",
				},
				customIconBackgroundColor: {
					type: "string",
				},
				iconBackgroundColorValue: {
					type: "string",
				},
				openInNewTab: {
					type: "boolean",
					default: false,
				},
				size: {
					type: "string",
				},
				align: {
					type: "string",
					enum: ["left", "center", "right", "wide", "full", ""],
				},
				anchor: {
					type: "string",
					source: "attribute",
					attribute: "id",
					selector: "*",
				},
				className: {
					type: "string",
				},
			},
			providesContext: {
				openInNewTab: "openInNewTab",
				iconColorValue: "iconColorValue",
				iconBackgroundColorValue: "iconBackgroundColorValue",
			},
			usesContext: [],
			supports: {
				align: ["left", "center", "right"],
				anchor: true,
			},
			styles: [
				{
					name: "default",
					label: "Default",
					isDefault: true,
				},
				{
					name: "logos-only",
					label: "Logos Only",
				},
				{
					name: "pill-shape",
					label: "Pill Shape",
				},
			],
			apiVersion: 2,
			category: "widgets",
			editorStyle: "wp-block-social-links-editor",
			style: "wp-block-social-links",
			title: "Social Icons",
			description:
				"Display icons linking to your social media profiles or websites.",
			example: {
				innerBlocks: [
					{
						name: "core/social-link",
						attributes: {
							service: "wordpress",
							url: "https://wordpress.org",
						},
					},
					{
						name: "core/social-link",
						attributes: {
							service: "facebook",
							url: "https://www.facebook.com/WordPress/",
						},
					},
					{
						name: "core/social-link",
						attributes: {
							service: "twitter",
							url: "https://twitter.com/WordPress",
						},
					},
				],
			},
			deprecated: [
				{
					attributes: {
						iconColor: {
							type: "string",
						},
						customIconColor: {
							type: "string",
						},
						iconColorValue: {
							type: "string",
						},
						iconBackgroundColor: {
							type: "string",
						},
						customIconBackgroundColor: {
							type: "string",
						},
						iconBackgroundColorValue: {
							type: "string",
						},
						openInNewTab: {
							type: "boolean",
							default: false,
						},
						size: {
							type: "string",
						},
						align: {
							type: "string",
							enum: ["left", "center", "right", "wide", "full", ""],
						},
						anchor: {
							type: "string",
							source: "attribute",
							attribute: "id",
							selector: "*",
						},
						className: {
							type: "string",
						},
					},
					supports: {
						align: ["left", "center", "right"],
						anchor: true,
					},
				},
			],
			variations: [],
		},
		{
			name: "core/social-link",
			icon: {
				src: {
					key: null,
					ref: null,
					props: {
						viewBox: "0 0 24 24",
						xmlns: "http://www.w3.org/2000/svg",
						children: {
							key: null,
							ref: null,
							props: {
								d: "M9 11.8l6.1-4.5c.1.4.4.7.9.7h2c.6 0 1-.4 1-1V5c0-.6-.4-1-1-1h-2c-.6 0-1 .4-1 1v.4l-6.4 4.8c-.2-.1-.4-.2-.6-.2H6c-.6 0-1 .4-1 1v2c0 .6.4 1 1 1h2c.2 0 .4-.1.6-.2l6.4 4.8v.4c0 .6.4 1 1 1h2c.6 0 1-.4 1-1v-2c0-.6-.4-1-1-1h-2c-.5 0-.8.3-.9.7L9 12.2v-.4z",
							},
							_owner: null,
						},
					},
					_owner: null,
				},
			},
			keywords: [],
			attributes: {
				url: {
					type: "string",
				},
				service: {
					type: "string",
				},
				label: {
					type: "string",
				},
				className: {
					type: "string",
				},
			},
			providesContext: {},
			usesContext: [
				"openInNewTab",
				"iconColorValue",
				"iconBackgroundColorValue",
			],
			supports: {
				reusable: false,
				html: false,
			},
			styles: [],
			apiVersion: 2,
			category: "widgets",
			parent: ["core/social-links"],
			editorStyle: "wp-block-social-link-editor",
			title: "Social Icon",
			description:
				"Display an icon linking to a social media profile or website.",
			variations: [
				{
					isDefault: true,
					name: "wordpress",
					attributes: {
						service: "wordpress",
					},
					title: "WordPress",
				},
				{
					name: "fivehundredpx",
					attributes: {
						service: "fivehundredpx",
					},
					title: "500px",
				},
				{
					name: "amazon",
					attributes: {
						service: "amazon",
					},
					title: "Amazon",
				},
				{
					name: "bandcamp",
					attributes: {
						service: "bandcamp",
					},
					title: "Bandcamp",
				},
				{
					name: "behance",
					attributes: {
						service: "behance",
					},
					title: "Behance",
				},
				{
					name: "chain",
					attributes: {
						service: "chain",
					},
					title: "Link",
				},
				{
					name: "codepen",
					attributes: {
						service: "codepen",
					},
					title: "CodePen",
				},
				{
					name: "deviantart",
					attributes: {
						service: "deviantart",
					},
					title: "DeviantArt",
				},
				{
					name: "dribbble",
					attributes: {
						service: "dribbble",
					},
					title: "Dribbble",
				},
				{
					name: "dropbox",
					attributes: {
						service: "dropbox",
					},
					title: "Dropbox",
				},
				{
					name: "etsy",
					attributes: {
						service: "etsy",
					},
					title: "Etsy",
				},
				{
					name: "facebook",
					attributes: {
						service: "facebook",
					},
					title: "Facebook",
				},
				{
					name: "feed",
					attributes: {
						service: "feed",
					},
					title: "RSS Feed",
				},
				{
					name: "flickr",
					attributes: {
						service: "flickr",
					},
					title: "Flickr",
				},
				{
					name: "foursquare",
					attributes: {
						service: "foursquare",
					},
					title: "Foursquare",
				},
				{
					name: "goodreads",
					attributes: {
						service: "goodreads",
					},
					title: "Goodreads",
				},
				{
					name: "google",
					attributes: {
						service: "google",
					},
					title: "Google",
				},
				{
					name: "github",
					attributes: {
						service: "github",
					},
					title: "GitHub",
				},
				{
					name: "instagram",
					attributes: {
						service: "instagram",
					},
					title: "Instagram",
				},
				{
					name: "lastfm",
					attributes: {
						service: "lastfm",
					},
					title: "Last.fm",
				},
				{
					name: "linkedin",
					attributes: {
						service: "linkedin",
					},
					title: "LinkedIn",
				},
				{
					name: "mail",
					attributes: {
						service: "mail",
					},
					title: "Mail",
					keywords: ["email", "e-mail"],
				},
				{
					name: "mastodon",
					attributes: {
						service: "mastodon",
					},
					title: "Mastodon",
				},
				{
					name: "meetup",
					attributes: {
						service: "meetup",
					},
					title: "Meetup",
				},
				{
					name: "medium",
					attributes: {
						service: "medium",
					},
					title: "Medium",
				},
				{
					name: "patreon",
					attributes: {
						service: "patreon",
					},
					title: "Patreon",
				},
				{
					name: "pinterest",
					attributes: {
						service: "pinterest",
					},
					title: "Pinterest",
				},
				{
					name: "pocket",
					attributes: {
						service: "pocket",
					},
					title: "Pocket",
				},
				{
					name: "reddit",
					attributes: {
						service: "reddit",
					},
					title: "Reddit",
				},
				{
					name: "skype",
					attributes: {
						service: "skype",
					},
					title: "Skype",
				},
				{
					name: "snapchat",
					attributes: {
						service: "snapchat",
					},
					title: "Snapchat",
				},
				{
					name: "soundcloud",
					attributes: {
						service: "soundcloud",
					},
					title: "SoundCloud",
				},
				{
					name: "spotify",
					attributes: {
						service: "spotify",
					},
					title: "Spotify",
				},
				{
					name: "telegram",
					attributes: {
						service: "telegram",
					},
					title: "Telegram",
				},
				{
					name: "tiktok",
					attributes: {
						service: "tiktok",
					},
					title: "TikTok",
				},
				{
					name: "tumblr",
					attributes: {
						service: "tumblr",
					},
					title: "Tumblr",
				},
				{
					name: "twitch",
					attributes: {
						service: "twitch",
					},
					title: "Twitch",
				},
				{
					name: "twitter",
					attributes: {
						service: "twitter",
					},
					title: "Twitter",
				},
				{
					name: "vimeo",
					attributes: {
						service: "vimeo",
					},
					title: "Vimeo",
				},
				{
					name: "vk",
					attributes: {
						service: "vk",
					},
					title: "VK",
				},
				{
					name: "yelp",
					attributes: {
						service: "yelp",
					},
					title: "Yelp",
				},
				{
					name: "youtube",
					attributes: {
						service: "youtube",
					},
					title: "YouTube",
				},
			],
		},
		{
			name: "core/spacer",
			icon: {
				src: {
					key: null,
					ref: null,
					props: {
						viewBox: "0 0 24 24",
						xmlns: "http://www.w3.org/2000/svg",
						children: {
							key: null,
							ref: null,
							props: {
								d: "M12.5 4.2v1.6h4.7L5.8 17.2V12H4.2v7.8H12v-1.6H6.8L18.2 6.8v4.7h1.6V4.2z",
							},
							_owner: null,
						},
					},
					_owner: null,
				},
			},
			keywords: [],
			attributes: {
				height: {
					type: "number",
					default: 100,
				},
				anchor: {
					type: "string",
					source: "attribute",
					attribute: "id",
					selector: "*",
				},
				className: {
					type: "string",
				},
			},
			providesContext: {},
			usesContext: [],
			supports: {
				anchor: true,
			},
			styles: [],
			apiVersion: 2,
			category: "design",
			editorStyle: "wp-block-spacer-editor",
			style: "wp-block-spacer",
			title: "Spacer",
			description: "Add white space between blocks and customize its height.",
			variations: [],
		},
		{
			name: "core/subhead",
			icon: {
				src: {
					key: null,
					ref: null,
					props: {
						xmlns: "http://www.w3.org/2000/svg",
						viewBox: "0 0 24 24",
						children: {
							key: null,
							ref: null,
							props: {
								d: "M7.1 6l-.5 3h4.5L9.4 19h3l1.8-10h4.5l.5-3H7.1z",
							},
							_owner: null,
						},
					},
					_owner: null,
				},
			},
			keywords: [],
			attributes: {
				align: {
					type: "string",
				},
				content: {
					type: "string",
					source: "html",
					selector: "p",
				},
				className: {
					type: "string",
				},
			},
			providesContext: {},
			usesContext: [],
			supports: {
				inserter: false,
				multiple: false,
			},
			styles: [],
			apiVersion: 2,
			category: "text",
			editorStyle: "wp-block-subhead-editor",
			style: "wp-block-subhead",
			title: "Subheading (deprecated)",
			description:
				"This block is deprecated. Please use the Paragraph block instead.",
			transforms: {
				to: [
					{
						type: "block",
						blocks: ["core/paragraph"],
					},
				],
			},
			variations: [],
		},
		{
			name: "core/table",
			icon: {
				src: {
					key: null,
					ref: null,
					props: {
						viewBox: "0 0 24 24",
						xmlns: "http://www.w3.org/2000/svg",
						children: {
							key: null,
							ref: null,
							props: {
								d: "M19 3H5c-1.1 0-2 .9-2 2v14c0 1.1.9 2 2 2h14c1.1 0 2-.9 2-2V5c0-1.1-.9-2-2-2zM5 4.5h14c.3 0 .5.2.5.5v3.5h-15V5c0-.3.2-.5.5-.5zm8 5.5h6.5v3.5H13V10zm-1.5 3.5h-7V10h7v3.5zm-7 5.5v-4h7v4.5H5c-.3 0-.5-.2-.5-.5zm14.5.5h-6V15h6.5v4c0 .3-.2.5-.5.5z",
							},
							_owner: null,
						},
					},
					_owner: null,
				},
			},
			keywords: [],
			attributes: {
				hasFixedLayout: {
					type: "boolean",
					default: false,
				},
				backgroundColor: {
					type: "string",
				},
				caption: {
					type: "string",
					source: "html",
					selector: "figcaption",
					default: "",
				},
				head: {
					type: "array",
					default: [],
					source: "query",
					selector: "thead tr",
					query: {
						cells: {
							type: "array",
							default: [],
							source: "query",
							selector: "td,th",
							query: {
								content: {
									type: "string",
									source: "html",
								},
								tag: {
									type: "string",
									default: "td",
									source: "tag",
								},
								scope: {
									type: "string",
									source: "attribute",
									attribute: "scope",
								},
								align: {
									type: "string",
									source: "attribute",
									attribute: "data-align",
								},
							},
						},
					},
				},
				body: {
					type: "array",
					default: [],
					source: "query",
					selector: "tbody tr",
					query: {
						cells: {
							type: "array",
							default: [],
							source: "query",
							selector: "td,th",
							query: {
								content: {
									type: "string",
									source: "html",
								},
								tag: {
									type: "string",
									default: "td",
									source: "tag",
								},
								scope: {
									type: "string",
									source: "attribute",
									attribute: "scope",
								},
								align: {
									type: "string",
									source: "attribute",
									attribute: "data-align",
								},
							},
						},
					},
				},
				foot: {
					type: "array",
					default: [],
					source: "query",
					selector: "tfoot tr",
					query: {
						cells: {
							type: "array",
							default: [],
							source: "query",
							selector: "td,th",
							query: {
								content: {
									type: "string",
									source: "html",
								},
								tag: {
									type: "string",
									default: "td",
									source: "tag",
								},
								scope: {
									type: "string",
									source: "attribute",
									attribute: "scope",
								},
								align: {
									type: "string",
									source: "attribute",
									attribute: "data-align",
								},
							},
						},
					},
				},
				align: {
					type: "string",
					enum: ["left", "center", "right", "wide", "full", ""],
				},
				anchor: {
					type: "string",
					source: "attribute",
					attribute: "id",
					selector: "*",
				},
				className: {
					type: "string",
				},
			},
			providesContext: {},
			usesContext: [],
			supports: {
				anchor: true,
				align: true,
				__experimentalSelector: ".wp-block-table > table",
			},
			styles: [
				{
					name: "regular",
					label: "Default",
					isDefault: true,
				},
				{
					name: "stripes",
					label: "Stripes",
				},
			],
			apiVersion: 2,
			category: "text",
			editorStyle: "wp-block-table-editor",
			style: "wp-block-table",
			title: "Table",
			description: "Insert a table — perfect for sharing charts and data.",
			example: {
				attributes: {
					head: [
						{
							cells: [
								{
									content: "Version",
									tag: "th",
								},
								{
									content: "Jazz Musician",
									tag: "th",
								},
								{
									content: "Release Date",
									tag: "th",
								},
							],
						},
					],
					body: [
						{
							cells: [
								{
									content: "5.2",
									tag: "td",
								},
								{
									content: "Jaco Pastorius",
									tag: "td",
								},
								{
									content: "May 7, 2019",
									tag: "td",
								},
							],
						},
						{
							cells: [
								{
									content: "5.1",
									tag: "td",
								},
								{
									content: "Betty Carter",
									tag: "td",
								},
								{
									content: "February 21, 2019",
									tag: "td",
								},
							],
						},
						{
							cells: [
								{
									content: "5.0",
									tag: "td",
								},
								{
									content: "Bebo Valdés",
									tag: "td",
								},
								{
									content: "December 6, 2018",
									tag: "td",
								},
							],
						},
					],
				},
			},
			transforms: {
				from: [
					{
						type: "raw",
						selector: "table",
					},
				],
			},
			deprecated: [
				{
					attributes: {
						hasFixedLayout: {
							type: "boolean",
							default: false,
						},
						backgroundColor: {
							type: "string",
						},
						head: {
							type: "array",
							default: [],
							source: "query",
							selector: "thead tr",
							query: {
								cells: {
									type: "array",
									default: [],
									source: "query",
									selector: "td,th",
									query: {
										content: {
											type: "string",
											source: "html",
										},
										tag: {
											type: "string",
											default: "td",
											source: "tag",
										},
										scope: {
											type: "string",
											source: "attribute",
											attribute: "scope",
										},
									},
								},
							},
						},
						body: {
							type: "array",
							default: [],
							source: "query",
							selector: "tbody tr",
							query: {
								cells: {
									type: "array",
									default: [],
									source: "query",
									selector: "td,th",
									query: {
										content: {
											type: "string",
											source: "html",
										},
										tag: {
											type: "string",
											default: "td",
											source: "tag",
										},
										scope: {
											type: "string",
											source: "attribute",
											attribute: "scope",
										},
									},
								},
							},
						},
						foot: {
							type: "array",
							default: [],
							source: "query",
							selector: "tfoot tr",
							query: {
								cells: {
									type: "array",
									default: [],
									source: "query",
									selector: "td,th",
									query: {
										content: {
											type: "string",
											source: "html",
										},
										tag: {
											type: "string",
											default: "td",
											source: "tag",
										},
										scope: {
											type: "string",
											source: "attribute",
											attribute: "scope",
										},
									},
								},
							},
						},
						align: {
							type: "string",
							enum: ["left", "center", "right", "wide", "full", ""],
						},
						className: {
							type: "string",
						},
					},
					supports: {
						align: true,
					},
				},
			],
			variations: [],
		},
		{
			name: "core/tag-cloud",
			icon: {
				src: {
					key: null,
					ref: null,
					props: {
						xmlns: "http://www.w3.org/2000/svg",
						viewBox: "0 0 24 24",
						children: {
							key: null,
							ref: null,
							props: {
								d: "M20.1 11.2l-6.7-6.7c-.1-.1-.3-.2-.5-.2H5c-.4-.1-.8.3-.8.7v7.8c0 .2.1.4.2.5l6.7 6.7c.2.2.5.4.7.5s.6.2.9.2c.3 0 .6-.1.9-.2.3-.1.5-.3.8-.5l5.6-5.6c.4-.4.7-1 .7-1.6.1-.6-.2-1.2-.6-1.6zM19 13.4L13.4 19c-.1.1-.2.1-.3.2-.2.1-.4.1-.6 0-.1 0-.2-.1-.3-.2l-6.5-6.5V5.8h6.8l6.5 6.5c.2.2.2.4.2.6 0 .1 0 .3-.2.5zM9 8c-.6 0-1 .4-1 1s.4 1 1 1 1-.4 1-1-.4-1-1-1z",
							},
							_owner: null,
						},
					},
					_owner: null,
				},
			},
			keywords: [],
			attributes: {
				taxonomy: {
					type: "string",
					default: "post_tag",
				},
				showTagCounts: {
					type: "boolean",
					default: false,
				},
				align: {
					type: "string",
					enum: ["left", "center", "right", "wide", "full", ""],
				},
				className: {
					type: "string",
				},
			},
			providesContext: {},
			usesContext: [],
			supports: {
				html: false,
				align: true,
			},
			styles: [],
			apiVersion: 2,
			category: "widgets",
			editorStyle: "wp-block-tag-cloud-editor",
			title: "Tag Cloud",
			description: "A cloud of your most used tags.",
			example: {},
			variations: [],
		},
		{
			name: "core/text-columns",
			icon: {
				src: "columns",
			},
			keywords: [],
			attributes: {
				content: {
					type: "array",
					source: "query",
					selector: "p",
					query: {
						children: {
							type: "string",
							source: "html",
						},
					},
					default: [{}, {}],
				},
				columns: {
					type: "number",
					default: 2,
				},
				width: {
					type: "string",
				},
				className: {
					type: "string",
				},
			},
			providesContext: {},
			usesContext: [],
			supports: {
				inserter: false,
			},
			styles: [],
			apiVersion: 2,
			category: "design",
			editorStyle: "wp-block-text-columns-editor",
			style: "wp-block-text-columns",
			title: "Text Columns (deprecated)",
			description:
				"This block is deprecated. Please use the Columns block instead.",
			transforms: {
				to: [
					{
						type: "block",
						blocks: ["core/columns"],
					},
				],
			},
			variations: [],
		},
		{
			name: "core/verse",
			icon: {
				src: {
					key: null,
					ref: null,
					props: {
						viewBox: "0 0 24 24",
						xmlns: "http://www.w3.org/2000/svg",
						children: {
							key: null,
							ref: null,
							props: {
								d: "M17.8 2l-.9.3c-.1 0-3.6 1-5.2 2.1C10 5.5 9.3 6.5 8.9 7.1c-.6.9-1.7 4.7-1.7 6.3l-.9 2.3c-.2.4 0 .8.4 1 .1 0 .2.1.3.1.3 0 .6-.2.7-.5l.6-1.5c.3 0 .7-.1 1.2-.2.7-.1 1.4-.3 2.2-.5.8-.2 1.6-.5 2.4-.8.7-.3 1.4-.7 1.9-1.2s.8-1.2 1-1.9c.2-.7.3-1.6.4-2.4.1-.8.1-1.7.2-2.5 0-.8.1-1.5.2-2.1V2zm-1.9 5.6c-.1.8-.2 1.5-.3 2.1-.2.6-.4 1-.6 1.3-.3.3-.8.6-1.4.9-.7.3-1.4.5-2.2.8-.6.2-1.3.3-1.8.4L15 7.5c.3-.3.6-.7 1-1.1 0 .4 0 .8-.1 1.2zM6 20h8v-1.5H6V20z",
							},
							_owner: null,
						},
					},
					_owner: null,
				},
			},
			keywords: ["poetry", "poem"],
			attributes: {
				content: {
					type: "string",
					source: "html",
					selector: "pre",
					default: "",
					__unstablePreserveWhiteSpace: true,
				},
				textAlign: {
					type: "string",
				},
				anchor: {
					type: "string",
					source: "attribute",
					attribute: "id",
					selector: "*",
				},
				className: {
					type: "string",
				},
				fontSize: {
					type: "string",
				},
				style: {
					type: "object",
				},
			},
			providesContext: {},
			usesContext: [],
			supports: {
				anchor: true,
				__experimentalFontFamily: true,
				fontSize: true,
			},
			styles: [],
			apiVersion: 2,
			category: "text",
			style: "wp-block-verse",
			editorStyle: "wp-block-verse-editor",
			title: "Verse",
			description:
				"Insert poetry. Use special spacing formats. Or quote song lyrics.",
			example: {
				attributes: {
					content:
						"WHAT was he doing, the great god Pan,\n\tDown in the reeds by the river?\nSpreading ruin and scattering ban,\nSplashing and paddling with hoofs of a goat,\nAnd breaking the golden lilies afloat\n    With the dragon-fly on the river.",
				},
			},
			transforms: {
				from: [
					{
						type: "block",
						blocks: ["core/paragraph"],
					},
				],
				to: [
					{
						type: "block",
						blocks: ["core/paragraph"],
					},
				],
			},
			deprecated: [
				{
					attributes: {
						content: {
							type: "string",
							source: "html",
							selector: "pre",
							default: "",
						},
						textAlign: {
							type: "string",
						},
						className: {
							type: "string",
						},
					},
				},
			],
			variations: [],
		},
		{
			name: "core/video",
			icon: {
				src: {
					key: null,
					ref: null,
					props: {
						viewBox: "0 0 24 24",
						xmlns: "http://www.w3.org/2000/svg",
						children: {
							key: null,
							ref: null,
							props: {
								d: "M18.7 3H5.3C4 3 3 4 3 5.3v13.4C3 20 4 21 5.3 21h13.4c1.3 0 2.3-1 2.3-2.3V5.3C21 4 20 3 18.7 3zm.8 15.7c0 .4-.4.8-.8.8H5.3c-.4 0-.8-.4-.8-.8V5.3c0-.4.4-.8.8-.8h13.4c.4 0 .8.4.8.8v13.4zM10 15l5-3-5-3v6z",
							},
							_owner: null,
						},
					},
					_owner: null,
				},
			},
			keywords: ["movie"],
			attributes: {
				autoplay: {
					type: "boolean",
					source: "attribute",
					selector: "video",
					attribute: "autoplay",
				},
				caption: {
					type: "string",
					source: "html",
					selector: "figcaption",
				},
				controls: {
					type: "boolean",
					source: "attribute",
					selector: "video",
					attribute: "controls",
					default: true,
				},
				id: {
					type: "number",
				},
				loop: {
					type: "boolean",
					source: "attribute",
					selector: "video",
					attribute: "loop",
				},
				muted: {
					type: "boolean",
					source: "attribute",
					selector: "video",
					attribute: "muted",
				},
				poster: {
					type: "string",
					source: "attribute",
					selector: "video",
					attribute: "poster",
				},
				preload: {
					type: "string",
					source: "attribute",
					selector: "video",
					attribute: "preload",
					default: "metadata",
				},
				src: {
					type: "string",
					source: "attribute",
					selector: "video",
					attribute: "src",
				},
				playsInline: {
					type: "boolean",
					source: "attribute",
					selector: "video",
					attribute: "playsinline",
				},
				tracks: {
					type: "array",
					items: {
						type: "object",
					},
					default: [],
				},
				align: {
					type: "string",
					enum: ["left", "center", "right", "wide", "full", ""],
				},
				anchor: {
					type: "string",
					source: "attribute",
					attribute: "id",
					selector: "*",
				},
				className: {
					type: "string",
				},
			},
			providesContext: {},
			usesContext: [],
			supports: {
				anchor: true,
				align: true,
			},
			styles: [],
			apiVersion: 2,
			category: "media",
			editorStyle: "wp-block-video-editor",
			style: "wp-block-video",
			title: "Video",
			description: "Embed a video from your media library or upload a new one.",
			example: {
				attributes: {
					src: "https://upload.wikimedia.org/wikipedia/commons/c/ca/Wood_thrush_in_Central_Park_switch_sides_%2816510%29.webm",
					caption: "Wood thrush singing in Central Park, NYC.",
				},
			},
			transforms: {
				from: [
					{
						type: "files",
					},
					{
						type: "shortcode",
						tag: "video",
						attributes: {
							src: {
								type: "string",
							},
							poster: {
								type: "string",
							},
							loop: {
								type: "string",
							},
							autoplay: {
								type: "string",
							},
							preload: {
								type: "string",
							},
						},
					},
				],
			},
			variations: [],
		},
	];
}
