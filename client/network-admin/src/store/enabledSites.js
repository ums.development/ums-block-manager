import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import axios from "../axios";

export const getEnabledSites = createAsyncThunk(
	"enabledSites/getEnabledSites",
	async () => {
		const response = await axios.get("ums-block-manager/enabled-sites");
		return response.data;
	}
);

export const addEnabledSite = createAsyncThunk(
	"enabledSites/add",
	async (payload, thunkAPI) => {
		const response = await axios.post("ums-block-manager/site", payload);
		thunkAPI.dispatch(getEnabledSites());
		return response.data;
	}
);

export const removeEnabledSite = createAsyncThunk(
	"enabledSites/remove",
	async (payload, thunkAPI) => {
		const response = await axios({
			method: "delete",
			url: "ums-block-manager/site",
			data: payload,
		});
		thunkAPI.dispatch(getEnabledSites());
		return response.data;
	}
);

export const enabledSitesSlice = createSlice({
	name: "enabledSites",
	initialState: {
		sites: [],
		isFetching: false,
	},
	reducers: {},
	extraReducers: (builder) => {
		builder.addCase(getEnabledSites.pending, (state) => {
			state.isFetching = true;
		});
		builder.addCase(getEnabledSites.fulfilled, (state, action) => {
			state.sites = action.payload;
			state.isFetching = false;
		});
		builder.addCase(getEnabledSites.rejected, (state) => {
			state.isFetching = null;
		});
		builder.addCase(addEnabledSite.pending, (state) => {
			state.isFetching = true;
		});
		builder.addCase(addEnabledSite.fulfilled, (state) => {
			state.isFetching = false;
			getEnabledSites();
		});
		builder.addCase(addEnabledSite.rejected, (state) => {
			state.isFetching = false;
			//TODO: alert user of error
		});
		builder.addCase(removeEnabledSite.pending, (state) => {
			state.isFetching = true;
		});
		builder.addCase(removeEnabledSite.fulfilled, (state) => {
			state.isFetching = false;
			getEnabledSites();
		});
		builder.addCase(removeEnabledSite.rejected, (state) => {
			state.isFetching = false;
			//TODO: alert user of error
		});
	},
});
export default enabledSitesSlice.reducer;
