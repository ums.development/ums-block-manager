import React from "react";
import BlockManager from "./components/BlockManager/index";
import SiteSelector from "./components/SiteSelector";
import ManualBlockToggle from "./components/ManualBlockToggle";

function App() {
	return (
		<React.Fragment>
			{
				<div>
					<SiteSelector />
					<BlockManager />
					<ManualBlockToggle />
				</div>
			}
		</React.Fragment>
	);
}

export default App;
