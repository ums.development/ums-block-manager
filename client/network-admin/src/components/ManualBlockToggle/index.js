import { Button, Card, Col, Container, Row, Form } from "react-bootstrap";
import CardHeader from "react-bootstrap/CardHeader";
import { useDispatch } from "react-redux";
import {
	toggleBlock,
	toggleExtraBlock,
	addUnregisteredBlock,
	removeUnregisteredBlock,
	addUnregisteredExtraBlock,
	removeUnregisteredExtraBlock,
} from "../../store/blocks";
import { useState } from "react";

export default function ManualBlockToggle() {
	const dispatch = useDispatch();
	const [slug, setSlug] = useState("");
	const [parent, setParent] = useState("");
	const [toggleState, setToggleState] = useState(null);
	const blockType = {
		name: slug,
		parent: parent.length === 0 ? null : parent,
	};

	return (
		<div>
			<Card className="p-0 m-4">
				<CardHeader>Manually Enable/Disable Block Types</CardHeader>
				<Card.Body>
					<Container>
						<Row>
							<Col>
								<Form onSubmit={(e) => e.preventDefault()}>
									<Form.Group controlId="txtBlockTypeSlug">
										<Form.Label>Block Type Slug</Form.Label>
										<Form.Control
											type="text"
											placeholder="core/button"
											value={slug}
											onChange={(e) => {
												setToggleState(null);
												setSlug(e.target.value);
											}}
										/>
									</Form.Group>
								</Form>
							</Col>
						</Row>
						<Row className="mt-2">
							<Col>
								<Form onSubmit={(e) => e.preventDefault()}>
									<Form.Group controlId="txtBlockTypeParent">
										<Form.Label>Block Type Parent</Form.Label>
										<Form.Control
											type="text"
											placeholder=""
											value={parent}
											onChange={(e) => {
												setToggleState(null);
												setParent(e.target.value);
											}}
										/>
									</Form.Group>
								</Form>
							</Col>
						</Row>
						<Row className="mt-4">
							<Col>
								<Button
									variant="primary"
									onClick={() => {
										setToggleState(null);

										dispatch(toggleBlock(blockType))
											.unwrap()
											.then((res) => {
												setToggleState(res.status);
												if (res.status) {
													dispatch(addUnregisteredBlock(blockType));
												} else {
													dispatch(removeUnregisteredBlock(blockType));
												}
											})
											.catch((error) => {
												//TODO: Report error to user.
												console.log(error);
												setToggleState(null);
											});
									}}
								>
									Toggle Global
								</Button>
							</Col>
							<Col>
								<Button
									variant="primary"
									onClick={() => {
										setToggleState(null);
										dispatch(toggleExtraBlock(blockType))
											.unwrap()
											.then((res) => {
												setToggleState(res.status);
												if (res.status) {
													dispatch(addUnregisteredExtraBlock(blockType));
												} else {
													dispatch(removeUnregisteredExtraBlock(blockType));
												}
											})
											.catch((error) => {
												//TODO: Report error to user.
												console.log(error);
												setToggleState(null);
											});
									}}
								>
									Toggle List
								</Button>
							</Col>
						</Row>
						<Row className="mt-4">
							<Col>
								{toggleState !== null
									? "Status of " +
									  (parent.length > 0 ? parent : "none") +
									  " ::: " +
									  slug +
									  " is now " +
									  (toggleState ? "enabled" : "disabled")
									: ""}
							</Col>
						</Row>
					</Container>
				</Card.Body>
			</Card>
		</div>
	);
}
