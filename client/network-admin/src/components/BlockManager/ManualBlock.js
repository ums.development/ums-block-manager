import React, { useState } from "react";
import Card from "react-bootstrap/Card";
import { useDispatch } from "react-redux";
import {
	toggleBlock,
	toggleExtraBlock,
	removeUnregisteredBlock,
	removeUnregisteredExtraBlock,
} from "../../store/blocks";
import FormCheck from "react-bootstrap/FormCheck";

export default function ManualBlock(props) {
	const dispatch = useDispatch();
	const [value, setValue] = useState(props.isEnabled);
	const [extraValue, setExtraValue] = useState(props.isExtraEnabled);
	const parent = props.parent === undefined ? null : props.parent;
	const slug = (parent ? parent.replace("/", "-") : "none") + props.name;
	const blockType = { name: props.name, parent };

	return (
		<div>
			<Card border={"success"} className={"m-2 p-0"} style={{ width: "18rem" }}>
				<Card.Header style={{ background: "azure" }}>{props.name}</Card.Header>
				<Card.Body>
					{parent === null && (
						<Card.Text className="fw-bold">{props.name}</Card.Text>
					)}
					{parent !== null && (
						<Card.Text className="fw-bold">
							{parent + " > " + props.name}
						</Card.Text>
					)}
					{value && (
						<p>
							<FormCheck
								type="switch"
								id={"chkEnableGlobally_" + slug}
								label="Enable Globally"
								checked={value}
								onChange={() => {
									dispatch(toggleBlock(blockType))
										.unwrap()
										.then((res) => {
											setValue(res.status);
											dispatch(removeUnregisteredBlock(blockType));
										})
										.catch((error) => {
											//TODO: Report error to user.
											console.log(error);
										});
								}}
							/>
						</p>
					)}
					{extraValue && (
						<p>
							<FormCheck
								type="switch"
								id={"chkEnableForList_" + slug}
								label="Enable For List"
								checked={extraValue}
								onChange={() => {
									dispatch(toggleExtraBlock(blockType))
										.unwrap()
										.then((res) => {
											setExtraValue(res.status);
											dispatch(removeUnregisteredExtraBlock(blockType));
										})
										.catch((error) => {
											//TODO: Report error to user.
											console.log(error);
										});
								}}
								disabled={value}
							/>
						</p>
					)}
				</Card.Body>
			</Card>
		</div>
	);
}
