import CardGroup from "react-bootstrap/CardGroup";
import Block from "./Block";
import React from "react";
import { useSelector } from "react-redux";
import Card from "react-bootstrap/Card";

export default function BlockCategory(props) {
	const blocks = useSelector((state) => state.blocks.blocks);
	const enabledBlocks = useSelector((state) => state.blocks.enabledBlocks);
	const enabledExtraBlocks = useSelector(
		(state) => state.blocks.enabledExtraBlocks
	);
	const categoryBlocks = blocks.filter(
		(block) => block.category === props.slug
	);

	let catBlocksWithVariants = [];
	categoryBlocks.forEach((cb) => {
		catBlocksWithVariants.push(cb);
		if (cb.variations) {
			cb.variations.forEach((variant) => {
				let v = {
					isVariant: true,
					parent: cb.name,
					...variant,
				};
				catBlocksWithVariants.push(v);
			});
		}
	});
	catBlocksWithVariants = catBlocksWithVariants.sort((a, b) => {
		if (a.title < b.title) {
			return -1;
		}
		if (a.title > b.title) {
			return 1;
		}

		return 0;
	});

	return (
		<Card
			className={"m-4 w-auto mw-100 p-0"}
			style={{ background: "whitesmoke" }}
		>
			<Card.Header>{props.name}</Card.Header>
			<Card.Body>
				<CardGroup>
					{catBlocksWithVariants.map((block) => (
						<Block
							key={block.name}
							name={block.name}
							title={block.title}
							description={block.description}
							isEnabled={
								enabledBlocks.findIndex((eb) => eb.name === block.name) !== -1
							}
							isExtraEnabled={
								enabledExtraBlocks.findIndex((eb) => eb.name === block.name) !==
								-1
							}
							icon={block.icon}
							parent={block.isVariant === true ? block.parent : null}
						/>
					))}
				</CardGroup>
			</Card.Body>
		</Card>
	);
}
