import CardGroup from "react-bootstrap/CardGroup";
import ManualBlock from "./ManualBlock";
import React from "react";
import { useSelector } from "react-redux";
import Card from "react-bootstrap/Card";

export default function ManualBlockCategory() {
	const blocks = useSelector((state) => state.blocks.unregisteredBlocks);
	const extraBlocks = useSelector(
		(state) => state.blocks.unregisteredExtraBlocks
	);

	return (
		<Card
			className={"m-4 w-auto mw-100 p-0"}
			style={{ background: "whitesmoke" }}
		>
			<Card.Header>Manually Enabled Block Types</Card.Header>
			<Card.Body>
				<CardGroup>
					{blocks.map((block) => (
						<ManualBlock
							key={block.name}
							name={block.name}
							parent={block.parent}
							isEnabled={true}
							isExtraEnabled={false}
						/>
					))}
					{extraBlocks.map((block) => (
						<ManualBlock
							key={block.name}
							name={block.name}
							parent={block.parent}
							isEnabled={false}
							isExtraEnabled={true}
						/>
					))}
				</CardGroup>
			</Card.Body>
		</Card>
	);
}
