import React, { useState } from "react";
import Card from "react-bootstrap/Card";
import { useDispatch } from "react-redux";
import { toggleBlock, toggleExtraBlock } from "../../store/blocks";
import FormCheck from "react-bootstrap/FormCheck";

export default function Block(props) {
	const dispatch = useDispatch();
	const [value, setValue] = useState(props.isEnabled);
	const [extraValue, setExtraValue] = useState(props.isExtraEnabled);
	const hasIconToDisplay =
		props.icon &&
		props.icon.src &&
		props.icon.src.props &&
		props.icon.src.props.children;
	const iconProps = hasIconToDisplay ? props.icon.src.props : {};
	const slug =
		(props.parent ? props.parent.replace("/", "-") : "none") + props.name;

	return (
		<div>
			<Card border={"success"} className={"m-2 p-0"} style={{ width: "18rem" }}>
				<Card.Header style={{ background: "azure" }}>
					{props.title ? props.title : props.name}
					{hasIconToDisplay && (
						<svg
							className={"float-end"}
							width={"24"}
							height={"24"}
							viewBox={iconProps.viewBox}
							xmlns={iconProps.xmlns}
							role="img"
						>
							{iconProps.children.props && iconProps.children.props.d && (
								<path d={iconProps.children.props.d} />
							)}
						</svg>
					)}
				</Card.Header>
				<Card.Body>
					{props.parent === null && (
						<Card.Text className="fw-bold">{props.name}</Card.Text>
					)}
					{props.parent !== null && (
						<Card.Text className="fw-bold">
							{props.parent + " > " + props.name}
						</Card.Text>
					)}
					<Card.Text>{props.description}</Card.Text>
					<p>
						<FormCheck
							type="switch"
							id={"chkEnableGlobally_" + slug}
							label="Enable Globally"
							checked={value}
							onChange={() => {
								dispatch(
									toggleBlock({ name: props.name, parent: props.parent })
								)
									.unwrap()
									.then((res) => {
										setValue(res.status);
									})
									.catch((error) => {
										//TODO: Report error to user.
										console.log(error);
									});
							}}
						/>
					</p>
					<p>
						<FormCheck
							type="switch"
							id={"chkEnableForList_" + slug}
							label="Enable For List"
							checked={extraValue}
							onChange={() => {
								dispatch(
									toggleExtraBlock({
										name: props.name,
										parent: props.parent,
									})
								)
									.unwrap()
									.then((res) => {
										setExtraValue(res.status);
									})
									.catch((error) => {
										//TODO: Report error to user.
										console.log(error);
									});
							}}
							disabled={value}
						/>
					</p>
				</Card.Body>
			</Card>
		</div>
	);
}
