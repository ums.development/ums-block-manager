import React from "react";
import { useSelector } from "react-redux";
import BlockCategory from "./BlockCategory";
import ManualBlockCategory from "./ManualBlockCategory";

export default function BlockManager() {
	const categories = useSelector((state) => state.categories.categories)
		.slice()
		.sort((a, b) => {
			if (a.title < b.title) {
				return -1;
			}
			if (a.title > b.title) {
				return 1;
			}

			return 0;
		});

	return (
		<div>
			{categories.map((cat) => (
				<BlockCategory key={cat.slug} slug={cat.slug} name={cat.title} />
			))}
			<ManualBlockCategory />
		</div>
	);
}
