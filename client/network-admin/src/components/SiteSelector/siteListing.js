import { useDispatch, useSelector } from "react-redux";
import { useEffect, useState } from "react";
import LoadingOverlay from "@ronchalant/react-loading-overlay";
import { getAllSites } from "../../store/sites";
import { Button, Col, Container, Form, Row, Table } from "react-bootstrap";
import { addEnabledSite, removeEnabledSite } from "../../store/enabledSites";

export default function SiteListing() {
	const dispatch = useDispatch();
	const enabledSites = useSelector((state) => state.enabledSites.sites);
	const enabledSitesFetching = useSelector(
		(state) => state.enabledSites.isFetching
	);
	const sites = useSelector((state) => state.sites.sites);
	const sitesIsFetching = useSelector((state) => state.sites.isFetching);

	const [searchID, setSearchID] = useState("");
	const [searchDomain, setSearchDomain] = useState("");
	const [searchPath, setSearchPath] = useState("");
	const [filteredSites, setFilteredSites] = useState(sites);

	useEffect(() => {
		let workingFilteredSites = sites;

		if (searchID.length > 0) {
			workingFilteredSites = sites.filter((site) => {
				return site.blog_id === searchID;
			});
		}

		if (searchDomain.length > 0) {
			workingFilteredSites = workingFilteredSites.filter((site) => {
				return site.domain.includes(searchDomain);
			});
		}

		if (searchPath.length > 0) {
			workingFilteredSites = workingFilteredSites.filter((site) => {
				return site.path.includes(searchPath);
			});
		}

		setFilteredSites(workingFilteredSites);
	}, [sites, searchID, searchDomain, searchPath]);

	useEffect(() => {
		dispatch(getAllSites());
	}, [dispatch]);

	return (
		<div>
			<LoadingOverlay active={enabledSitesFetching || sitesIsFetching} spinner>
				<Container>
					<Row>
						<Col>
							<Form.Group>
								<Form.Label htmlFor="txtSearchID" visuallyHidden>
									Search for Exact Blog Id
								</Form.Label>
								<Form.Control
									id="txtSearchID"
									type="text"
									placeholder="Exact Id Search"
									value={searchID}
									onChange={(e) => setSearchID(e.target.value)}
								/>
							</Form.Group>
						</Col>
						<Col>
							<Form.Group>
								<Form.Label htmlFor="txtSearchDomain" visuallyHidden>
									Search for Like Domain
								</Form.Label>
								<Form.Control
									id="txtSearchDomain"
									type="text"
									placeholder="Domain Search"
									value={searchDomain}
									onChange={(e) => setSearchDomain(e.target.value)}
								/>
							</Form.Group>
						</Col>
						<Col>
							<Form.Group>
								<Form.Label htmlFor="txtSearchPath" visuallyHidden>
									Search for Like Path
								</Form.Label>
								<Form.Control
									id="txtSearchPath"
									type="text"
									placeholder="Path Search"
									value={searchPath}
									onChange={(e) => setSearchPath(e.target.value)}
								/>
							</Form.Group>
						</Col>
					</Row>
					<Row className="mt-3">
						<Col>
							<Table striped bordered hover>
								<thead>
									<tr>
										<th>Action</th>
										<th>Blog Id</th>
										<th>Domain</th>
										<th>Path</th>
									</tr>
								</thead>
								<tbody>
									{filteredSites.map((site) => {
										return (
											<tr key={site.blog_id}>
												<td>
													{enabledSites.filter((es) => {
														return es.blog_id === site.blog_id;
													}).length === 0 ? (
														<Button
															variant="success"
															onClick={() => {
																dispatch(addEnabledSite({ id: site.blog_id }));
															}}
														>
															Add
														</Button>
													) : (
														<Button
															variant="danger"
															onClick={() => {
																dispatch(
																	removeEnabledSite({ id: site.blog_id })
																);
															}}
														>
															Remove
														</Button>
													)}
												</td>
												<td>{site.blog_id}</td>
												<td>{site.domain}</td>
												<td>{site.path}</td>
											</tr>
										);
									})}
								</tbody>
							</Table>
						</Col>
					</Row>
				</Container>
			</LoadingOverlay>
		</div>
	);
}
