import React, { useEffect, useState } from "react";
import { getEnabledSites } from "../../store/enabledSites";
import LoadingOverlay from "@ronchalant/react-loading-overlay";
import { useDispatch, useSelector } from "react-redux";
import {
	Button,
	Card,
	Col,
	Container,
	Modal,
	Row,
	Stack,
} from "react-bootstrap";
import CardHeader from "react-bootstrap/CardHeader";
import SiteListing from "./siteListing";

export default function SiteSelector() {
	const dispatch = useDispatch();
	const enabledSites = useSelector((state) => state.enabledSites.sites);
	const enabledSitesFetching = useSelector(
		(state) => state.enabledSites.isFetching
	);
	const [showSitesModal, setShowSitesModal] = useState(false);

	useEffect(() => {
		dispatch(getEnabledSites());
	}, [dispatch]);

	return (
		<div>
			<LoadingOverlay active={enabledSitesFetching} spinner>
				<Card className="p-0 m-4">
					<CardHeader>
						<Container>
							<Row>
								<Col>Sites with Extra Blocks Enabled</Col>
								<Col className="d-flex justify-content-end">
									<Button onClick={() => setShowSitesModal(true)}>
										Add/Remove Sites from List
									</Button>
								</Col>
							</Row>
						</Container>
					</CardHeader>
					<Card.Body>
						<Stack direction="horizontal" gap={2}>
							{enabledSites.map((site) => {
								return <div key={site.blog_id}>{site.domain + site.path}</div>;
							})}
						</Stack>
					</Card.Body>
				</Card>
			</LoadingOverlay>
			<Modal
				size="lg"
				show={showSitesModal}
				onHide={() => setShowSitesModal(false)}
			>
				<Modal.Header closeButton>
					<Modal.Title>Add/Remove Sites from the List</Modal.Title>
				</Modal.Header>
				<Modal.Body>
					<SiteListing />
				</Modal.Body>
			</Modal>
		</div>
	);
}
