module.exports = {
	eslint: {
		configure: {
			rules: {
				"no-use-before-define": ["error", { variables: false }],
			},
		},
	},
	webpack: {
		configure: {
			output: {
				filename: "static/js/ums-block-manager-network-admin.js",
			},
			optimization: {
				runtimeChunk: false,
				splitChunks: {
					chunks(chunk) {
						return false;
					},
				},
			},
		},
	},
	plugins: [
		{
			plugin: {
				overrideWebpackConfig: ({ webpackConfig }) => {
					webpackConfig.plugins[5].options.filename =
						"static/css/ums-block-manager-network-admin.css";
					return webpackConfig;
				},
			},
			options: {},
		},
	],
};
